<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Http\Models\User;
use App\Libraries\Verify;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;

class MemberController extends BaseController
{
    private $request;


    public function __construct()
    {
        $this->request = $this->requestAll();
        parent::__construct();
    }

    public function test(){

        echo  23232;die;
    }

    //合伙人列表
    public function showInviteMan(){
        $request = $this->requestAll();

        $invite = DB::table('user');
        if(!empty($request['user_nickname'])){
            $invite = $invite->where('user_nickname','like','%'.$request['user_nickname'].'%');
        }
        if(!empty($request['user_phone'])){
            $invite = $invite->where('user_phone','like','%'.$request['user_phone'].'%');
        }
        $invite = $invite->where('is_invite',1)->orderBy('up_time','desc')->paginate(10);

        //邀请码
        $inviteCode = DB::table('invite_code')->get()->toArray();
        $code_column = array_column($inviteCode,'code','user_id');
        $status_column = array_column($inviteCode,'code_status','user_id');

        //推荐人数
        $inviteList = DB::table('invite_list')
            ->select(DB::raw('invite_user_id,count(user_id) as num'))
            ->groupBy('invite_user_id')->get()->toArray();

        $num_column = array_column($inviteList,'num','invite_user_id');

        return view("user.showinviteman",[
            'invite'=>$invite,
            'code_column'=>$code_column,
            'status_column'=>$status_column,
            'num_column'=>$num_column

        ]);

    }
    //禁用启用 邀请码
    public function banUserCode(){
        $request = $this->requestAll();
        DB::table('invite_code')->where('user_id',$request['user_id'])->update(['code_status'=>$request['code_status']]);

        return redirect('user/showpartner');

    }



    //未认证，新列表
    public function showNewUser(){
        $request = $this->requestAll();

        $user = DB::table('user');
        if(!empty($request['user_nickname'])){
            $user = $user->where('user_nickname','like','%'.$request['user_nickname'].'%');
        }
        if(!empty($request['user_phone'])){
            $user = $user->where('user_phone','like','%'.$request['user_phone'].'%');
        }
        $user = $user->where('user_type',0)->paginate(10);

        return view("user.shownewuser",[
            'user'=>$user,
        ]);

    }

    //跟踪用户
    public function trackNewUser(){
        $request = $this->requestAll();
        DB::table('user')->where('user_id',$request['user_id'])->update(['is_track'=>1]);

        return redirect('user/shownewuser');

    }


}
