<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Http\Models\User;
use App\Libraries\Verify;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;

class BuyerController extends BaseController
{
    private $request;


    public function __construct()
    {
        $this->request = $this->requestAll();
        parent::__construct();
    }

    public function test(){

        echo  23232;die;
    }

    //医院列表
    public function showBuyer(){
        $request = $this->requestAll();

        $buyer = DB::table('user');
        if(!empty($request['company_name'])){
            $buyer = $buyer->where('company_name','like','%'.$request['company_name'].'%');
        }
        if(!empty($request['user_phone'])){
            $buyer = $buyer->where('user_phone','like','%'.$request['user_phone'].'%');
        }
        $buyer = $buyer->where('user_type',10)->orderBy('up_time','desc')->paginate(10);
        $data = $buyer->items();
        foreach ($data as $k=>$v){
            $data[$k]['hospitalType'] = hospitalType1DataMap($v['hospital_type1']).','.hospitalType2DataMap($v['hospital_type1'],$v['hospital_type2']);
            $data[$k]['area'] =provinceDataMap($v['province_id']).'-'.cityDataMap($v['city_id']).'-'.areaDataMap($v['area_id']);

        }
        return view("user.showbuyer",[
            'data'=>$data,
            'page'=>$buyer->links()
        ]);

    }

    //审核
    public function exaBuyer(){
        $request = $this->requestAll();

        $user = DB::table('user')->where('user_id',$request['user_id'])->first();

        $re= DB::table('user')->where('user_id',$request['user_id'])->update(['user_status'=>$request['user_status']]);

        //发送审核通过短信
        if($re >0 && $request['user_status'] ==2){
            $content = '【医竞采】恭喜,'.$user['company_name'].',您的审核通过了!';
            $this->sendSms($user['user_phone'],$content);

        }elseif($re >0 && $request['user_status'] ==3){
            $content = '【医竞采】您好,'.$user['company_name'].',您的审核被驳回了,请联系客服。';
            $this->sendSms($user['user_phone'],$content);

        }
        return redirect('/user/showbuyer');

    }
    //禁用
    public function banUseBuyer(){
        $request = $this->requestAll();
        DB::table('user')->where('user_id',$request['user_id'])->update(['is_del'=>$request['is_del']]);

        return redirect('/user/showbuyer');
    }

    //医院详情
    public function buyerDetail(){
        $request = $this->requestAll();
        $buyer = DB::table('user')->where('user_id',$request['user_id'])->first();
        //医院类型
        $buyer['hospitalType'] = hospitalType1DataMap($buyer['hospital_type1']).','.hospitalType2DataMap($buyer['hospital_type1'],$buyer['hospital_type2']);
        //省市
        $buyer['area'] =provinceDataMap($buyer['province_id']).'-'.cityDataMap($buyer['city_id']).'-'.areaDataMap($buyer['area_id']);
        //邀请码
        $invite = DB::table('invite_list')->where('user_id',$buyer['user_id'])->first();
        $buyer['invite_code'] = $invite['invite_code'];
        //图片 医院经营许可证
        $file = DB::table('file')->where(['file_type'=>20,'file_user_id'=>$buyer['user_id'],'status'=>0])->first();
        //图片 医疗机构执业许可证
        $file1 = DB::table('file')->where(['file_type'=>21,'file_user_id'=>$buyer['user_id'],'status'=>0])->first();
        $buyer['yingye'] = Config('app.file_base_url').$file['file_url'];
        $buyer['xvke'] = Config('app.file_base_url').$file1['file_url'];

        return view("user.buyerdetail",[
            'buyer'=>$buyer
        ]);

    }

}
