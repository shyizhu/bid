<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Http\Models\User;
use App\Libraries\Verify;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;

class SellerController extends BaseController
{
    private $request;


    public function __construct()
    {
        $this->request = $this->requestAll();
        parent::__construct();
    }

    public function test(){
      p(hospitalType2DataMap(2,5));
    }

    //供应商列表
    public function showSeller(){
        $request = $this->requestAll();

        $seller = DB::table('user');
        if(!empty($request['company_name'])){
            $seller = $seller->where('company_name','like','%'.$request['company_name'].'%');
        }
        if(!empty($request['user_phone'])){
            $seller = $seller->where('user_phone','like','%'.$request['user_phone'].'%');
        }
        $seller = $seller->where('user_type',11)->orderBy('up_time','desc')->paginate(10);
        $data = $seller->items();
        foreach ($data as $k=>$v){
            $data[$k]['area'] =provinceDataMap($v['province_id']).'-'.cityDataMap($v['city_id']).'-'.areaDataMap($v['area_id']);

        }
        return view("user.showseller",[
            'data'=>$data,
            'page'=>$seller->links()
        ]);


    }
    //审核
    public function exaSeller(){
        $request = $this->requestAll();
        $user = DB::table('user')->where('user_id',$request['user_id'])->first();
        $re = DB::table('user')->where('user_id',$request['user_id'])->update(['user_status'=>$request['user_status']]);
        //发送审核通过短信
        if($re >0 && $request['user_status'] ==2){
            $content = '【医竞采】恭喜,'.$user['company_name'].',您的审核通过了!';
            $this->sendSms($user['user_phone'],$content);

        }elseif($re >0 && $request['user_status'] ==3){
            $content = '【医竞采】您好,'.$user['company_name'].',您的审核被驳回了,请联系客服。';
            $this->sendSms($user['user_phone'],$content);

        }

        return redirect('/user/showseller');

    }
    //禁用
    public function banUseSeller(){
        $request = $this->requestAll();
        DB::table('user')->where('user_id',$request['user_id'])->update(['is_del'=>$request['is_del']]);

        return redirect('/user/showseller');
    }

    //供应商详情
    public function sellerDetail(){
        $request = $this->requestAll();
        $seller = DB::table('user')->where('user_id',$request['user_id'])->first();
        //省市
        $seller['area'] =provinceDataMap($seller['province_id']).'-'.cityDataMap($seller['city_id']).'-'.areaDataMap($seller['area_id']);
        //$seller['area'] =2323;
        //图片1供应商经营许可证，2身份证正面，3反面4医疗器械经营许可证
        $file1 = DB::table('file')->where(['file_type'=>1,'file_user_id'=>$seller['user_id'],'status'=>0])->first();
        $file2 = DB::table('file')->where(['file_type'=>2,'file_user_id'=>$seller['user_id'],'status'=>0])->first();
        $file3 = DB::table('file')->where(['file_type'=>3,'file_user_id'=>$seller['user_id'],'status'=>0])->first();
        $file4 = DB::table('file')->where(['file_type'=>4,'file_user_id'=>$seller['user_id'],'status'=>0])->get()->toArray();


        $seller['yingye'] = Config('app.file_base_url').$file1['file_url'];
        $seller['zheng'] = Config('app.file_base_url').$file2['file_url'];
        $seller['fan'] = Config('app.file_base_url').$file3['file_url'];
        foreach ($file4 as $k => $v){
            $seller['xvke'][] = Config('app.file_base_url').$v['file_url'];

        }

        return view("user.sellerdetail",[
            'seller'=>$seller
            ]
        );
    }



}
