<?php

namespace App\Http\Controllers\Bid;

use App\Http\Controllers\BaseController;
use App\Http\Models\User;
use App\Libraries\Verify;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;

class BidController extends BaseController
{
    private $request;


    public function __construct()
    {
        $this->request = $this->requestAll();
        parent::__construct();
    }

    public function test(){
        $phone = '15810867923';
        $content = '【医竞采】恭喜,****************,您的审核通过了!';
        $a = '【医竞采】您好,***********,您的审核被驳回了,请联系客服。';
        $b = '【医竞采】您报名的，******************************************************项目，还有**********开标，请及时关注！';

        $c = '【医竞采】您发布的，******************************************************项目，还有**********开标，请及时关注！';

        $d = '【医竞采】恭喜，***********，您参与的******************************************************项目，已中标，如有疑问，请联系客服！';

        $e = '【医竞采】恭喜，***********，您参与的******************************************************项目，采购方已确认，请尽快发货，如有疑问，请联系客服！';


        $this->sendSms($phone,$b);
    }

    //竞价列表
    public function showBid(){
        $request = $this->requestAll();
        //10暂存，20已发布(竞价中)，25竞价进行中,28(中标，待医院确认)，29(医院确认),30流拍，40效益作废，50拒收，60待确认(等待供应商确认)，70成交  状态
        //task //10竞价中，20中标，30流拍，40效益作废，50拒收，60待确认，70成交
        $task = 20;
        if(!empty($request['task'])){
            $task =$request['task'];
        }
        $bid = DB::table('biding');
        if(!empty($request['bid_sn'])){
            $bid = $bid->where('bid_sn','like','%'.$request['bid_sn'].'%');
        }
        if(!empty($request['bid_contact_phone'])){
            $bid = $bid->where('bid_contact_phone','like','%'.$request['bid_contact_phone'].'%');
        }

        
        $bid = $bid->where('bid_status',$task)->orderBy('up_time','desc')->paginate(10);
        //数据过滤
        $data = $bid->items();


        return view("bid.showbid",[
            'data'=>$data,
            'bid'=>$bid,
            'task' =>$task


        ]);

    }

    //关闭竞价
    public function banBid(){
        $request = $this->requestAll();

        DB::table('biding')->where('bid_id',$request['bid_id'])->update(['is_del'=>$request['is_del']]);

        return redirect('bid/showBid?task='.$request['task']);

    }

    //改变竞价状态
    public function changeBidStatus(){
        $request = $this->requestAll();

        DB::table('biding')->where('bid_id',$request['bid_id'])->update(['bid_status'=>$request['bid_status']]);

        return redirect('bid/showBid?task='.$request['task']);

    }

    //竞价详情
    public function bidDetail(){
        $request = $this->requestAll();
        $bid = DB::table('biding')->where('bid_id',$request['bid_id'])->first();
        $goods =  DB::table('biding_goods')->where('bid_id',$request['bid_id'])->get()->toArray();
        $record =  DB::table('biding_record')->where('bid_id',$request['bid_id'])->get()->toArray();

        return view("bid.biddetail",[
            'bid'=>$bid,
            'goods'=>$goods,
            'record'=>$record
        ]);

    }

}
