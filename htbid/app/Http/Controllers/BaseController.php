<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Config;
use Request;
use App\Exceptions\ThrowException;
use App\Libraries\wxBizDataCrypt;
use DB;

class BaseController extends Controller
{
    private $pc_token ='e6161f18c4602cdd2bd0a5bc3e45f405';
    public function __construct(){
    }

    public function test(){
        echo 1111;
    }

    //获取后台用户信息
    public function getAdminInfo(){
        $adminInfo = json_decode(session(config('session.pass')),true);

        return $adminInfo;
    }

    public function getTokenInfo($request){

        $date = date('Y-m-d H:i:s',strtotime("-20000 minute"));
        $data = DB::table('token')->where('key',$request['token'])->where('add_time','>',$date)->first();
        if(!empty($data)){
            return json_decode(stripslashes($data['val']),true);
        }else{
            $this->jsonResult(['status'=>401,'msg'=>'token过期重新登录']);
        }
    }


    public function getMemberInfo($request){
        $date = date('Y-m-d H:i:s',strtotime("-20000 minute"));

        if(!empty($request['token'])) {
            $data = DB::table('weixin_token')->where('key', $request['token'])->where('add_time', '>', $date)->first();
        }
//        p($data);die;
        if(!empty($data)){
            $tokenInfo  =  json_decode(stripslashes($data['val']),true);

            $member = DB::table('user')->where('userid',$tokenInfo['userid'])->first();
            return  $member;
        }else{
            $this->jsonResult(['status'=>401,'msg'=>'token过期重新登录']);
        }
    }


    //更新token,信息
    public function updateTokenInfo($token,$data){
        $tokenInfo = DB::table('token')->select('val')->where('key',$token)->first();
        $tokenInfo =json_decode(stripslashes($tokenInfo['val']),true);
        //手机号
        if(!empty($data['phoneNumber'])){
            $tokenInfo['phoneNumber'] = $data['phoneNumber'];
        }
        if(!empty($data['userid'])){
            $tokenInfo['userid'] = $data['userid'];
        }
        if(!empty($data['user_status'])){
            $tokenInfo['user_status'] = $data['user_status'];
        }
        if(!empty($data['code'])){
            $tokenInfo['code'] = $data['code'];
        }
        if(!empty($data['a_userid'])){
            $tokenInfo['a_userid'] = $data['a_userid'];
        }
        $tokenInfo = addslashes(json_encode($tokenInfo));
        DB::table('token')->where('key',$token)->update(['val'=>$tokenInfo]);
    }


    public function jsonResult($code, $data = '', $message = '',$field=[])
    {

        header('Content-Type:application/json; charset=utf-8');

        //加载配置错误信息
        $errorInfo = Config::get('error.errorCode');
        if (!$code && empty($message) && !empty($data)) {
            $message = $errorInfo[$code];
        }
        $result = array(
            'code' => $code,
            'msg' => $message,
            'data' => $data,
        );
        //添加附加字段
        if(!empty($field)){
            foreach ($field as $k=>$v){
                $result[$k] = $v;
            }
        }

        echo json_encode($result);
        exit(1);

    }

    /**
     * 请求参数 全部方式 获取
     */
    public function requestAll(){

        $request = Request::all();

        return $request;
    }

    //发送短信
    public function sendSms($phone,$content) {
        $url ='http://115.28.50.135:8888/sms.aspx';
        $content = $content;

        $params=[
            'userid'=>'4259',
            'account'=>'北京医麦科技',
            'password'=>'123456',
            'mobile'=>$phone,
            'content'=>$content,
            'sendTime'=>'',
            'action'=>'send',
            'extno'=>'',
        ];

        $rs = http_request($url,$params);
        $arr = xmlToArray($rs);
        if($arr['returnstatus']== 'Success'){
            //return DB::table('sms')->insertGetId($sms);
        }

    }

}
