<?php
namespace App\Http\Controllers\Back;

use App\Http\Controllers\BaseController;
use App\Http\Models\Menu;
use App\Http\Models\Role;
use App\Http\Models\User;
use App\Libraries\Verify;
use Request;
use Config;
use DB;

class RoleController extends BaseController{

    public function __construct()
    {
        $this->request = $this->requestAll();
        parent::__construct();
    }

    //角色列表
    public function showRole(){
        //角色
        $roleList = DB::table('role')->get()->toArray();
        foreach ($roleList as $k=>$v){
            $ids = explode(',',$v['role_menu_id']);
            $tmp = DB::table('menu')->whereIn('menu_id',$ids)->pluck('menu_name')->toArray();
            $roleList[$k]['menuName']= implode($tmp,',');
        }

        return view('power.showrole',[
            'roleList'=>$roleList
        ]);
    }
    //显示添加角色
    public function showRoleAdd(){
        //获取菜单

        $menuList = (new Menu())->getList();


        return view('power.showroleadd',[
            'menuList'=>$menuList,
            'roleMenuId'=>0//默认选中

        ]);

    }

    //添加用户
    public function roleAdd(){
        $request = $this->request;

        $data=[
            'role_name'=>$request['role_name'],
            'role_menu_id'=>$request['role_menu_id'],
        ];

        $res =DB::table('role')->insert($data);

        if($res>0) {

            return redirect('power/showrole');

        }
    }

    //显示编辑
    public function showRoleEdit(){
        $request = $this->request;
        $role_id = $request['role_id'];
        $role = DB::table('role')->where('role_id',$role_id)->first();
        //菜单列表
        $menuList = (new Menu())->getList();


        return view('power.showroleedit',[
            'role'=>$role,
            'menuList'=>$menuList,
            'roleMenuId'=>$role['role_menu_id']
        ]);

    }

    //编辑
    public function roleEdit(){
        $request = $this->request;

        $data = [
          'role_name'=>$request['role_name'],
          'role_menu_id'=>$request['role_menu_id']
        ];

        $res = DB::table('role')->where('role_id',$request['role_id'])->update($data);
        if($res>0) {

            return redirect('power/showrole');
        }
    }


}
