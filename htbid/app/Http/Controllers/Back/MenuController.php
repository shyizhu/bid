<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\BaseController;
use App\Http\Models\Menu;
use App\Libraries\Verify;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use DB;

class MenuController extends BaseController
{
    private $request;


    public function __construct()
    {
        $this->request = $this->requestAll();
        parent::__construct();
    }

    //菜单列表
    public function showMenu(){

        $menuList = (new Menu())->getList();

//        dd($menuList);

        return view('power.showmenu',[
            'menuList'=>$menuList
        ]);

    }

    //显示新增
    public function showMenuAdd(){
        $menu = DB::table('menu')->where(['leval'=>1,'is_show'=>1])->get()->toArray();

        return view('power.showmenuadd',[
            'menu'=>$menu
        ]);
    }


    //新增菜单
    public function menuAdd(){
        $pid     = $this->request['menu_pid'];

        //验证菜单等级
        if($pid == 0){
            $leval = 1;
        }else{
          $pLeval=  (new Menu())->getParentDepth($pid);
            $leval = $pLeval[0]+1;
        }

        $input = [
            'menu_pid'        => $this->request['menu_pid'],
            'menu_name'        => $this->request['menu_name'],
            'menu_path'        => $this->request['menu_path'],
            'menu_sort'        => $this->request['menu_sort'],
            'menu_icon'        => $this->request['menu_icon'],
            'leval'            => $leval,
            'is_show'            => 1,

        ];
        $rs = Menu::create($input);

        if($rs['id'] > 0) {

            return redirect('power/showmenu');
        }
    }


    /**
     * 菜单编辑
     */
    public function edit(){
        $request = Request::all();

        $id       = (int)Request::input('id');
        $parentId       = (int)Request::input('parent_id');
        $name     = strip_tags(Request::input('name'));
        $path     = strip_tags(Request::input('path'));
        $sort     = (int)Request::input('menu_sort');
        $show     = (int) Request::input('menu_show');
        $expend   = (int)Request::input('menu_expand');
        $icon   = Request::input('icon');
        $content   = strip_tags(Request::input('content'));

        //$uid = $this->userInfo['id'];

        //菜单等级
        if($parentId == 0){
            $depth = 1;
        }else{
            $parentDepth =  (new Menu())->getParentDepth($parentId);
            $depth = $parentDepth[0]+1;
        }

        //ORM 模型
        $input = [
            'name'        => $name,
            'parent_id' => $parentId,
            'path'        => $path,
            'menu_sort'   => $sort,
            'menu_expand' => $expend,
            'menu_show'   => $show,
            'icon' =>$icon,
            'content'=>$content,
           'edit_id'     => $this->userInfo['id'],

            'edit_time'    => date('Y-m-d H:i:s'),
        ];

//        echo '<pre>';
//        print_r($input);die;

        $menuModel = new Menu();
        $rs = $menuModel->saveMenuInfo($id,$input);
        (new SaasOperateLog())->insertOperateLog(10,$name);//插入日志

        return redirect('menu/show');
    }
















    /**
     * 异步查询菜单记录
     */
    public function info(){

        $id = (int)Request::input('id');

        $model = new MenuModel();
        $info = $model->getMenuInfo($id);
        echo json_encode($info);
    }


    /**
     * 权限列表
     */
    public function authList(){

        $id = (int)Request::input('id');
        $page = (int) Request::input('page');//分页条数
        $rows = (int) Request::input('rows');//每页显示数量
        $page == 0 ? $page =1 : $page;
        $rows == 0 ? $rows =10 : $rows;


        $opration = new OprationModel();
        $rs = $opration->getTreeOpration($id,$rows);
        echo json_encode($rs);

    }

    /**
     * 页面操作权限选择
     * 角色筛选操作权限
     */
    public function authTreeCheck(){

        $id     = (int) Request::input('id');
        $treeId = (int)Request::input('treeid');

        $opration = new OprationModel();
        $rs = $opration->oprationList($id,$treeId);
        echo json_encode($rs);
    }

    /**
     * 菜单权限
     */
    public function authority(){

        $param = strip_tags(Request::input('param'));
        $id    = (int)Request::input('id');

        $data['authority'] = $param;
        $model = new MenuModel();
        $rs = $model->saveMenuInfo($id,$data);
        echo $rs;
    }
    

    /*
     * 菜单权限修改
     */
    public function authEdit(){
        $id         = (int)Request::input('auth_edit_id');
        $code       = (string)Request::input('menu_opration_code');
        $name       = strip_tags(Request::input('menu_opration_name'));
        $comment    = strip_tags(Request::input('menu_opration_edit_message'));
        $show       = (int)Request::input('menu_opration_edit_show');

        $data['code']    = $code;
        $data['name']    = $name;
        $data['content'] = $comment;
        $data['show']    = $show;

        $model = new MenuModel();
        $model->editAuthOpration($id, $data);
    }

}
