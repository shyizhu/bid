<?php
namespace App\Http\Controllers\Back;

use App\Http\Controllers\BaseController;


use DB;
class IndexController extends BaseController{


	public function test(){

        echo 12123;

	}

    // 主页
    public function index(){
            $adminInfo = $this->getAdminInfo();


            //获取左侧导航栏

        $menuArr  = explode(',',$adminInfo['role_menu_id']);
        $adminInfo['parentTree'] = DB::table('menu')->where(['leval'=>1,'is_show'=>1])->whereIn('menu_id',$menuArr)->get()->toArray();//一级菜单
        $adminInfo['kidTree'] = DB::table('menu')->where(['leval'=>2,'is_show'=>1])->whereIn('menu_id',$menuArr)->get()->toArray();//二级菜单


        return view('back.index',[
            'adminInfo'=>$adminInfo
        ]);

    }

    //主页统计图
    public function data(){

        return view('back.data');


    }

    //欢迎页面
    public function welcome(){

	    return view('welcome');
    }
}
