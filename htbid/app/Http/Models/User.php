<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

use DB;
class User extends Model
{

    protected $table = 'user';
    protected $primaryKey = 'user_id';
    public $timestamps = false;
    protected $guarded = [];


    public function getUserList(){
        return  $this->orderBy('add_time','desc')->get()->toArray();
    }

    //手机号注册
    public function phoneRegister($data){
        $result =[];
        $user = DB::table('user')->where('user_phone',$data['phoneNumber'])->first();
        //未注册
        if(empty($user)){
            $result = [
                'user_phone'=>$data['phoneNumber'],
                'openid'=>$data['openid'],//小程序标识
                'user_nickname'=>$data['user_nick'],//昵称
                'user_type' => 0

            ];
            $result['user_id']  = DB::table('user')->insertGetId($result);

        }

        if(!empty($user)){
            $result['user_id'] = $user['user_id'];
            $result['user_type'] = $user['user_type'];

        }
        return $result;

    }

    //获取所有公司名
    public function getAllCompanyName(){
        $company = DB::table('user')->select('user_id as id','company_name as name')->get();
        $data = array();
        foreach($company as $v){
            $data[$v['id']] = $v['name'];
        }
        return $data;
    }

    //获取所有电话
    public function getAllUserPhone(){
        $company = DB::table('user')->select('user_id as id','user_phone as name')->get();
        $data = array();
        foreach($company as $v){
            $data[$v['id']] = $v['name'];
        }
        return $data;
    }

}
