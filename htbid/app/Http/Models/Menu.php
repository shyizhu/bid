<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Menu extends Model
{
    protected $table = 'menu';

    protected $fillable = array('menu_pid','menu_name','menu_path','menu_icon','menu_sort','leval','is_show','add_time','up_time');

    //protected $guarded = array();

    public $timestamps = false;

    //获取一条
    public function getListOne(){

        $where['status'] = 1;
        $where['depth'] = 1;
        $data = $this->where($where)->orderBy('menu_sort', 'asc')->get()->toArray();

        return $data;

    }
    public function getList(){


        $data['is_show'] = 1;
        $data = $this->where($data)->orderBy('menu_sort', 'asc')->get()->toArray();
        
        $rs = array();
        $rs = $this->_menuSort($data,$rs);

        return $rs;
    }

    //排序
    private function _menuSort($data,$rs,$leval = 1,$id = 0){

        foreach ($data as $k=>$v){

            if ( $leval == $v['leval'] ){

                if ($leval == 1 ){
                    $v['menu_name'] = $this->_getLine($leval).$v['menu_name'];
                    $id = $v['menu_id'];
                    $rs[] = $v;
                    //array_splice($data, $k, 1);
                }

                foreach ($data as $key=>$value){
                    $tmp = $leval +1;
                    if (($value['leval'] == $tmp) && ($v['menu_id'] == $value['menu_pid']) && ($id == $value['menu_pid'])){
                        $value['menu_name'] = $this->_getLine($leval+1).$value['menu_name'];
                        $rs[] = $value;
                        //array_splice($data, $key, 1);
                        $rs = $this->_menuSort($data,$rs,$value['leval'],$value['menu_id']);
                    }
                }

            }
        }

        return $rs;
    }

    private function _getLine($depth){

        $str = '|—';
        if ($depth == 1 ){
            return $str;
        }else{

            for ($i=1;$i<$depth;$i++){
                $str .= '——';
            }
            return $str;
        }
    }

    // 得到父级栏目的等级
    public function getParentDepth($pid){
        return $this->where('menu_id',$pid)->pluck('leval');
    }

    //取一条
    public function getMenuInfo($id){

        $rs = $this->where('id',$id)->get()->toArray();
        if (empty($rs)){
            return null;
        }
        return $rs[0];
    }

    //更新
    public function saveMenuInfo($id,$data){
        return $this->where('id',$id)->update($data);
    }

    /**
     * 返回 EasyUI 格式树行结构
     */
    public function getTreeList(){

        $field = array('id','name','parent_id','depth');

        $data['status'] = 1;
        $result = $this->select($field)
                       ->where($data)
                       ->orderBy('menu_sort', 'asc')
                       ->get()
                       ->toArray();

        $rs = array();
        $result = $this->_getTreeArray($result,0);
        return $result;
    }



    /**
     * 左侧菜单
     * @param string $uid 用户id
     * @return array 多维数组
     */
    public function leftMenu($uid){

        $rs = $this->_getRoleIds($uid);
        if (empty($rs)) return null;

        $field = array('id','name','parent_id','depth','authority','path');
        $data['status'] = 1;

        $result = $this->select($field)
                        ->where($data)
                        ->wherein('id',$rs)
                        ->orderBy('menu_sort', 'asc')
                        ->get()->toArray();

        return $this->_getTreeMenu($result,0);

    }

    /**
     * 生成数型结构
     * @param $data
     * @param $parentId
     * @return array|string
     */
    private function _getTreeMenu($data,$parentId){

        $tree = '';
        foreach($data as $k => $v) {
            if($v['parent_id'] == $parentId) {
                $v['tree'] = $this->_getTreeMenu($data, $v['id']);
                $tree[] = $v;
            }
        }
        return $tree;
    }

    /**
     * 生成一维数组
     * @param $arr
     * @param $id
     * @return array
     */
    private function _getTreeArray($arr, $id){

        $subs = array();
        foreach ($arr as $k => $v) {
            if ($v['parent_id'] == $id) {

                $node['id']   = $v['id'];
                $node['name'] = $v['name'];
                $node['pId']  = $v['parent_id'];
                if ($v['parent_id'] == 0){
                    $node['open'] = true;
                }

                $subs[] = $node;
                $subs = array_merge($subs, $this->_getTreeArray($arr, $v['id']));
            }
        }
        return $subs;
    }

    /**
     * 菜单
     * @param $uid
     * @return null
     */
    private function _getRoleIds($uid){

        $sql = "SELECT role_id FROM cq_job WHERE id = (SELECT job_id FROM cq_user WHERE id={$uid})";
        $rs = DB::select($sql);

        if (!empty($rs[0]['role_id'])){
            $sql = "SELECT authorise FROM cq_role WHERE id IN ({$rs[0]['role_id']})";
            $result = DB::select($sql);
            if (!empty($result)){

                $ids = array();
                foreach ($result as $k=>$v){
                    $tmp = explode(',',$v['authorise']);
                    //取并集
                    $ids = array_merge($ids,$tmp); //array_intersect($ids, $tmp);

                }
                //去重
                $ids = array_unique($ids);

                //去除已删除菜单
                $sql = "SELECT id FROM cq_tree WHERE STATUS=0";
                $treeIds = DB::select($sql);

                if (!empty($treeIds)){
                    foreach ($treeIds as $v){
                        foreach ($ids as $key=>$val){
                            if ($v['id'] == $val){
                                unset($ids[$key]);
                            }
                        }
                        //unset($ids[array_search($v['id'],$ids)]);
                    }
                }

                return $ids;
            }
        }
        return null;
    }

    /**
     * 页面操作权限
     * @param $uid
     */
    public function pageOpration($uid,$treeId){

        if ( empty($treeId )){
            return null;
        }

        $sql = "SELECT role_id FROM cq_job WHERE id = (SELECT job_id FROM cq_user WHERE id={$uid})";
        $rs = DB::select($sql);

        if (!empty($rs)){
            $sql = "SELECT opration FROM cq_role WHERE id IN ({$rs[0]['role_id']})";
            $result = DB::select($sql);
            if (!empty($result)){

                $ids = array();
                foreach ($result as $k=>$v){

                    $list = json_decode($v['opration'],true);
                    //取并集

                    if (!empty($list)){
                        foreach ($list as $k=>$v){
                            //if ($v['key'] == $treeId){
                               // $ids = array_merge($ids,$v['value']); //array_intersect($ids, $tmp);
                            //}
                            if ($k == $treeId){
                                $ids = array_merge($ids,$v);
                            }

                        }
                    }

                }
                //去重
                return array_unique($ids);
            }
        }
        return null;
    }

}
