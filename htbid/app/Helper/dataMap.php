<?php

/**数据映射**/
/*
 * 标书分类
 */
if( ! function_exists('bidTypeDataMap') ){
    function bidTypeDataMap($key=null){
        $map = [
            10 => '医疗耗材',
            20 => '医用设备',
            30 => '外科器械',
            40 => '康复护理',
            50 => '宠物用品',
            60 => '医院其他'
        ];
        if( isset($key) ){
            return !empty($map[$key]) ? $map[$key] : '-';
        }
        return $map;
    }
}

//文件管理
if( ! function_exists('fileTypeDataMap') ){
    function fileTypeDataMap($key=null){
        $map = [
            1 => '营业执照扫描件',//供应商
            2 => '法定代表人身份证(正面)',
            3 => '法定代表人身份证(反面)',
            4 => '医疗器械经营许可证/第二类医疗器械经营备案证',
            20 => '营业执扫描件',//医院
            21 => '医疗机构执业许可证'
        ];
        if( isset($key) ){
            return !empty($map[$key]) ? $map[$key] : '-';
        }
        return $map;
    }
}

//10暂存，20已发布(竞价中)，25竞价进行中,28(中标，待医院确认)，29(医院确认),30流拍，40效益作废，50拒收，60待确认(等待供应商确认)，70成交  状态
if( ! function_exists('bidStatusDataMap') ){
    function bidStatusDataMap($key=null){
        $map = [
            10 => '暂存',
            20 => '已发布',
            25 => '竞价进行中',
            28 => '中标',
            29 => '医院确认中标',
            30 => '流拍',
            40 => '效益作废',
            50 => '拒收',
            60 => '待供应商确认',
            70 => '成交'

        ];
        if( isset($key) ){
            return !empty($map[$key]) ? $map[$key] : '-';
        }
        return $map;
    }
}
//标签切换，标书状态 0竞价中，1已中标，2未中标，3流拍，4效益作废，5拒收，6待确认，7成交
if( ! function_exists('tabBidStatusDataMap') ){
    function tabBidStatusDataMap($key=null){
        $map = [
            0 => 20,
            1 => 28,
            2 => 28,
            3 => 30,
            4 => 40,
            5 => 50,
            6 => 60,
            7 => 70

        ];
        if( isset($key) ){
            return !empty($map[$key]) ? $map[$key] : '-';
        }
        return $map;
    }
}
//交货时间bid_arrival_time
if( ! function_exists('bidArrivalTimeDataMap') ){
    function bidArrivalTimeDataMap($key=null){
        $map = [
            1 => '7天',
            2 => '15天',
            3 => '30天',

        ];
        if( isset($key) ){
            return !empty($map[$key]) ? $map[$key] : '-';
        }
        return $map;
    }
}
//付款方式bid_pay_type
if( ! function_exists('bidPayTypeDataMap') ){
    function bidPayTypeDataMap($key=null){
        $map = [
            1 => '首付30%',
            2 => '货到七日内付款',

        ];
        if( isset($key) ){
            return !empty($map[$key]) ? $map[$key] : '-';
        }
        return $map;
    }
}

//省级
if( !function_exists('provinceDataMap') ){
    function provinceDataMap($key=null){
        $AreaModel = new App\Http\Models\Area();
        $map = $AreaModel->getAllProvince();
        if( isset($key) ){
            return !empty($map[$key]) ? $map[$key] : '';
        }
        return $map;
    }
}

//市级
if( !function_exists('cityDataMap') ){
    function cityDataMap($key=null){
        $AreaModel = new App\Http\Models\Area();
        $map = $AreaModel->getAllCity();
        if( isset($key) ){
            return !empty($map[$key]) ? $map[$key] : '';
        }
        return $map;
    }
}

//地区
if( !function_exists('areaDataMap') ){
    function areaDataMap($key=null){
        $AreaModel = new App\Http\Models\Area();
        $map = $AreaModel->getAllArea();
        if( isset($key) ){
            return !empty($map[$key]) ? $map[$key] : '';
        }
        return $map;
    }
}

//公司名称
if( !function_exists('companyNameDataMap') ){
    function companyNameDataMap($key=null){
        $UserModel = new App\Http\Models\User();
        $map = $UserModel->getAllCompanyName();
        if( isset($key) ){
            return !empty($map[$key]) ? $map[$key] : '-';
        }
        return $map;
    }
}

//用户电话
if( !function_exists('userPhoneDataMap') ){
    function userPhoneDataMap($key=null){
        $UserModel = new App\Http\Models\User();
        $map = $UserModel->getAllUserPhone();
        if( isset($key) ){
            return !empty($map[$key]) ? $map[$key] : '-';
        }
        return $map;
    }
}
//医院类型1
if( !function_exists('hospitalType1DataMap') ){
    function hospitalType1DataMap($key=null){
        $map = Config("hospitaltype.hospital_type1");
        if( isset($key) ){
            return !empty($map[$key]) && !is_array($map[$key]) ? $map[$key] : '-';
        }
        return $map;
    }
}
//医院类型2
if( !function_exists('hospitalType2DataMap') ){
    function hospitalType2DataMap($key1=null,$key2=null){
        $map = Config("hospitaltype.hospital_type2");
        if( isset($key1) && isset($key2)){
            return !empty($map[$key1][$key2]) && !is_array($map[$key1][$key2]) ? $map[$key1][$key2] : '-';
        }
        return $map;
    }
}



?>