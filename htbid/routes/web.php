<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::any('/test','Back\IndexController@test');
Route::any('/welcome','Back\IndexController@welcome');


//back
Route::any('/back/login','Back\UserController@login');
Route::any('/back/showLogin','Back\UserController@showLogin');
Route::any('/back/logout','Back\UserController@logout');
Route::any('/back/getImgCode','Back\UserController@getImgCode');


//back 主页
Route::group(['namespace' => 'Back','middleware' => ['guest']], function(){

    Route::any('/back/index','IndexController@index');//主页
    Route::any('/back/data','IndexController@data');//数据引导图
    Route::any('/back/getListArea','CommonController@getListArea');//获取省级数据
    Route::any('/back/upload','CommonController@upload');//上传图片


});

//权限
Route::group(['prefix'=>'power','namespace' => 'Back','middleware' => ['guest']], function(){

    Route::any('showrole','RoleController@showRole');//角色列表
    Route::any('showroleadd','RoleController@showRoleAdd');//显示角色新增
    Route::any('roleadd','RoleController@roleAdd');//角色新增
    Route::any('showroleedit','RoleController@showRoleEdit');//显示编辑
    Route::any('roleedit','RoleController@roleEdit');//编辑
    Route::any('roledel','RoleController@roleDel');//删除


    Route::any('showmenu','MenuController@showMenu');//菜单列表
    Route::any('showmenuadd','MenuController@showMenuAdd');//显示菜单新增
    Route::any('menuadd','MenuController@menuAdd');//菜单新增
    Route::any('showmenuedit','MenuController@showMenuEdit');//显示编辑
    Route::any('menuedit','MenuController@menuEdit');//编辑
    Route::any('menudel','MenuController@menuDel');//删除

    Route::any('showuser','UserController@showUser');//后台用户列表
    Route::any('showuseradd','UserController@showUserAdd');//显示后台用户新增
    Route::any('useradd','UserController@userAdd');//后台用户新增
    Route::any('showuseredit','UserController@showUserEdit');//显示编辑
    Route::any('useredit','UserController@userEdit');//编辑
    Route::any('userdel','UserController@userDel');//删除
    Route::any('showeditpass','UserController@showEditPass');//显示修改密码
    Route::any('editpass','UserController@editPass');//修改密码


});

//user
Route::group(['prefix'=>'user','namespace' => 'User','middleware' => ['guest']], function(){
    Route::any('test','SellerController@test');//test

    //医院
    Route::any('showbuyer','BuyerController@showBuyer');//采购方列表
    Route::any('exabuyer','BuyerController@exaBuyer');//采购审核
    Route::any('banusebuyer','BuyerController@banUseBuyer');//禁用
    Route::any('buyerDetail','BuyerController@buyerDetail');//医院详情



    //供应商
    Route::any('showseller','SellerController@showSeller');//供应商列表
    Route::any('exaseller','SellerController@exaSeller');//采购审核
    Route::any('banuseseller','SellerController@banUseSeller');//禁用
    Route::any('sellerDetail','SellerController@sellerDetail');//供应商详情

    //合伙人
    Route::any('showpartner','MemberController@showInviteMan');//合伙人列表
    Route::any('banUserCode','MemberController@banUserCode');//邀请码 禁用启用



    //新用户
    Route::any('shownewuser','MemberController@showNewUser');//未认证用户列表
    Route::any('trackNewUser','MemberController@trackNewUser');//跟踪新用户


});

//标书
Route::group(['prefix'=>'bid','namespace' => 'Bid','middleware' => ['guest']], function(){
    Route::any('test','BidController@test');//test

    //竞价
    Route::any('showBid','BidController@showBid');//竞价管理
    Route::any('banBid','BidController@banBid');//关闭竞价
    Route::any('changeBidStatus','BidController@changeBidStatus');//改变竞价状态
    Route::any('bidDetail','BidController@bidDetail');//标书详情




});



