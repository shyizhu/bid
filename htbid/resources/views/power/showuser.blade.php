<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/css/layui.css" media="all">
    <link rel="stylesheet" href="/css/admin.css" media="all">
    <link rel="stylesheet" href="/css/layer/layer.css" media="all">
    <script src="/js/jquery.js"></script>
</head>

<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header">后台用户</div>

        <div class="layui-card-body">
            <div style="padding-bottom: 10px;">
                <a href="/power/showuseradd" target="iframe">
                    <button class="layui-btn layuiadmin-btn-role" >添加</button>
                </a>
            </div>

            <table id="LAY-user-back-role" lay-filter="LAY-user-back-role"></table>
            <script type="text/html" id="buttonTpl">
                {{--{{#  if(d.check == true){ }}--}}
                {{--<button class="layui-btn layui-btn-xs">已审核</button>--}}
                {{--{{#  } else { }}--}}
                {{--<button class="layui-btn layui-btn-primary layui-btn-xs">未审核</button>--}}
                {{--{{#  } }}--}}
            </script>
            <script type="text/html" id="table-useradmin-admin">
                <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit"><i class="layui-icon layui-icon-edit"></i>编辑</a>
                <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><i class="layui-icon layui-icon-delete"></i>删除</a>
            </script>
        </div>
    </div>

    <div class="layui-col-md12">
        <div class="layui-card">
            <div class="layui-card-header">菜单列表</div>
            <div class="layui-card-body">
                <table class="layui-table">
                    <colgroup>
                        <col width="150">
                        <col width="150">
                        <col width="200">
                        <col width="200">

                        <col>
                    </colgroup>
                    <thead>
                    <tr>
                        <th>用户id</th>
                        <th>用户手机号</th>
                        <th>用户邮箱</th>
                        <th>角色</th>
                        <th>操作</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($userList as $k => $v)
                        <tr>
                            <td>{{$v['user_id']}}</td>
                            <td>{{$v['user_phone']}}</td>
                            <td>{{$v['user_email']}}</td>
                            <td>{{$v['role_name']}}</td>
                            <td>
                                <a href="/power/showuseredit?role_id={{$v['user_id']}}">
                                    <button class="layui-btn layuiadmin-btn-role">编辑</button>
                                </a>
                            </td>
                        </tr>
                    @endforeach


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>

</script>
</body>
</html>

