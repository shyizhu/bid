<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/css/layui.css" media="all">
    <link rel="stylesheet" href="/css/admin.css" media="all">
    <link rel="stylesheet" href="/css/layer/layer.css" media="all">
    <script src="/js/jquery.js"></script>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">新增菜单</div>
                <form method="post" action="/power/menuadd" >
                    <div class="layui-card-body" pad15>

                    <div class="layui-form" lay-filter="">
                        <div class="layui-form-item">
                            <label class="layui-form-label">上级菜单</label>
                            <div class="layui-input-inline">
                                <select  class="layui-input" name="menu_pid">
                                    <option value="0">一级页面</option>
                                    @foreach($menu as $k=>$v)
                                    <option value="{{$v['menu_id']}}">{{$v['menu_name']}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">菜单名称</label>
                            <div class="layui-input-inline">
                                <input name="menu_name" class="layui-input">
                            </div>
                            <div class="layui-form-mid layui-word-aux">2到10个字符</div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">菜单路径</label>
                            <div class="layui-input-inline">
                                <input  name="menu_path" class="layui-input">
                            </div>
                            <div class="layui-form-mid layui-word-aux">2到10个字符</div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">排序</label>
                            <div class="layui-input-inline">
                                <input  name="menu_sort" class="layui-input">
                            </div>
                            <div class="layui-form-mid layui-word-aux">0-99数字，数字越小排序靠前</div>

                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">icon</label>
                            <div class="layui-input-inline">
                                <input  name="menu_icon" class="layui-input">
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <div class="layui-input-block">
                                <button class="layui-btn" >确认</button>
                                <button type="reset" class="layui-btn layui-btn-primary" onclick="history.go(-1)">返回</button>
                            </div>
                        </div>
                    </div>

                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>

</script>
</body>
</html>