<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/css/layui.css" media="all">
    <link rel="stylesheet" href="/css/admin.css" media="all">
    <link rel="stylesheet" href="/css/layer/layer.css" media="all">
    <script src="/js/jquery.js"></script>
</head>

<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-card-header">菜单</div>

        <div class="layui-card-body">
            <div style="padding-bottom: 10px;">
                <a href="/power/showmenuadd" target="iframe">
                    <button class="layui-btn layuiadmin-btn-role" >添加</button>
                </a>
            </div>

            <table id="LAY-user-back-role" lay-filter="LAY-user-back-role"></table>
            <script type="text/html" id="buttonTpl">
                {{--{{#  if(d.check == true){ }}--}}
                {{--<button class="layui-btn layui-btn-xs">已审核</button>--}}
                {{--{{#  } else { }}--}}
                {{--<button class="layui-btn layui-btn-primary layui-btn-xs">未审核</button>--}}
                {{--{{#  } }}--}}
            </script>
            <script type="text/html" id="table-useradmin-admin">
                <a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="edit"><i class="layui-icon layui-icon-edit"></i>编辑</a>
                <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><i class="layui-icon layui-icon-delete"></i>删除</a>
            </script>
        </div>
    </div>

    <div class="layui-col-md12">
        <div class="layui-card">
            <div class="layui-card-header">菜单列表</div>
            <div class="layui-card-body">
                <table class="layui-table">
                    <colgroup>
                        <col width="150">
                        <col width="150">
                        <col width="200">
                        <col>
                    </colgroup>
                    <thead>
                    <tr>
                        <th>名称</th>
                        <th>路径</th>
                        <th>icon</th>
                        <th>排序</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($menuList as $k => $v)
                    <tr>
                        <td>{{$v['menu_name']}}</td>
                        <td>{{$v['menu_path']}}</td>
                        <td>{{$v['menu_icon']}}</td>
                        <td>{{$v['menu_sort']}}</td>
                    </tr>
                    @endforeach


                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>

</script>
</body>
</html>

