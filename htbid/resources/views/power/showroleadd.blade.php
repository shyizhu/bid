<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/css/layui.css" media="all">
    <link rel="stylesheet" href="/css/admin.css" media="all">
    <link rel="stylesheet" href="/css/layer/layer.css" media="all">
    <script src="/js/jquery.js"></script>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">新增角色</div>
                <form method="post" action="/power/roleadd" id="addform">
                <div class="layui-card-body" pad15>

                    <div class="layui-form" lay-filter="">
                        <div class="layui-form-item">
                            <label class="layui-form-label">角色名称</label>
                            <div class="layui-input-inline">
                                <input name="role_name" class="layui-input">
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label">权限范围</label>
                            <div class="layui-input-block" >


                                <?php foreach ($menuList as $k => $v){
                                // 如何判断 ,1, 是否在 ,12,4,21,453,12, 字符串中
                                // 问： strpos('1,2,3', '1');  的返回值：0 , 0 == false
                                if(strpos(','.$roleMenuId.',', ','.$v['menu_id'].',') !== FALSE)
                                    $check = 'checked="checked"';
                                else
                                    $check = '';
                                ?>
                                <?php echo str_repeat('&nbsp', 8*$v['leval']); ?>
                                <input <?php echo $check; ?> level_id="<?php echo $v['leval']; ?>" type="checkbox" data-id="<?php echo $v['menu_id']; ?>" value="<?php echo $v['menu_id']; ?>"  class="checkall"/>
                                <?php echo $v['menu_name']; ?><br />
                            <?php }; ?>

                            <!-- 隐藏域-->
                                {{--<input hidden name="roleid" value="{{$roleid}}" >--}}
                                {{--<input hidden  name="role_name" value="{{$roleData['name']}}" >--}}
                                <input hidden id="role_menu_id" name="role_menu_id" value="" >


                            </div>
                        </div>
                        <div class="layui-form-item">
                            <div class="layui-input-block">
                                <button class="layui-btn" onclick="formSubmit()">确认</button>
                                <button type="reset" class="layui-btn layui-btn-primary" onclick="history.go(-1)">返回</button>
                            </div>
                        </div>
                    </div>

                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    // 为所有的复选框绑定一个点击事件
    $(":checkbox").click(function(){
        // 先获取点击的这个level_id
        var tmp_level_id = level_id = $(this).attr("level_id");
        // 判断是选中还是取消
        if($(this).prop("checked"))
        {
            // 所有的子权限也选中
            $(this).nextAll(":checkbox").each(function(k,v){
                if($(v).attr("level_id") > level_id)
                    $(v).prop("checked", "checked");
                else
                    return false;
            });
            // 所有的上级权限也选中
            $(this).prevAll(":checkbox").each(function(k,v){
                if($(v).attr("level_id") < tmp_level_id)
                {
                    $(v).prop("checked", "checked");
                    tmp_level_id--; // 再找更上一级的
                }
            });
        }
        else
        {
            // 所有的子权限也取消
            $(this).nextAll(":checkbox").each(function(k,v){
                if($(v).attr("level_id") > level_id)
                    $(v).removeAttr("checked");
                else
                    return false;
            });
        }
    });
    //获取选中菜单id
    function formSubmit(){
        var obj = $("input[class=checkall]");
        var idArr = [];
        for(var i in obj){
            if(obj[i].checked){
                idArr.push($(obj[i]).attr('data-id'))
            }
        }
        $("#role_menu_id").val(idArr);
        $("#addform").submit();
    }

</script>
</body>
</html>