<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/css/layui.css" media="all">
    <link rel="stylesheet" href="/css/admin.css" media="all">
    <link rel="stylesheet" href="/css/layer/layer.css" media="all">
    <script src="/js/jquery.js"></script>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">竞价详情</div>
                <div class="layui-card-body" pad15>
                        <div class="layui-form" lay-filter="">
                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <label class="layui-form-label">项目名称</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input" value="{{$bid['bid_name']}}" disabled>
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">项目编号</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input" value="{{$bid['bid_sn']}}" disabled>
                                    </div>
                                </div>
                            </div>

                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <label class="layui-form-label">分类</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input" value="{{bidTypeDataMap($bid['bid_type'])}}" disabled>
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">收货地址</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input" value="{{provinceDataMap($bid['province_id']).'-'.cityDataMap($bid['city_id']).'-'.areaDataMap($bid['area_id'])}}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <label class="layui-form-label">详细地址</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input" value="{{$bid['bid_post_addr']}}" disabled>
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">联系人</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input" value="{{$bid['bid_contact_man']}}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <label class="layui-form-label">联系电话</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input" value="{{$bid['bid_contact_phone']}}" disabled>
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">交货时间</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input" value="{{bidArrivalTimeDataMap($bid['bid_arrival_time'])}}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <label class="layui-form-label">预算</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input"  value="{{$bid['bid_budget']}}"  disabled>
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">付款方式</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input"   value="{{bidPayTypeDataMap($bid['bid_pay_type'])}}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <label class="layui-form-label">项目备注</label>
                                    <div class="layui-input-inline">
                                        <textarea  name=""  class="layui-input"   disabled>
                                            {{$bid['bid_remark']}}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="layui-card">
                <div class="layui-card-header">产品列表</div>
                <div class="layui-card-body" pad15>
                    <table class="layui-table">
                        <colgroup>
                            <col width="150">
                            <col width="150">
                            <col width="200">
                            <col>
                        </colgroup>
                        <thead>
                        <tr>
                            <th>产品名称</th>
                            <th>品牌/型号</th>
                            <th>单位</th>
                            <th>数量</th>
                            <th>单价</th>
                            <th>产品说明</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($goods as $k => $v)
                            <tr>
                                <td>{{$v['goods_name']}}</td>
                                <td>{{$v['goods_brand_model']}}</td>
                                <td>{{$v['goods_unit']}}</td>
                                <td>{{$v['goods_price']}}</td>
                                <td>{{$v['goods_num']}}</td>
                                <td>{{$v['goods_remark']}}</td>

                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="layui-card">
                <div class="layui-card-header">竞价详情</div>
                <div class="layui-card-body" pad15>
                    <table class="layui-table">
                        <colgroup>
                            <col width="150">
                            <col width="150">
                            <col width="200">
                            <col>
                        </colgroup>
                        <thead>
                        <tr>
                            <th>供应商</th>
                            <th>金额</th>
                            <th>轮次</th>
                            <th>报价时间</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($record as $k => $v)
                            <tr>
                                <td>{{$v['user_company']}}</td>
                                <td>{{$v['price']}}</td>
                                <td>{{$v['rotation']}}</td>
                                <td>{{$v['add_time']}}</td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>

                </div>
                <div class="layui-form-item layui-layout-admin">
                    <div class="layui-input-block">
                        <div class="layui-footer" style="left: 0;">
                            <button type="reset" class="layui-btn" onclick="history.go(-1)">返回</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>

</script>
</body>
</html>