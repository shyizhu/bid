<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/css/layui.css" media="all">
    <link rel="stylesheet" href="/css/admin.css" media="all">
    <link rel="stylesheet" href="/css/layer/layer.css" media="all">
    <script src="/js/jquery.js"></script>
    <style>
        .table-td{
            width:100px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    </style>
</head>

<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-form layui-card-header layuiadmin-card-header-auto">
            <form method="get" action="/bid/showBid" id="getForm">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">订单编号</label>
                        <div class="layui-input-inline">
                            <input type="text" name="bid_sn" placeholder="昵称" class="layui-input" value="<?php echo !empty($_GET['bid_sn'])?$_GET['bid_sn']:'';?>">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">联系人电话</label>
                        <div class="layui-input-inline">
                            <input type="text" name="bid_contact_phone" placeholder="请输入" class="layui-input" value="<?php echo !empty($_GET['bid_contact_phone'])?$_GET['bid_contact_phone']:'';?>">
                        </div>
                    </div>
                    <input style="display: none;" id="formTask" type="text" name="task" value="<?php echo !empty($_GET['task'])?$_GET['task']:20;?>">

                    <div class="layui-inline">
                        <button class="layui-btn layuiadmin-btn-list" type="button" onclick="formSubmit()">
                            <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                        </button>

                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="layui-col-md12">
        <div class="layui-card">
            <div class="layui-card-header">默认风格</div>
            <div class="layui-card-body">
                <div class="layui-tab">
                    <ul class="layui-tab-title">
                        <li  data-task="20" class="{{$task == 20?'layui-this':''}}">竞价中</li>
                        <li  data-task="28" class="{{$task == 28?'layui-this':''}}">待医院确认(中标)</li>
                        <li  data-task="29" class="{{$task == 29?'layui-this':''}}">待供应商确认</li>

                        <li  data-task="30" class="{{$task == 30?'layui-this':''}}">流拍</li>
                        <li  data-task="40" class="{{$task == 40?'layui-this':''}}">效益作废</li>
                        <li  data-task="50" class="{{$task == 50?'layui-this':''}}">拒收</li>
                        <li  data-task="60" class="{{$task == 60?'layui-this':''}}">待确认</li>
                        <li  data-task="70" class="{{$task == 70?'layui-this':''}}">成交</li>

                    </ul>
                    <div class="layui-tab-content">
                        <table class="layui-table">
                            <thead>
                            <tr>
                                <th>id</th>
                                <th>项目名称</th>
                                <th>编号</th>
                                <th>发布公司</th>
                                <th>联系电话</th>
                                <th>参与人数</th>
                                <th>发布时间</th>

                                <th>状态</th>

                                <th>操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $k => $v)
                            <tr>
                            <td>{{$v['bid_id']}}</td>
                            <td class="table-td"><a href="/bid/bidDetail?bid_id={{$v['bid_id']}}">{{$v['bid_name']}}</a></td>
                            <td class="table-td">{{$v['bid_sn']}}</td>
                            <td>{{$v['bid_user_company']}}</td>
                            <td>{{$v['bid_contact_phone']}}</td>
                            <td>{{$v['bid_enlist_num']}}</td>
                            <td>{{$v['add_time']}}</td>
                            <td>
                                @if($v['is_del']== 0)
                                <button class="layui-btn layui-btn-primary layui-btn-xs">正常</button>
                                @else
                                    <button class="layui-btn layui-btn-primary layui-btn-xs">关闭</button>
                                @endif

                            </td>
                            <td>
                            @if($v['is_del']== 0)
                            <a href="/bid/banBid?bid_id={{$v['bid_id']}}&is_del=1&task={{$task}}" onclick="if(confirm('确定关闭此竞价吗？')==true){return true;}else{return false}">
                            <button class="layui-btn layui-btn-danger  layui-btn-xs">关闭</button>
                            </a>
                            @endif

                             @if($v['bid_status'] > 20)
                                    <a href="/bid/changeBidStatus?bid_id={{$v['bid_id']}}&bid_status=30&task={{$task}}" onclick="if(confirm('确定流拍吗？')==true){return true;}else{return false}">
                                        <button class="layui-btn layui-btn-warm  layui-btn-xs">流拍</button>
                                    </a><a href="/bid/changeBidStatus?bid_id={{$v['bid_id']}}&bid_status=40&task={{$task}}" onclick="if(confirm('确定效益作废吗？')==true){return true;}else{return false}">
                                        <button class="layui-btn layui-btn-normal  layui-btn-xs">效益作废</button>
                                    </a><a href="/bid/changeBidStatus?bid_id={{$v['bid_id']}}&bid_status=50&task={{$task}}" onclick="if(confirm('确定拒收吗？')==true){return true;}else{return false}">
                                        <button class="layui-btn layui-btn-primary layui-btn-xs">拒收</button>

                                    </a><a href="/bid/changeBidStatus?bid_id={{$v['bid_id']}}&bid_status=70&task={{$task}}" onclick="if(confirm('确定成交吗？')==true){return true;}else{return false}">
                                        <button class="layui-btn   layui-btn-xs">成交</button>
                                    </a>
                             @endif

                            </td>

                            </tr>
                            @endforeach


                            </tbody>
                        </table>
                        <div>
                            {{ $bid->appends(['task'=>$task])->links() }}

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    function formSubmit(){
        let task = $('.layui-tab-title .layui-this').attr("data-task");

            $('#formTask').val(task);

       $("#getForm").submit();
    }

    $(function() {
        $('.layui-tab-title li').on('click', function () {
            $(this).addClass('layui-this').siblings().removeClass('layui-this')
            formSubmit();
        })
    })
</script>
</body>
</html>

