<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/css/layui.css" media="all">
    <link rel="stylesheet" href="/css/admin.css" media="all">
    <link rel="stylesheet" href="/css/layer/layer.css" media="all">
    <script src="/js/jquery.js"></script>
</head>

<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-form layui-card-header layuiadmin-card-header-auto">
            <form method="get" action="/user/showseller">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">供应商名称</label>
                        <div class="layui-input-inline">
                            <input type="text" name="company_name" placeholder="昵称" class="layui-input" value="<?php echo !empty($_GET['company_name'])?$_GET['company_name']:'';?>">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">电话</label>
                        <div class="layui-input-inline">
                            <input type="text" name="user_phone" placeholder="请输入" class="layui-input" value="<?php echo !empty($_GET['user_phone'])?$_GET['user_phone']:'';?>">
                        </div>
                    </div>

                    <div class="layui-inline">
                        <button class="layui-btn layuiadmin-btn-list" type="submit">
                            <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                        </button>

                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="layui-col-md12">
        <div class="layui-card">
            <div class="layui-card-body">
                <table class="layui-table">

                    <thead>
                    <tr>
                        <th>供应商名称</th>
                        <th>用户名</th>
                        <th>电话</th>
                        <th>地区</th>
                        <th>法人</th>
                        <th>注册时间</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $k => $v)
                        <tr>
                            <td><a href="/user/sellerDetail?user_id={{$v['user_id']}}">{{$v['company_name']}}</a></td>
                            <td>{{$v['user_name']}}</td>
                            <td>{{$v['user_phone']}}</td>
                            <th>{{$v['area']}}</th>
                            <th>{{$v['legal_person']}}</th>
                            <th>{{$v['add_time']}}</th>

                            <th>
                                @if($v['user_status'] == 1)
                                    <button class="layui-btn layui-btn-primary layui-btn-xs">审核中</button>
                                @elseif($v['user_status'] ==2)
                                    <button class="layui-btn layui-btn-primary layui-btn-xs">通过</button>
                                    @if($v['is_del'] == 1)
                                        <button class="layui-btn layui-btn-primary layui-btn-xs">禁用</button>
                                    @endif
                                @elseif($v['user_status'] ==3)
                                    <button class="layui-btn layui-btn-primary layui-btn-xs">驳回</button>
                                @endif
                            </th>
                            <td>
                                @if($v['user_status'] == 1)
                                    <a href="/user/exaseller?user_id={{$v['user_id']}}&user_status=2" onclick="if(confirm('确定通过？')==true){return true;}else{return false}">
                                        <button class="layui-btn layui-btn-xs">通过</button>
                                    </a>
                                    <a href="/user/exaseller?user_id={{$v['user_id']}}&user_status=3" onclick="if(confirm('确定驳回？')==true){return true;}else{return false}">
                                        <button class="layui-btn layui-btn-xs">驳回</button>
                                    </a>
                                @elseif($v['user_status'] ==2)
                                    @if($v['is_del'] == 1)
                                        <a href="/user/banuseseller?user_id={{$v['user_id']}}&is_del=0" onclick="if(confirm('确定启用？')==true){return true;}else{return false}">
                                            <button class="layui-btn layui-btn-xs">启用</button>
                                        </a>
                                    @else
                                        <a href="/user/banuseseller?user_id={{$v['user_id']}}&is_del=1" onclick="if(confirm('确定禁用？')==true){return true;}else{return false}">
                                            <button class="layui-btn layui-btn-danger layui-btn-xs">禁用</button>
                                        </a>
                                    @endif

                                @elseif($v['user_status'] ==3)

                                @endif
                            </td>
                        </tr>
                           @endforeach


                    </tbody>
                </table>
                <div>
                    {{ $page }}

                </div>
            </div>
        </div>
    </div>
</div>

<script>

</script>
</body>
</html>

