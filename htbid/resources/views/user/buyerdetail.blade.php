<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/css/layui.css" media="all">
    <link rel="stylesheet" href="/css/admin.css" media="all">
    <link rel="stylesheet" href="/css/layer/layer.css" media="all">
    <script src="/js/jquery.js"></script>
</head>
<body>

<div class="layui-fluid">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md12">
            <div class="layui-card">
                <div class="layui-card-header">医院详情</div>
                <div class="layui-card-body" pad15>
                    <form method="post" action="/power/useradd">
                        <div class="layui-form" lay-filter="">
                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <label class="layui-form-label">医院名称</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input" value="{{$buyer['company_name']}}" disabled>
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">登录名</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input" value="{{$buyer['user_name']}}" disabled>
                                    </div>
                                </div>
                            </div>

                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <label class="layui-form-label">负责人</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input" value="{{$buyer['agent']}}" disabled>
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">所在省市</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input" value="{{$buyer['area']}}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <label class="layui-form-label">通信地址</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input" value="{{$buyer['post_addr']}}" disabled>
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">医院类型</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input" value="{{$buyer['hospitalType']}}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <label class="layui-form-label">税务登记号</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input" value="{{$buyer['tax_register_num']}}" disabled>
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">邀请码</label>
                                    <div class="layui-input-inline">
                                        <input  name=""  class="layui-input" value="{{$buyer['invite_code']}}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <label class="layui-form-label">营业执照扫描件</label>
                                    <div class="layui-input-inline">
                                        <img src="{{$buyer['yingye']}}" style="height:200px;"/>
                                    </div>
                                </div>
                                <div class="layui-inline">
                                    <label class="layui-form-label">医疗机构执业许可证</label>
                                    <div class="layui-input-inline">
                                        <img src="{{$buyer['xvke']}}" style="height:200px;"/>
                                    </div>
                                </div>
                            </div>

                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button type="reset" class="layui-btn" onclick="history.go(-1)">返回</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

</script>
</body>
</html>