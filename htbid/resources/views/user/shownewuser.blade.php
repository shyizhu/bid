<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/css/layui.css" media="all">
    <link rel="stylesheet" href="/css/admin.css" media="all">
    <link rel="stylesheet" href="/css/layer/layer.css" media="all">
    <script src="/js/jquery.js"></script>
</head>

<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-form layui-card-header layuiadmin-card-header-auto">
            <form method="get" action="/user/shownewuser">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">微信昵称</label>
                        <div class="layui-input-inline">
                            <input type="text" name="user_nickname" placeholder="昵称" autocomplete="off" class="layui-input" value="<?php echo !empty($_GET['user_nickname'])?$_GET['user_nickname']:'';?>">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">电话</label>
                        <div class="layui-input-inline">
                            <input type="text" name="user_phone" placeholder="请输入" autocomplete="off" class="layui-input" value="<?php echo !empty($_GET['user_phone'])?$_GET['user_phone']:'';?>">
                        </div>
                    </div>

                    <div class="layui-inline">
                        <button class="layui-btn layuiadmin-btn-list" type="submit">
                            <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                        </button>

                    </div>
                    {{--<div class="layui-inline">--}}
                        {{--<button class="layui-btn layui-btn-sm">已跟踪</button>--}}
                        {{--<button class="layui-btn layui-btn-primary layui-btn-sm">未跟踪</button>--}}
                    {{--</div>--}}
                </div>
            </form>
        </div>
    </div>
    <div class="layui-col-md12">
        <div class="layui-card">
            <div class="layui-card-body">
                <table class="layui-table">
                    <colgroup>
                        <col width="150">
                        <col width="150">
                        <col width="200">
                        <col>
                    </colgroup>
                    <thead>
                    <tr>
                        <th>微信昵称</th>
                        <th>电话</th>
                        <th>注册时间</th>
                        <th>状态</th>
                        <th>操作</th>

                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user as $k => $v)
                    <tr>
                    <td>{{$v['user_nickname']}}</td>
                    <td>{{$v['user_phone']}}</td>
                    <td>{{$v['add_time']}}</td>
                    <td>
                            @if($v['is_track'] == 1)
                                <button class="layui-btn layui-btn-primary layui-btn-xs">已跟踪</button>
                            @elseif($v['is_track'] ==0)
                                <button class="layui-btn layui-btn-primary layui-btn-xs">未跟踪</button>
                            @endif
                    </td>
                    <td>
                        @if($v['is_track'] == 1)
                        @elseif($v['is_track'] ==0)
                        <a href="/user/trackNewUser?user_id={{$v['user_id']}}">
                            <button class="layui-btn layui-btn-xs">跟踪</button>
                        </a>
                        @endif
                    </td>

                    </tr>
                    @endforeach

                    </tbody>

                </table>
                <div>
                        {{ $user->links() }}

                </div>
            </div>

        </div>
    </div>
</div>

<script>

</script>
</body>
</html>

