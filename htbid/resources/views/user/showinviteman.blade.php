<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="stylesheet" href="/css/layui.css" media="all">
    <link rel="stylesheet" href="/css/admin.css" media="all">
    <link rel="stylesheet" href="/css/layer/layer.css" media="all">
    <script src="/js/jquery.js"></script>
</head>

<body>

<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-form layui-card-header layuiadmin-card-header-auto">
            <form method="get" action="/user/showinviteman">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">微信昵称</label>
                        <div class="layui-input-inline">
                            <input type="text" name="user_nickname" placeholder="昵称" class="layui-input" value="<?php echo !empty($_GET['user_nickname'])?$_GET['user_nickname']:'';?>">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">电话</label>
                        <div class="layui-input-inline">
                            <input type="text" name="user_phone" placeholder="请输入" class="layui-input" value="<?php echo !empty($_GET['user_phone'])?$_GET['user_phone']:'';?>">
                        </div>
                    </div>

                    <div class="layui-inline">
                        <button class="layui-btn layuiadmin-btn-list" type="submit">
                            <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                        </button>

                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="layui-col-md12">
        <div class="layui-card">
            <div class="layui-card-body">
                <table class="layui-table">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>微信昵称</th>
                        <th>电话</th>
                        <th>邀请码</th>

                        <th>推荐人数</th>
                        <th>注册时间</th>
                        <th>状态</th>

                        <th>操作</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($invite as $k => $v)
                    <tr>
                    <td>{{$v['user_id']}}</td>
                    <td>{{$v['user_nickname']}}</td>
                    <td>{{$v['user_phone']}}</td>
                    <th>{{isset($code_column[$v['user_id']])?$code_column[$v['user_id']]:'-'}}</th>
                    <td>{{isset($num_column[$v['user_id']])?$num_column[$v['user_id']]:'-'}}</td>
                    <td>{{$v['add_time']}}</td>
                    <td>
                        @if(isset($status_column[$v['user_id']]) && $status_column[$v['user_id']]== 1)
                            <button class="layui-btn layui-btn-primary layui-btn-xs">禁用</button>

                        @elseif(isset($status_column[$v['user_id']]) && $status_column[$v['user_id']]== 0)
                            <button class="layui-btn layui-btn-primary layui-btn-xs">启用</button>

                        @endif
                    </td>
                    <td>
                        @if(isset($status_column[$v['user_id']]) && $status_column[$v['user_id']]== 1)
                            <a href="/user/banUserCode?user_id={{$v['user_id']}}&code_status=0" onclick="if(confirm('确定启用此用户邀请码？')==true){return true;}else{return false}">
                                <button class="layui-btn layui-btn-xs">启用</button>
                            </a>
                        @elseif(isset($status_column[$v['user_id']]) && $status_column[$v['user_id']]== 0)
                            <a href="/user/banUserCode?user_id={{$v['user_id']}}&code_status=1" onclick="if(confirm('确定禁用此用户邀请码？')==true){return true;}else{return false}">
                                <button class="layui-btn layui-btn-danger  layui-btn-xs">禁用</button>
                            </a>
                        @endif

                    </td>

                    </tr>
                    @endforeach


                    </tbody>
                </table>
                <div>
                    {{ $invite->links() }}

                </div>
            </div>
        </div>
    </div>
</div>

<script>

</script>
</body>
</html>

