<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>医竞采</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="/css/layui.css" media="all">
  <link rel="stylesheet" href="/css/admin.css" media="all">
  <link rel="stylesheet" href="/css/layer/layer.css" media="all">
  <script src="/js/jquery.js"></script>

</head>
<body class="layui-layout-body">
  
  <div id="LAY_app">
    <div class="layui-layout layui-layout-admin">
      <div class="layui-header">
        <!-- 头部区域 -->
        <ul class="layui-nav layui-layout-left">
          <li class="layui-nav-item layadmin-flexible" lay-unselect>
            <a href="javascript:;" layadmin-event="flexible" title="侧边伸缩">
              <i class="layui-icon layui-icon-shrink-right" id="LAY_app_flexible"></i>
            </a>
          </li>
          <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <a href="/back/index" target="_blank" title="前台">
              <i class="layui-icon layui-icon-website"></i>
            </a>
          </li>
          <li class="layui-nav-item" lay-unselect>
            <a  layadmin-event="refresh" title="刷新" onclick="shuaxin()">
              <i class="layui-icon layui-icon-refresh-3"></i>
            </a>
          </li>
          <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <input type="text" placeholder="搜索..." autocomplete="off" class="layui-input layui-input-search" layadmin-event="serach" lay-action="template/search.html?keywords="> 
          </li>
        </ul>
        <ul class="layui-nav layui-layout-right" lay-filter="layadmin-layout-right">

          <li class="layui-nav-item" id="user-set">
            <a href="javascript:;">
              <cite>{{$adminInfo['user_phone']}}</cite>
                <span class="layui-nav-more"></span>
            </a>
            <dl class="layui-nav-child" id="user-set-list">
              <dd><a href="set/user/info.html">基本资料</a></dd>
              <dd><a href="/power/showeditpass" target="iframe">修改密码</a></dd>
              <hr>
              <dd  style="text-align: center;"><a href="/back/logout">退出</a></dd>
            </dl>
          </li>
          
          <li class="layui-nav-item layui-hide-xs" lay-unselect>
            <a href="javascript:;" layadmin-event="about"><i class="layui-icon layui-icon-more-vertical"></i></a>
          </li>
          <li class="layui-nav-item layui-show-xs-inline-block layui-hide-sm" lay-unselect>
            <a href="javascript:;" layadmin-event="more"><i class="layui-icon layui-icon-more-vertical"></i></a>
          </li>
        </ul>
      </div>
      
      <!-- 侧边菜单 -->
      <div class="layui-side layui-side-menu">
        <div class="layui-side-scroll">
          <div class="layui-logo" lay-href="home/console.html">
            <span>医麦采</span>
          </div>
          
          <ul class="layui-nav layui-nav-tree" lay-shrink="all" id="LAY-system-side-menu" lay-filter="layadmin-system-side-menu">

            <!-- 根据权限显示菜单-->
            @if(!empty($adminInfo['parentTree']))
              @foreach($adminInfo['parentTree'] as $k=>$v)
                <li data-name="home" class="layui-nav-item ">
                  <a href="javascript:;" lay-tips="主页" lay-direction="2">
                    <i class="layui-icon {{$v['menu_icon']}}"></i>
                    <cite>{{$v['menu_name']}}</cite>
                    <span class="layui-nav-more"></span>
                  </a>
                     <dl class="layui-nav-child">

                    @foreach($adminInfo['kidTree'] as $k1=>$v1)
                      @if($v1['menu_pid'] == $v['menu_id'])
                           <dd data-name="console" class="">
                             <a href="{{$v1['menu_path']}}" target="iframe">{{$v1['menu_name']}}</a>
                           </dd>

                      @endif
                    @endforeach

                      </dl>
                    </li>
              @endforeach
            @endif

           </ul>
        </div>
      </div>

      <!-- 页面标签 -->
      <div class="layadmin-pagetabs" id="LAY_app_tabs">
        <div class="layui-icon layadmin-tabs-control layui-icon-prev" layadmin-event="leftPage"></div>
        <div class="layui-icon layadmin-tabs-control layui-icon-next" layadmin-event="rightPage"></div>
        <div class="layui-icon layadmin-tabs-control layui-icon-down">
          <ul class="layui-nav layadmin-tabs-select" lay-filter="layadmin-pagetabs-nav">
            <li class="layui-nav-item" lay-unselect>
              <a href="javascript:;"></a>
              <dl class="layui-nav-child layui-anim-fadein">
                <dd layadmin-event="closeThisTabs"><a href="javascript:;">关闭当前标签页</a></dd>
                <dd layadmin-event="closeOtherTabs"><a href="javascript:;">关闭其它标签页</a></dd>
                <dd layadmin-event="closeAllTabs"><a href="javascript:;">关闭全部标签页</a></dd>
              </dl>
            </li>
          </ul>
        </div>
        <div class="layui-tab" lay-unauto lay-allowClose="true" lay-filter="layadmin-layout-tabs">
          <ul class="layui-tab-title" id="LAY_app_tabsheader">
            <li lay-id="home/console.html" lay-attr="home/console.html" class="layui-this"><i class="layui-icon layui-icon-home"></i></li>
          </ul>
        </div>
      </div>
      

      <!-- 主体内容 -->
      <div class="layui-body" id="LAY_app_body">
        <div class="layadmin-tabsbody-item layui-show">
          <iframe id="mainiframe" name="iframe" src="/back/data" frameborder="0" class="layadmin-iframe"></iframe>
        </div>
      </div>
      
      <!-- 辅助元素，一般用于移动设备下遮罩 -->
      <div class="layadmin-body-shade" layadmin-event="shade"></div>
    </div>
  </div>
<script>
    function shuaxin(){
      document.getElementById('mainiframe').contentWindow.location.reload(true);

    }
    //二级菜单导航
    $(function() {
        //一级
        $(".layui-nav-tree .layui-nav-item").click(function(){
            if ($(this).hasClass("layui-nav-itemed")) {
                $(this).removeClass("layui-nav-itemed");
            }else{
                $(this).addClass("layui-nav-itemed").siblings().removeClass("layui-nav-itemed");

            }

        })

        //二级
        $(".layui-nav-child dd").click(function () {
            var $this = $(this);
            $this.addClass("layui-this").siblings().removeClass("layui-this");

        })

        $(".layui-nav-child dd").bind("click",function(event){
            event.stopPropagation();
        });
    })

    //个人菜单
    //鼠标的移入移出
    $("#user-set").mouseover(function (){
        var $this = $(this);
        $this.find('span').addClass("layui-nav-mored");
        $this.find('dl').addClass("layui-show");

    }).mouseout(function (){
        var $this = $(this);
        $this.find('span').removeClass("layui-nav-mored");

        $this.find('dl').removeClass("layui-show");
    });


</script>



  
  
</body>
</html>


