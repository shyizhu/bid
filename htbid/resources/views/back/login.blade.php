<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>登入 - 医竞采</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <link rel="stylesheet" href="/css/layui.css" media="all">
  <link rel="stylesheet" href="/css/admin.css" media="all">
  <link rel="stylesheet" href="/css/login.css" media="all">
  <link rel="stylesheet" href="/css/layer/layer.css" media="all">

</head>
<body>

  <div class="layadmin-user-login layadmin-user-display-show" id="LAY-user-login" style="display: none;">

    <div class="layadmin-user-login-main">
      <div class="layadmin-user-login-box layadmin-user-login-header">
        <h2>医竞采</h2>
        <p>医竞采 后台登录系统</p>
      </div>
      <form id='loginform' action='/back/login' method="post">
        <input hidden name="csrf_token" value="{{ csrf_token() }}">
      <div class="layadmin-user-login-box layadmin-user-login-body layui-form">
        <div class="layui-form-item">
          <label class="layadmin-user-login-icon layui-icon layui-icon-username" for="LAY-user-login-username"></label>
          <input type="text" name="user_phone" id="LAY-user-login-username" lay-verify="required" placeholder="用户名" class="layui-input">
        </div>
        <div class="layui-form-item">
          <label class="layadmin-user-login-icon layui-icon layui-icon-password" for="LAY-user-login-password"></label>
          <input type="password" name="user_pass" id="LAY-user-login-password" lay-verify="required" placeholder="密码" class="layui-input">
        </div>
        <div class="layui-form-item">
          <div class="layui-row">
            <div class="layui-col-xs7">
              <label class="layadmin-user-login-icon layui-icon layui-icon-vercode" for="LAY-user-login-vercode"></label>
              <input type="text" name="vercode" id="LAY-user-login-vercode" lay-verify="required" placeholder="图形验证码" class="layui-input">
            </div>
            <div class="layui-col-xs5">
              <div style="margin-left: 10px;">
                <img src="/back/getImgCode?csrfToken={{ csrf_token() }}" class="layadmin-user-login-codeimg" id="imgcode" onclick="reimg()" >
              </div>
            </div>
          </div>
        </div>
        <div class="layui-form-item" style="margin-bottom: 20px;">
          {{--<input type="checkbox" name="remember" lay-skin="primary" title="记住密码">--}}
          {{--<a href="forget.html" class="layadmin-user-jump-change layadmin-link" style="margin-top: 7px;">忘记密码？</a>--}}
        </div>
        <div class="layui-form-item">
          <button id="tijiao" class="layui-btn layui-btn-fluid" lay-submit lay-filter="LAY-user-login-submit">登 入</button>
        </div>
        <div class="layui-trans layui-form-item layadmin-user-login-other">
          <label>申请登录</label>
          
          
          <a class="layadmin-user-jump-change layadmin-link">联系管理员</a>
        </div>
      </div>
      </form>

    </div>
    
    <div class="layui-trans layadmin-user-login-footer">
      
      <p>© 2019 <a href="" target="_blank">sapuo.com</a></p>
      <p>
        
      </p>
    </div>
    
    
    
  </div>
  <!--正确提示

  <div class="layui-layer layui-layer-dialog layui-layer-border layui-layer-msg"
       style="z-index: 19891017; top: 15px; left: 475px;">
    <div  class="layui-layer-content layui-layer-padding">
      <i class="layui-layer-ico layui-layer-ico1"></i>为了方便演示，用户名密码可随意输入
    </div>
    <span class="layui-layer-setwin"></span>
  </div>

  <div class="layui-layer layui-layer-dialog layui-layer-border layui-layer-msg"
       style="z-index: 19891017; top: 137px; left: 541.5px;">
    <div class="layui-layer-content layui-layer-padding">
      <i class="layui-layer-ico layui-layer-ico5"></i>必填项不能为空
    </div><span class="layui-layer-setwin"></span>
  </div>
  <script>

    错误提示-->



</body>
<script>
  function reimg(){
    var img = document.getElementById("imgcode");
    img.src = "/back/getImgCode?rnd=" + Math.random()+"&csrfToken={{ csrf_token() }}";
  }
</script>
</html>