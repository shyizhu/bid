<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    echo '非法参数！';
});//H5首页

Route::any('/getImgCode','Web\CommonController@getImgCode');

Route::options('/{all}', function(Request $request) {
//    $origin = $request->header('ORIGIN', '*');
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Credentials: true");
    header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
    header('Access-Control-Allow-Headers: Origin, Access-Control-Request-Headers, SERVER_NAME, Access-Control-Allow-Headers, cache-control, token, X-Requested-With, Content-Type, Accept, Connection, User-Agent, Cookie');
})->where(['all' => '([a-zA-Z0-9-]|/)+']);

/*
 * 商城后台
 */
//商品管理
Route::group(['prefix'=>'product','namespace' => 'Admin\Product'], function(){
    Route::any('test','ProductController@test');//test

    //商品管理
    Route::any('getProductList','ProductController@getProductList');//
    Route::any('productAdd','ProductController@productAdd');//
    Route::any('productEdit','ProductController@productEdit');//
    Route::any('productDel','ProductController@productDel');//
    Route::any('productExamine','ProductController@productExamine');//


    //属性管理
    Route::any('getAttrById','AttributeController@getAttrById');//
    Route::any('attrAdd','AttributeController@attrAdd');//
    Route::any('attrEdit','AttributeController@attrEdit');//
    Route::any('attrDel','AttributeController@attrDel');//

    //属性组管理
    Route::any('getAttrGroupList','AttributeController@getAttrGroupList');//
    Route::any('attrGroupAdd','AttributeController@attrGroupAdd');//
    Route::any('attrGroupEdit','AttributeController@attrGroupEdit');//
    Route::any('attrGroupDel','AttributeController@attrGroupDel');//

    //品牌管理 brand
    Route::any('getBrandList','BrandController@getBrandList');//
    Route::any('brandAdd','BrandController@brandAdd');//
    Route::any('brandEdit','BrandController@brandEdit');//
    Route::any('brandDel','BrandController@brandDel');//

    //菜单管理
    Route::any('getCateList','CategoryController@getCateList');//
    Route::any('cateAdd','CategoryController@cateAdd');//
    Route::any('cateEdit','CategoryController@cateEdit');//
    Route::any('cateDel','CategoryController@cateDel');//

});


//后台接口
Route::group(['prefix'=>'admin','namespace' => 'Admin'], function(){
    Route::any('test','AdminController@test');//test
    Route::any('adminLogin','AdminController@adminLogin');//后台登录

    //后台用户
    Route::any('getAdminList','AdminController@getAdminList');//
    Route::any('adminAdd','AdminController@adminAdd');//
    Route::any('adminEdit','AdminController@adminEdit');//
    Route::any('adminDel','AdminController@adminDel');//

    //角色管理
    Route::any('getRoleList','RoleController@getRoleList');//
    Route::any('roleAdd','RoleController@roleAdd');//
    Route::any('roleEdit','RoleController@roleEdit');//
    Route::any('roleDel','RoleController@roleDel');//

    //菜单管理
    Route::any('getMenuList','MenuController@getMenuList');//
    Route::any('menuAdd','MenuController@menuAdd');//
    Route::any('menuDel','MenuController@menuDel');//


});


//前台小程序，h5,pc api
Route::group(['prefix'=>'index','namespace' => 'User'], function(){

    Route::any('test','IndexController@test');//test

    Route::any('upfile','IndexController@upfile');//上传图片

    Route::any('getProList','IndexController@getProList');//获取省列表

    Route::any('getListArea','IndexController@getListArea');//省市区三级联动

    Route::any('getHospitalType1','IndexController@getHospitalType1');//获取医院类型

    Route::any('getHospitalType2','IndexController@getHospitalType2');//医院类型二级联动

    Route::any('indexdata','IndexController@indexData');//主页数据

    Route::any('typeBidList','IndexController@typeBidList');//单分类数据

    Route::any('tabBidList','IndexController@tabBidList');//招标中，已完成，已收藏标书列表

    Route::any('linePlay', 'IndexController@linePlay');//在线直播
    Route::any('linPlayCompanyInfo', 'IndexController@linPlayCompanyInfo');//在线直播 获取公司信息


});

//登录注册
Route::group(['prefix'=>'login','namespace' => 'User'], function(){
    Route::any('test', 'LoginController@test');

    Route::any('getSessionKey', 'LoginController@getSessionKey');//获取头像昵称信息
    Route::any('getUserPhone', 'LoginController@getUserPhone');//获取手机号
    Route::any('sendSmsCode', 'LoginController@sendSmsCode');//发送验证码
    Route::any('getUserInfoByToken', 'LoginController@getUserInfoByToken');//通过token 获取用户信息
    Route::any('userLogin', 'LoginController@userLogin');//账号密码登录


    Route::any('sellerRegister', 'LoginController@sellerRegister');//供应商注册 seller
    Route::any('buyerRegister', 'LoginController@buyerRegister');//医院注册  buyer

    Route::any('editPass', 'LoginController@editPass');//修改密码

    Route::any('phoneLogin', 'LoginController@phoneLogin');//快捷登录/注册 手机号



});

//标书
Route::group(['prefix'=>'bid','namespace' => 'Bid'], function(){
    Route::any('test', 'BidController@test');

    Route::any('pushBidInit', 'BidController@pushBidInit');//发布标书 初始化页面

    Route::any('pushBid', 'BidController@pushBid');//发布标书

    Route::any('enrollBid', 'BidController@enrollBid');//报名投标

    Route::any('getGoWhere', 'BidController@getGoWhere');//获取应该跳转的页面

    Route::any('getBidDetail', 'BidController@getBidDetail');//标书详情

    Route::any('pushPrice', 'BidController@pushPrice');//报价

    Route::any('statusBidList', 'BidController@statusBidList');//状态标书列表

    Route::any('isTrue', 'BidController@isTrue');//医院供应商确认


    Route::any('buyerIsTrue', 'BidController@buyerIsTrue');//医院确认

    Route::any('sellerIsTrue', 'BidController@sellerIsTrue');//供应商确认





});

//我的
Route::group(['prefix'=>'user','namespace' => 'User'], function(){
    Route::any('test', 'UserController@test');

    Route::any('getUserByToken', 'UserController@getUserByToken');//获取用户信息

    Route::any('myInit', 'UserController@myInit');//我的init

    Route::any('goInvite', 'UserController@goInvite');//开启邀请码

    Route::any('addCollect', 'UserController@addCollect');//添加收藏 ，取消收藏

    Route::any('getInviteInfo', 'UserController@getInviteInfo');//合伙人信息

    Route::any('getNoticeList', 'UserController@getNoticeList');//公告列表

});





