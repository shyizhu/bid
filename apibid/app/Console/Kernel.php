<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Http\Models\Biding;

use DB;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->call(function () {
            $bidModel = new Biding();
            //今天早上10点
            $day10= date("Y-m-d H:i:s",strtotime(date("Y-m-d"))+3600*10);

            $bid = DB::table('biding')->where('bid_status',20)
                ->where('bid_start_time',$day10)->get();


            if(!empty($bid)){
               foreach ($bid as $k =>$v){

                   //给发表人发开标提醒
                   $createUser = DB::table('user')->where('user_id',$v['bid_user_id'])->first();
                   if(!empty($createUser)){
                       $c = '【医竞采】您发布的，'.$v['bid_name'].'项目，还有10分钟开标，请及时关注！';
                       $this->f_bidSendMsg($createUser['user_phone'],$c);
                   }

                   //给参与人发短信
                   $ids = DB::table('biding_enlist')->where('bid_id',$v['bid_id'])->pluck('user_id');
                   if(!empty($ids)){
                       $msg = '【医竞采】您报名的，'.$v['bid_name'].'项目，还有10分钟开标，请及时关注！';
                       $enlistUser = DB::table('user')->whereIn('user_id',$ids)->get()->toArray();

                       foreach ($enlistUser as $v1){
                           $this->f_bidSendMsg($v1['user_phone'],$msg);

                       }
                   }

                }

            }
            DB::table('token')->insertGetId(['key'=>232323,'val'=>424242]);

        })->dailyAt('9:50');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    //发送通知短信
    protected function f_bidSendMsg($phone,$content){

        $params = array();
        $url ='http://115.28.50.135:8888/sms.aspx';
        $content = $content;

        $params=[
            'userid'=>'4259',
            'account'=>'北京医麦科技',
            'password'=>'123456',
            'mobile'=>$phone,
            'content'=>$content,
            'sendTime'=>'',
            'action'=>'send',
            'extno'=>'',
        ];

        $rs = $this->http_request($url,$params);
        $arr = $this->xmlToArray($rs);
        if($arr['returnstatus']== 'Success'){
            return true;
        }

    }

    protected function http_request($url,$data = null,$headers=array())
    {
        $curl = curl_init();
        if( count($headers) >= 1 ){
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);

        if (!empty($data)){
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($curl);
        curl_close($curl);
        return $output;
    }

    protected function xmlToArray($xml)
    {
        libxml_disable_entity_loader(true);
        $data = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        return $data;
    }



}
