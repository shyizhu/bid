<?php
namespace App\Http\Services;
use App\Http\Controllers\Controller;
use Config;
use Cookie;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Redis;


class TokenService
{

    public function __construct(){
    }

    // token存入数据redis
    public function tokenForReids($data, $type = 0,$time = ''){

        $token = $this->createToken();
        $redis = $this->redis();

        $redish = Config::get('key.redishkey');

        if (!empty($info['user_token'])) {
            $redis->hDel($redish,$info['user_token']);
        }

        $info['user_token'] = $token;
        $json = json_encode($info);
        //$redis->setex($token, $time, $json);
        $redis->hset($redish,$token,$json);
        return $token;
    }

    //生成token
    private function createToken()
    {
//        if ($this->mobile == '15810867923') {
//            return 'fa40aa9b10033591b85c76e03d3b489b';
//        }
        return md5(uniqid(rand()));
    }
    //获取redis 对象
    public function redis($redisConnectTag = 'default'){
        return Redis::connection($redisConnectTag);
    }
}
