<?php
namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use DB;
use App\Libraries\newsmsapi;
use App\Libraries\Captcha;
use Config;
use App\Http\Models\User;
use App\Http\Models\Biding;



class UserController extends BaseController
{

    private $request;


    private $captcha_expire_time = 180;

    public function __construct()
    {
        $this->request = $this->requestAll();
        parent::__construct();
    }

    public function  test()
    {
        echo makeInviteCode();

    }
    //添加收藏
    public function addCollect(){
        $request = $this->request;
        $userInfo = $this->userInfo();
        $data = [
            'user_id'=>$userInfo['user_id'],//会员id
            'type_id'=>$request['bid_id'],//biddingid
        ];

        //收藏  0收藏 1取消收藏
        $coll = DB::table('collect')->where($data)->first();
        if(!empty($coll)){
            if($coll['status'] == 0){
                DB::table('collect')->where($data)->update(['status'=>1]);

            }elseif($coll['status'] == 1){
                DB::table('collect')->where($data)->update(['status'=>0]);

            }

        }else{
            $re =DB::table('collect')->insert($data);

        }

        $this->jsonResult(200,'','');

    }

    //我的首页
    public function myInit(){
        $request = $this->requestAll();
        $user  = $this->getUserInfo(); //通过 user_token 获取用户
        $data = [];
        //如果是医院
        if($user['user_type'] == 10){

            //发布未开始
            $data['biding_num'] =DB::table('biding')->where('bid_user_id',$user['user_id'])
                ->where('bid_start_time','>',now())->count();

            $data['biding_num_text'] = '招标中';
            //已发布
            $data['enlist_num']= DB::table('biding')->where('bid_user_id',$user['user_id'])->count();
            $data['enlist_num_text'] = '已发布';

        }
        //如果是供应商
        if($user['user_type'] == 11){
            //已报名
            $ids= DB::table('biding_enlist')->where('user_id',$user['user_id'])->pluck('bid_id')->toArray();
            $data['enlist_num'] = count($ids);
            $data['enlist_num_text'] = '已报价';

            //报名未开始
            $data['biding_num'] =DB::table('biding')->whereIn('bid_id',$ids)
                ->where('bid_start_time','>',now())->count();
            $data['biding_num_text'] = '招标中';



        }

        //是否开启邀请码
        $data['user_status'] = $user['user_status'];
        $data['is_invite'] = $user['is_invite'];

        $this->jsonResult(200,$data,'');

    }



    //  开启邀请码
    public function goInvite(){
        $request = $this->requestAll();
        $user  = $this->getUserInfo(); //通过 user_token 获取用户
        $data = [];
        //生成邀请码
        $inviteCode =makeInviteCode();
        $r  = DB::table('invite_code')->insertGetId(['user_id'=>$user['user_id'],'code'=>$inviteCode]);
        $re = DB::table('user')->where('user_id',$user['user_id'])->update(['is_invite'=>1]);

        $this->jsonResult(200,$data,'');

    }

    //合伙人信息
    public function getInviteInfo(){
        $request = $this->requestAll();
        $user  = $this->getUserInfo(); //通过 user_token 获取用户
        $data = [];
        //邀请医院
        $data['hospital'] = DB::table('user')->where('invite_user_id',$user['user_id'])->get()->toArray();
        //邀请医院数
        $data['hospital_num'] = count($data['hospital']);
        //成单数
        $ids = array_column($data['hospital'],'user_id');
        $data['deal_num'] = DB::table('biding')->whereIn('bid_user_id',$ids)
            ->where('bid_status',70)->count();

        //二维码
        $inviteCode = DB::table('invite_code')->where('user_id',$user['user_id'])
        ->where('code_status',0)->first();
        $data['invite_code'] = $inviteCode['code'];

        $this->jsonResult(200,$data,'');

    }

    //公告列表
    public function getNoticeList(){
        $request = $this->requestAll();
        //$user  = $this->getUserInfo(); //通过 user_token 获取用户
        $data = [];
        $data= DB::table('notice')->where('type',1)
            ->orderBy('add_time','desc')->get()->toArray();



        $this->jsonResult(200,$data,'');
    }

    //通过token获取用户信息
    public function getUserByToken(){
        $request  = $this->requestAll();
        $re = $this->getUserInfo($request);
//        p($request);
//        $re['region'] = [
//            provinceDataMap($re['province_id']),
//            cityDataMap($re['city_id']),
//            areaDataMap($re['area_id'])
//        ];
        $this->jsonResult(200,$re);
    }

}