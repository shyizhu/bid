<?php
namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Input;

use Config;
use App\Http\Models\Biding;
use App\Http\Models\Area;

use DB;
class IndexController extends BaseController{


	public function test(){
        //拼接成url字符串start
        //writeLog(1212121);

//        $request  = $this->requestAll();
//        $param_str = http_build_query($request);
//        $strpos = strpos($_SERVER['REQUEST_URI'],"?");
//        $url ='';
//        if(!$strpos)
//        {
//            $url = $_SERVER['REQUEST_URI'];
//        }else{
//            $url = substr($_SERVER['REQUEST_URI'],0,$strpos);
//        }
//
//        $url_str = 'http://'.$_SERVER['HTTP_HOST'].$url.'?'.$param_str;
//        var_dump($url_str);die;
//
//        //拼接成url字符串end
//        $error_info  ='';


	}

    //首页 20竞价中，70已完成，100已报名  数据切换
    public function tabBidList(){
        $request  = $this->requestAll();
        $redis = $this->redis();
        $bid = new Biding();
        $userInfo =[];
        $data=[];
        if(!empty($request['user_token'])){
            $userInfo = $this->getUserInfo();
        }

        //报名ids
        $ids =[];
        if(!empty($userInfo)){
            $ids =DB::table('biding_enlist')->where(['user_id'=>$userInfo['user_id'],'status'=>0])
                ->pluck('bid_id')->toArray();
        }

        $request['bid_status'] = $request['tab'];
        if($request['bid_status'] == 20){
            $data  = $bid->getBidBeing($request,$ids);

        }
        if($request['bid_status'] == 70){
            $data = $bid->getBidDone($request,$ids);

        }
        if($request['bid_status'] == 100){
            $request['user_type'] = $userInfo['user_type'];//用户类型
            $request['user_id'] = $userInfo['user_id'];
            $data  = $bid->getBidColl($request,$ids);
        }

        $this->jsonResult(200,$data);



    }


	//首页数据
    public function indexData(){
        $request  = $this->requestAll();
        $redis = $this->redis();
        $userInfo =[];
        if(!empty($request['user_token'])){
            $userInfo = $this->getUserInfo();
        }
        $bid = new Biding();
        $total = DB::table('total')->where('total_type',1)
            ->orderBy('total_id','asc')->pluck('total_val')->toArray(); //bidtype


        $data = [];
        //banner
	    $data['banner']=[
            '/img/index/banner1.jpg',
            '/img/index/banner2.jpg',
            '/img/index/banner3.jpg'

        ];
        $data['bid_type'] = bidTypeDataMap();
        //广告语
        $data['ad'] = '真正的“零”成本，不收取任何费用！';
        //公告
        $notice = DB::table('notice')->orderBy('add_time','desc')->limit(3)->pluck('content')->toArray();

//        $notice = [
//          '111111111111',
//          '222222222222',
//          '333333333333'
//        ];
        $data['notice'] = $notice;
        //统计
        $data['total']=$total;

        //报名ids
        $ids =[];
        if(!empty($userInfo)){
            $ids =DB::table('biding_enlist')->where(['user_id'=>$userInfo['user_id'],'status'=>0])
                ->pluck('bid_id')->toArray();
        }
        //招标中 数据
        $bidBeing = $bid->getBidBeing([],$ids);
        $data['bidBeing'] =$bidBeing;


        $this->jsonResult(200,$data);

    }

    //单分类数据
    public function typeBidList(){
        $request  = $this->requestAll();
        $redis = $this->redis();
        $userInfo =[];
        if(!empty($request['user_token'])){
            $userInfo = $this->getUserInfo();
        }
        $bid = new Biding();
        $total = DB::table('total')->where('total_type',$request['bidtype'])
            ->orderBy('total_id','asc')->pluck('total_val')->toArray(); //bidtype
        $total_bid_done  = $total[0];//已完成总单数
        $total_bid_being  = $total[1];//交易金额
        $total_seller_num  = $total[2];//最快完成天数

        $data = [];

        $data['bid_type'] = bidTypeDataMap($request['bidtype']);

        //已完成交易
        $data['total']=[
            'total_bid_done'=>$total_bid_done,
            'total_bid_being'=>$total_bid_being,
            'total_seller_num'=>$total_seller_num,
        ];

        //报名ids
        $ids =[];
        if(!empty($userInfo)){
            $ids =DB::table('biding_enlist')->where(['user_id'=>$userInfo['user_id'],'status'=>0])
                ->pluck('bid_id')->toArray();
        }
        //招标中 数据
        $bidBeing = $bid->getBidBeing($request,$ids);
        $data['bidBeing'] =$bidBeing;


        $this->jsonResult(200,$data);

    }

    //上传图片
    public function upfile(){
        $file = Input::file('fileData');

        if($file -> isValid()) {
            $entension = $file->getClientOriginalExtension(); //上传文件的后缀.
            $newName = md5(date('YmdHis') . mt_rand(100, 999)) . '.' . $entension;
            $path = $file->move(base_path() . '/public/uploads', $newName);
            // $filepath1['data'] =  $_SERVER['HTTP_HOST'].'/'."uploads/".$newName;
            $filepath1['data'] = $newName;


            $this->jsonResult(200,$newName);

        }else{
            $this->jsonResult(200,'','没有图片');

        }

    }
    //获取省列表
    public function getProList(){
        $provinceList = [];
        $pro =  provinceDataMap();//省列表
        foreach ($pro as $k => $v){
            $provinceList[] =array('text'=> $v,'value'=>$k);
        }
        $this->jsonResult(200,$provinceList);

    }

    //省市地区三级联动
    public function getListArea(){
        $parent_id      = !empty(Input::get('parent_id')) ? Input::get('parent_id') : 0;
        $area = new Area();
        $result = $area->getListByParentId($parent_id);
        $this->jsonResult(200,$result);
    }

    public function getHospitalType1(){
        $data = [];
        $hospitalType1 = Config("hospitaltype.hospital_type1");
        foreach ($hospitalType1 as $k => $v){
            $data[] =array('text'=> $v,'value'=>$k);
        }
        $this->jsonResult(200,$data);

    }

    //医院类型二级联动
    public function getHospitalType2(){
        $data = [];$tmp=[];
        $parent_id = !empty(Input::get('parent_id')) ? Input::get('parent_id') : 0;
        $hospitalType2 = Config("hospitaltype.hospital_type2");
        if(!empty($hospitalType2[$parent_id])){
            $tmp = $hospitalType2[$parent_id];
            foreach ($tmp as $k => $v){
                $data[] =array('text'=> $v,'value'=>$k);
            }

        }
        $this->jsonResult(200,$data);
    }

    //在线直播
    public function linePlay(){
	    $sign = $this->getJsSdkSign();
//	    p($sign);
        return view('lineplay',[
            'sign'=>$sign
        ]);

    }
    //获取公司信息
    public function linPlayCompanyInfo(){
        $request  = $this->requestAll();
        $data = [
            'company_name'=>$request['company_name'],
            'link_name'=>$request['link_name'],
            'link_phone'=>$request['link_phone'],
            'sale_name'=>$request['sale_name'],

        ];
       $re = DB::table('line_play')->insertGetId($data);
        if($re >0){
            $this->jsonResult(200);
        }

    }

}
