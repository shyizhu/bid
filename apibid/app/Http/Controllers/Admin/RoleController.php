<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use DB;
use Illuminate\Http\Request;
use App\Http\Models\Role;
use App\Http\Models\Menu;



/**

 */
class RoleController extends BaseController
{
    //请求参数
    private $request;

    //验证码有效期
    private $captcha_expire_time = 180;

    public function __construct()
    {
        $this->request = $this->requestAll();
        parent::__construct();
    }

    public function  test()
    {
        echo 6666666666;
    }
    //获取列表
    public function getRoleList(){

        //角色
        $data = [];
        $roleList = DB::table('role')->get()->toArray();
        foreach ($roleList as $k=>$v){
            $ids = explode(',',$v['role_menu_id']);
            $tmp = DB::table('menu')->whereIn('menu_id',$ids)->pluck('menu_name')->toArray();
            $roleList[$k]['menuName']= implode($tmp,',');
        }
        $data['roleList'] = $roleList;
        $data['menuList'] = (new Menu())->getList();


        $this->jsonResult(200,$data);

    }
    //添加角色
    public function roleAdd(){

        $request = $this->request;
        $ids = '';
        if(!empty($request['role_menu_id'])){
            $ids = implode(',',$request['role_menu_id']);
        }

        $data=[
            'role_name'=>$request['role_name'],
            'role_menu_id'=>$ids,
        ];


        $res =DB::table('role')->insert($data);;

        if($res > 0) {
            $this->jsonResult(200,$res);
        }else{
            $this->jsonResult(201,'添加失败');

        }

    }
    //编辑
    public function roleEdit(){
        $request = $this->request;

        $ids = '';
        if(!empty($request['role_menu_arr'])){
            $ids = implode(',',$request['role_menu_arr']);
        }

        $data = [
            'role_name'=>$request['role_name'],
            'role_menu_id'=>$ids
        ];

        $res = DB::table('role')->where('role_id',$request['role_id'])->update($data);
        if($res>0) {

            $this->jsonResult(200,$res);
        }
    }

    //删除角色
    public function roleDel(){
        $id     = $this->request['id'];

        $re  = DB::table('role')->where(['role_id'=>$id])->delete();

        if($re > 0) {
            $this->jsonResult(200);
        }else{
            $this->jsonResult(201,'删除失败！');

        }


    }

}