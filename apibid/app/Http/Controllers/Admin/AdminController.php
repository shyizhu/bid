<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use DB;
use App\Libraries\newsmsapi;
use App\Libraries\Captcha;
use Config;
use Illuminate\Http\Request;
use App\Http\Models\Menu;

use App\Http\Services\TokenService;

/**

 */
class AdminController extends BaseController
{
    /**
     * 请求参数
     */
    private $request;
    private $tokenService;

    /**
     * 验证码有效期
     */
    private $captcha_expire_time = 180;

    public function __construct(TokenService $tokenService)
    {
        $this->request = $this->requestAll();
        $this->tokenService = $tokenService;
        parent::__construct();
    }

    public function  test()
    {
        echo 6666666666;
    }
    //后台登录
    public function adminLogin(){
        $request  = $this->requestAll();
        $userName =$request['user_name'];
        $pass = md5(md5($request['user_pass']).config('session.pass'));
        $res  = DB::table('user')
            ->where(['user_pass'=>$pass,'user_type'=>20,'is_del'=>0])
            ->where(function($query) use($userName){
                $query->where('user_phone','=',$userName)
                    ->orWhere(function($query) use($userName){
                        $query->where('user_email','=',$userName);
                    })->orWhere(function($query) use($userName){
                        $query->where('user_name','=',$userName);
                    });
            })
            ->first();

        $roleMenuId = DB::table('role')->where(['role_id'=>$res['user_role_id']])
            ->value('role_menu_id');

        //session 数据
        $data=[
            'user_id'=>$res['user_id'],
            'user_type'=>$res['user_type'],
            'user_role_id'=>$res['user_role_id'],
            'user_name'=>$res['user_name'],
            'user_phone'=>$res['user_phone'],
            'user_email'=>$res['user_email'],
            'role_menu_id'=>$roleMenuId,
            'user_nickname'=>$res['user_nickname'],
            'user_menu'=>(new Menu())->getAdminMenu($roleMenuId)
        ];

        if(!empty($res)){

            $data['user_token'] =  $this->tokenService->tokenForReids($data);

            $this->jsonResult(200,$data);
        }else{
            $this->jsonResult(201,'','账号或密码错误！');
        }
    }

    //验证登录
    public function checkLogin(){
        $login_session = session(config('session.pass'));

        if(json_decode($login_session)){
            return true;
        }else{
            return false;
        }
    }

    //退出登录
    public function logout(Request $request){

        $request->session()->invalidate();
        return redirect('back/showLogin');

    }

    //获取后台用户
    public function getAdminList(){
        $data = [];
        $adminList = DB::table('user')
            ->select('user.*','role.role_name')
            ->leftJoin('role','user.user_role_id','=','role.role_id')
            ->where(['user.user_type'=>20,'user.user_status'=>0])
            ->get()->toArray();

        $data['adminList'] = $adminList;
        $roleList=DB::table('role')->select('role_id','role_name')
            ->where('status',0 )->get()->toArray();

        $data['roleList'] = $roleList;

        $this->jsonResult(200,$data);

    }

    public function adminAdd(){
        $request = $this->request;
        $pass = md5(md5($request['user_pass']).config('session.pass'));


        $data = [
            'user_type'=>20,//后台用户
            'user_name'=>$request['user_name'],
            'user_pass'=>$pass,
            'user_role_id'=>$request['user_role_id']

        ];
        $res = DB::table('user')->insert($data);

        $this->jsonResult(200,$res);

    }

    public function showUserEdit(){


        return view('power.showuseredit',[

        ]);
    }

    public function adminEdit(){


    }

    //显示修改密码
    public function showEditPass(){

        $adminInfo = $this->getAdminInfo();


        return view('power.editpass',[
            'adminInfo'=>$adminInfo
        ]);
    }

    //修改密码
    public function editPass(){
        $request = $this->request;
        $pass = md5(md5($request['user_pass']).config('session.pass'));
        $rs = DB::table('user')->where('user_phone',$request['user_phone'])->update(['user_pass'=>$pass]);

        session([config('session.pass')=>'']);
        return redirect('back/showLogin');
    }
}