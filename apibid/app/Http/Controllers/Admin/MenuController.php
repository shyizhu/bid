<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\BaseController;
use DB;
use Illuminate\Http\Request;
use App\Http\Models\Menu;


/**

 */
class MenuController extends BaseController
{
    //请求参数
    private $request;

    //验证码有效期
    private $captcha_expire_time = 180;

    public function __construct()
    {
        $this->request = $this->requestAll();
        parent::__construct();
    }

    public function  test()
    {
        echo 6666666666;
    }
    //获取列表
    public function getMenuList(){

        $menuList = (new Menu())->getList();


        $this->jsonResult(200,$menuList);

    }
    //添加菜单
    public function menuAdd(){

        $pid     = $this->request['menu_pid'];

        //验证菜单等级
        if($pid == 0){
            $leval = 1;
        }else{
            $pLeval=  (new Menu())->getParentDepth($pid);
            $leval = $pLeval[0]+1;
        }

        $input = [
            'menu_pid'        => $this->request['menu_pid'],
            'menu_name'        => $this->request['menu_name'],
            'menu_path'        => $this->request['menu_path'],
            'menu_sort'        => !empty($this->request['menu_sort'])?$this->request['menu_sort']:0,
            'menu_icon'        => !empty($this->request['menu_icon'])?$this->request['menu_icon']:'',
            'leval'            => $leval,
            'is_show'            => 1,

        ];
        $rs = Menu::create($input);

        if($rs['id'] > 0) {
            $this->jsonResult(200,$rs['id']);
        }else{
            $this->jsonResult(201,'添加失败');

        }

    }

    //删除菜单
    public function menuDel(){
        $id     = $this->request['id'];
        $re  = DB::table('menu1')->where(['menu_id'=>$id])->delete();

        if($re > 0) {
            $this->jsonResult(200);
        }else{
            $this->jsonResult(201,'删除失败！');

        }


    }

}