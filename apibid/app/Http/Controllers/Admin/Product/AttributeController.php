<?php
namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\BaseController;
use DB;
use Config;
use Illuminate\Http\Request;
use App\Http\Models\Attr;


/**

 */
class AttributeController extends BaseController
{
    /**
     * 请求参数
     */
    private $request;

    /**
     * 验证码有效期
     */
    private $captcha_expire_time = 180;

    public function __construct()
    {
        $this->request = $this->requestAll();
        parent::__construct();
    }

    public function  test()
    {
        $c= 'a:3:{i:0;a:1:{s:3:"key";s:4:"cpu1";}i:1;a:1:{s:3:"key";s:4:"cpu2";}i:2;a:1:{s:3:"key";s:4:"cpu3";}}';
        $a ='a:7:{i:0;a:2:{s:7:"attr_id";s:2:"11";s:10:"sort_order";i:0;}i:1;a:2:{s:7:"attr_id";s:2:"10";s:10:"sort_order";i:0;}i:2;a:2:{s:7:"attr_id";s:1:"9";s:10:"sort_order";i:0;}i:3;a:2:{s:7:"attr_id";s:1:"8";s:10:"sort_order";i:0;}i:4;a:2:{s:7:"attr_id";s:1:"7";s:10:"sort_order";i:0;}i:5;a:2:{s:7:"attr_id";s:1:"3";s:10:"sort_order";i:4;}i:6;a:2:{s:7:"attr_id";s:1:"2";s:10:"sort_order";i:5;}}';
        $b = unserialize($c);
        p($b);

    }
    //获取分类下的属性列表
    public function getAttrById(){
        $request = $this->request;
        $id = $request['id'];
        $attr = new Attr();

        $list = $attr->getAttrListById($id);

//        p($list);

        $this->jsonResult(200,$list);

    }
    //给租，添加属性
    public function attrAdd(){
        $request = $this->request;

        $data = [
            'group_id'=>$request['group_id'],
            'attr_type'=>$request['attr_type'],
            'name'=>$request['name'],
            'status'=>$request['status']?1:2,
            'db_type'=>$request['db_type'],//数据类型，字符串，还是数字
            'show_as_img'=>$request['show_as_img']?1:2,
            'display_type'=>$request['display_type'],//显示方式
            'is_require'=>$request['is_require']?1:2,
            'default'=>$request['default'],
            'sort'=>$request['sort'],

        ];
        //如果有选项
        if($request['attr_type'] == 'spu_attr'){
            $data['display_data'] = serialize($request['items'] );
        }

        $res = DB::table('product_attribute')->insert($data);



        $this->jsonResult(200,$data);

    }
    public function attrEdit(){
        $request = $this->request;

        $data = [
            'attr_type'=>$request['attr_type'],
            'name'=>$request['name'],
            'status'=>$request['status']?1:2,
            'db_type'=>$request['db_type'],//数据类型，字符串，还是数字
            'show_as_img'=>$request['show_as_img']?1:2,
            'display_type'=>$request['display_type'],//显示方式
            'is_require'=>$request['is_require']?1:2,
            'default'=>$request['default'],
            'sort'=>$request['sort'],

        ];
        //如果有选项
        if($request['attr_type'] == 'spu_attr'){
            $data['display_data'] = serialize($request['items'] );
        }

        $res = DB::table('product_attribute')->where('id',$request['id'])->update($data);



        $this->jsonResult(200,$data);



    }
    public function attrDel(){
        $request = $this->request;
        $data['status'] = 0;
        $res = DB::table('product_attribute')->where('id',$request['id'])->update($data);

        $this->jsonResult(200,$res);



    }



    //属性组, 管理
    public function getAttrGroupList(){
        $attr = new Attr();
        $list = $attr->getAttrGroupList();

//        p($list);

        $this->jsonResult(200,$list);

    }
    //
    public function attrGroupAdd(){
        $request = $this->request;
//        $brandModel = new Brand();

        $data = [
            'name'=>$request['name'],

            'status'=>$request['status']?1:0,//是否启用

        ];
        $res = DB::table('product_attribute_group')->insert($data);

        $this->jsonResult(200,$res);

    }
    public function attrGroupEdit(){
        $request = $this->request;

        $data = [
            'name'=>$request['name'],

            'status'=>$request['status']?1:0,//是否启用

        ];

        $res = DB::table('product_attribute_group')->where('id',$request['id'])->update($data);


        $this->jsonResult(200,$res);


    }

    public function attrGroupDel(){
        $request = $this->request;
        $data['status'] = 0;
        $res = DB::table('product_attribute_group')->where('id',$request['id'])->update($data);

        $this->jsonResult(200,$res);

    }





}