<?php
namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\BaseController;
use DB;
use Config;
use Illuminate\Http\Request;
use App\Http\Models\Brand;


/**

 */
class BrandController extends BaseController
{
    /**
     * 请求参数
     */
    private $request;

    /**
     * 验证码有效期
     */
    private $captcha_expire_time = 180;

    public function __construct()
    {
        $this->request = $this->requestAll();
        parent::__construct();
    }

    public function  test()
    {
        echo 222222;
    }

    //获取全部菜单
    public function getBrandList(){
        $cate = new Brand();
        $list = $cate->getBrandList();

//        p($list);

        $this->jsonResult(200,$list);

    }
    //
    public function brandAdd(){
        $request = $this->request;
//        $brandModel = new Brand();

        $data = [
            'brand_name'=>$request['brand_name'],
            'brand_initial'=>$request['brand_initial'],
            'is_up'=>$request['is_up']?1:0,//上架
            'is_show'=>$request['is_show']?1:0,//是否显示
            'brand_sort'=>!empty($request['brand_sort'])?$request['brand_sort']:0,//排序

            'status'=>1,//1正常

        ];
        $res = DB::table('brand')->insert($data);

        $this->jsonResult(200,$res);

    }
    public function brandEdit(){
        $request = $this->request;

        $data = [
            'brand_name'=>$request['brand_name'],
            'brand_initial'=>$request['brand_initial'],
            'is_up'=>$request['is_up']?1:0,//上架
            'is_show'=>$request['is_show']?1:0,//是否显示
            'brand_sort'=>!empty($request['brand_sort'])?$request['brand_sort']:0,//排序

            'status'=>1,//1正常

        ];

        $res = DB::table('brand')->where('brand_id',$request['brand_id'])->update($data);


        $this->jsonResult(200,$res);


    }

    public function brandDel(){
        $request = $this->request;
        $data['status'] = 0;
        $res = DB::table('brand')->where('brand_id',$request['brand_id'])->update($data);

        $this->jsonResult(200,$res);

    }





}