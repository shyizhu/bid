<?php
namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\BaseController;
use DB;
use Config;
use Illuminate\Http\Request;
use App\Http\Models\Category;


/**

 */
class CategoryController extends BaseController
{
    /**
     * 请求参数
     */
    private $request;

    /**
     * 验证码有效期
     */
    private $captcha_expire_time = 180;

    public function __construct()
    {
        $this->request = $this->requestAll();
        parent::__construct();
    }

    public function  test()
    {
        echo 222222;
    }

    //获取全部菜单
    public function getCateList(){
        $cate = new Category();
        $list = $cate->getList();

//        p($list);

        $this->jsonResult(200,$list);

    }
    //
    public function cateAdd(){
        $request = $this->request;
        $cateModel = new Category();


        $pid = !empty($request['parent_id'])?$request['parent_id']:0;
        $level = $cateModel->getParentLevel($pid);

        $data = [
            'name'=>$request['name'],
            'parent_id'=>$pid,
            'is_show'=>$request['is_show']?1:0,//是否显示
            'sort'=>!empty($request['sort'])?$request['sort']:0,//排序
            'desc'=>$request['desc'],

            //'created_user_id'=>$request['created_user_id'],
            'level'=>$level,
            'status'=>1,//1正常

        ];
        $res = DB::table('category')->insert($data);

        $this->jsonResult(200,$res);

    }
    public function cateEdit(){
        $request = $this->request;
        $cateModel = new Category();


        $pid = !empty($request['parent_id'])?$request['parent_id']:0;
        $level = $cateModel->getParentLevel($pid);

        $data = [
            'name'=>$request['name'],
            'parent_id'=>$pid,
            'is_show'=>$request['is_show']?1:0,//是否显示
            'sort'=>!empty($request['sort'])?$request['sort']:0,//排序
            'desc'=>$request['desc'],

            //'created_user_id'=>$request['created_user_id'],
            'level'=>$level,
            'status'=>1,//1正常

        ];
        $res = DB::table('category')->where('id',$request['id'])->update($data);


        $this->jsonResult(200,$res);


    }

    public function cateDel(){
        $request = $this->request;
        $data['status'] = 0;
        $res = DB::table('category')->where('id',$request['id'])->update($data);

        $this->jsonResult(200,$res);

    }





}