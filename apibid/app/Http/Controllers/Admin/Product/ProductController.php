<?php
namespace App\Http\Controllers\Admin\Product;

use App\Http\Controllers\BaseController;
use DB;
use Config;
use Illuminate\Http\Request;
use App\Http\Models\Attr;
use App\Http\Models\Product;


/**

 */
class ProductController extends BaseController
{
    /**
     * 请求参数
     */
    private $request;

    /**
     * 验证码有效期
     */
    private $captcha_expire_time = 180;

    public function __construct()
    {
        $this->request = $this->requestAll();
        parent::__construct();
    }

    public function  test()
    {
        $c= 'a:3:{i:0;a:1:{s:3:"key";s:4:"cpu1";}i:1;a:1:{s:3:"key";s:4:"cpu2";}i:2;a:1:{s:3:"key";s:4:"cpu3";}}';
        $a ='a:7:{i:0;a:2:{s:7:"attr_id";s:2:"11";s:10:"sort_order";i:0;}i:1;a:2:{s:7:"attr_id";s:2:"10";s:10:"sort_order";i:0;}i:2;a:2:{s:7:"attr_id";s:1:"9";s:10:"sort_order";i:0;}i:3;a:2:{s:7:"attr_id";s:1:"8";s:10:"sort_order";i:0;}i:4;a:2:{s:7:"attr_id";s:1:"7";s:10:"sort_order";i:0;}i:5;a:2:{s:7:"attr_id";s:1:"3";s:10:"sort_order";i:4;}i:6;a:2:{s:7:"attr_id";s:1:"2";s:10:"sort_order";i:5;}}';
        $d = 'a:2:{s:7:"gallery";a:2:{i:0;a:5:{s:5:"image";s:29:"/f/gh/fghocd1467608103115.jpg";s:5:"label";s:0:"";s:10:"sort_order";s:0:"";s:13:"is_thumbnails";s:1:"1";s:9:"is_detail";s:1:"1";}i:1;a:5:{s:5:"image";s:29:"/x/wl/xwlxzn1467608134156.jpg";s:5:"label";s:0:"";s:10:"sort_order";s:0:"";s:13:"is_thumbnails";s:1:"1";s:9:"is_detail";s:1:"1";}}s:4:"main";a:5:{s:5:"image";s:29:"/r/nu/rnunew1467608090223.jpg";s:5:"label";s:0:"";s:10:"sort_order";s:0:"";s:13:"is_thumbnails";s:1:"1";s:9:"is_detail";s:1:"1";}}';
        $e = 'a:7:{s:6:"collar";s:6:"v-meck";s:13:"sleeve-length";s:13:"short-Sleeves";s:12:"pattern-type";s:6:"floral";s:5:"style";s:8:"Bohemian";s:14:"dresses-length";s:12:"ankle-length";s:5:"color";s:3:"red";s:4:"size";s:1:"L";}';

        $b = unserialize($e);
        p($b);

    }


    //获取全部商品
    public function getProductList(){
        $request = $this->request;
        $productModel = new Product();
        $attrModel = new Attr();

        $data = $productModel->getList($request);



        $this->jsonResult(200,$data);

    }
    //
    public function productAdd(){
        $request = $this->request;
//        $brandModel = new Brand();

        $data = [
            'name'=>$request['name'],

            'status'=>$request['status']?1:0,//是否启用

        ];
        $res = DB::table('product_attribute_group')->insert($data);

        $this->jsonResult(200,$res);

    }
    public function productEdit(){
        $request = $this->request;

        $data = [
            'name'=>$request['name'],

            'status'=>$request['status']?1:0,//是否启用

        ];

        $res = DB::table('product_attribute_group')->where('id',$request['id'])->update($data);


        $this->jsonResult(200,$res);


    }

    public function productDel(){
        $request = $this->request;
        $data['status'] = 0;
        $res = DB::table('product_attribute_group')->where('id',$request['id'])->update($data);

        $this->jsonResult(200,$res);

    }


    //审核
    public function productExamine(){



    }




}