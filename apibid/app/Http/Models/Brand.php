<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Brand extends Model
{
    protected $table = 'brand';

    protected $primaryKey = 'brand_id';
    public $timestamps = false;
    protected $guarded = [];


    //获取全部品牌
    public function getBrandList(){

        $data['status'] = 1;
        $data = $this->where($data)->orderBy('brand_sort', 'asc')->get()->toArray();

        return $data;
    }










}
