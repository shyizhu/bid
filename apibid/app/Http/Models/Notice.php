<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Notice extends Model
{
    protected $table = 'notice';

    protected $fillable = array();

    //protected $guarded = array();

    public $timestamps = false;


    //发布公告
    public function pushNotice($data){
        $inset= [
            'type'=>$data['type'],
            'title'=>$data['title'],
            'content'=>$data['content']
        ];
        $re = DB::table('notice')->insertGetId($inset);
        if($re >0){
            return true;
        }else{
            return false;
        }

    }

    



}
