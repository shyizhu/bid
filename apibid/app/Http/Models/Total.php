<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Total extends Model
{
    protected $table = 'total';

    protected $fillable = array();

    //protected $guarded = array();

    public $timestamps = false;


    //发布标书增加
    public function pushBidTotal($data){
        //首页招标中
       $r = DB::table('total')->where('total_key','total_bid_being')
           ->increment('total_val',1);
       $ids = [];
       if($data['bid_type'] == 10){
           $ids = [5,6];
       }
        if($data['bid_type'] == 20){
            $ids = [8,9];
       }
        if($data['bid_type'] == 30){
            $ids = [11,12];
        }
        if($data['bid_type'] == 40){
            $ids = [14,15];
        }
        if($data['bid_type'] == 50){
            $ids = [17,18];
        }
        if($data['bid_type'] == 60){
            $ids = [20,21];
        }
        //类型总交易金额
        $r = DB::table('total')->where('total_id',$ids[0])
            ->increment('total_val',$data['bid_budget']);
        //类型正在交易
        $r = DB::table('total')->where('total_id',$ids[1])
            ->increment('total_val',1);

    }

    //医院点击确认，统计
    public function buyerTrueTotal($data){
        //首页已完成
        $r = DB::table('total')->where('total_key','total_bid_done')
            ->increment('total_val',1);
        $ids = [];
        if($data['bid_type'] == 10){
            $ids = [4];
        }
        if($data['bid_type'] == 20){
            $ids = [7];
        }
        if($data['bid_type'] == 30){
            $ids = [10];
        }
        if($data['bid_type'] == 40){
            $ids = [13];
        }
        if($data['bid_type'] == 50){
            $ids = [16];
        }
        if($data['bid_type'] == 60){
            $ids = [19];
        }

        //类型已成交
        $r = DB::table('total')->whereIn('total_id',$ids)
            ->increment('total_val',1);

    }

    //供应商点击确认，统计
    public function sellerTrueTotal($data){
        //首页招标中
        $r = DB::table('total')->where('total_key','total_bid_being')
            ->decrement('total_val',1);
        $ids = [];
        if($data['bid_type'] == 10){
            $ids = [6];
        }
        if($data['bid_type'] == 20){
            $ids = [9];
        }
        if($data['bid_type'] == 30){
            $ids = [12];
        }
        if($data['bid_type'] == 40){
            $ids = [15];
        }
        if($data['bid_type'] == 50){
            $ids = [18];
        }
        if($data['bid_type'] == 60){
            $ids = [21];
        }

        //类型正在交易
        $r = DB::table('total')->whereIn('total_id',$ids)
            ->decrement('total_val',1);

    }

    //供应商注册 统计
    public function sellerRegisterTotal(){
        DB::table('total')->where('total_key','total_seller_num')
            ->increment('total_val',1);

    }




}
