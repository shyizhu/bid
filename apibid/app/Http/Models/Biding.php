<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Http\Models\Notice;


class Biding extends Model
{

    protected $table = 'biding';
    protected $primaryKey = 'bid_id';
    public $timestamps = false;
    protected $guarded = [];

    protected $oneTimeLen = 600;//600 //每轮竞标时间
    protected $pageSize =5 ;// 每页条数

    //获取标书列表 By status  0竞价中，1已中标，2未中标，3流拍，4效益作废，5拒收，6待确认(客服确认)，7成交
    public function getBidListByStatus($request){
        $userType = $request['user_type'];
        $status = $request['bid_status'];
        $userId = $request['user_id'];

        $data  = DB::table('biding');
        //10采购方 11供应商
        if(!in_array($userType,[10,11])){
            return [];
        }
        //如果是医院  就是所有发布的
        if($userType == 10){
            $data  = $data->where('bid_user_id',$userId);
            if($request['tab'] == 0 || $request['tab'] == 3 ) {
                $data  = $data->where('bid_status',$status);

            }
                //1已中标
            if($request['tab'] == 1){
                $data->whereIn('bid_status',[28,29]);
            }
            //2未中标
            if($request['tab'] == 2){
                $data  = $data->where('bid_status',30);
            }
            // 已中标，4效益作废，5拒收，6待确认(客服确认)，7成交
            if($request['tab'] == 4 || $request['tab'] == 5 || $request['tab'] == 6 || $request['tab'] == 7) {
                $data  = $data->where('bid_status',$status);

            }
            
        }
        //如果是供应商   就是所报名过的
        if($userType == 11){
            //所有报名的
            $ids = DB::table('biding_enlist')->where(['user_id'=>$userId,'status'=>0])->pluck('bid_id');
            $data  = $data->whereIn('bid_id',$ids);
            if($request['tab'] == 0 || $request['tab'] == 3 || $request['tab'] == 4 || $request['tab'] == 5 || $request['tab'] == 6) {
                $data  = $data->where('bid_status',$status);

            }
            //1已中标  只有中标
            if($request['tab'] == 1){
                $data  = $data->where('pick_user_id',$userId);
                $data->whereIn('bid_status',[28,29]);
            }
            //2未中标
            if($request['tab'] == 2){
                $data  = $data->where('pick_user_id','!=',$userId);
                $data->whereIn('bid_status',[28,29]);
            }
            if($request['tab'] == 7) {
                $data  = $data->where('pick_user_id',$userId);
                $data  = $data->where('bid_status',$status);
            }
        }


        $data  = $data->where('is_del',0)
            ->orderBy('bid_start_time','asc')
            ->get()
            ->toArray();

        //数据格式化
        foreach ($data as $k =>$v){
            //医院 ，已中标，28 中标，待医院确认
            if($userType == 10 && $request['tab'] == 1 && $v['bid_status'] == 28){
                $data[$k]['is_true'] =0;//未确认
            }
            if($userType == 10 && $request['tab'] == 1 && $v['bid_status'] == 29){
                $data[$k]['is_true'] =1;//已确认
            }
            //供应商 ，已中标，29 中标，待供应商确认
            if($userType == 11 && $request['tab'] == 1 && $v['bid_status'] == 29){
                $data[$k]['is_true'] =0;//未确认
            }
            if($userType == 11 && $request['tab'] == 1 && $v['bid_status'] == 70){
                $data[$k]['is_true'] =1;//已确认
            }
        }


        return $data;
    }

    //竞价中
    public function getBidBeing($request=[],$ids=[]){
        $page = !empty($request['page'])?$request['page']:1;
        $pageSize = $this->pageSize;

        $bidBeing  = DB::table('biding');

        if(isset($request['bidtype']) && !empty($request['bidtype'])){
            $bidBeing = $bidBeing->where('bid_type',$request['bidtype']);
        }

        $bidBeing= $bidBeing->where('bid_status',20)
            ->offset(0)
            ->limit($page*$pageSize)
            ->where('is_del',0)
            ->orderBy('bid_start_time','asc')
            ->get()->toArray();
        //数据格式化
        foreach ($bidBeing as $k =>$v){
            if(!empty($v)){
                //是否报名
                $bidBeing[$k]['is_enlist'] = 0;
                //项目备注显示隐藏
                $bidBeing[$k]['show_remark'] =false;
                $bidBeing[$k]['show_remark_ud'] = '/img/index/up.png';
                //时间
                $bidBeing[$k]['time'] = date('m-d',strtotime($v['bid_start_time']));
                $bidBeing[$k]['time1'] = (strtotime($v['bid_start_time']) - time())*1000;//距离开标时间
                //发布者
                $bidBeing[$k]['bid_user_company'] =companyNameFilter($bidBeing[$k]['bid_user_company']);
                //项目类型
                $bidBeing[$k]['bid_type'] =bidTypeDataMap($bidBeing[$k]['bid_type']);
                //城市
                $bidBeing[$k]['city_id'] =cityDataMap($bidBeing[$k]['city_id']);


            }

            foreach($ids as $k1=>$v1){
                if($v['bid_id'] == $v1){
                    $bidBeing[$k]['is_enlist'] = 1; //是否报名
                }
            }
        }
        $re['page']= $page;
        $re['data'] = $bidBeing;
        return $re;
    }
    //已完成
    public function getBidDone($request=[],$ids=[]){
        $page = !empty($request['page'])?$request['page']:1;
        $pageSize = $this->pageSize;

        $bidDone  = DB::table('biding');
        if(isset($request['bidtype']) && !empty($request['bidtype'])){
            $bidDone = $bidDone->where('bid_type',$request['bidtype']);
        }
        $bidDone = $bidDone->where('bid_status',70)
            ->offset(0)
            ->limit($page*$pageSize)
            ->where('is_del',0)
            ->orderBy('bid_start_time','desc')
            ->get()->toArray();
        foreach ($bidDone as $k =>$v){
            if(!empty($v)){
                //是否报名
                $bidDone[$k]['is_enlist'] = 0;
                //项目备注显示隐藏
                $bidDone[$k]['show_remark'] =false;
                $bidDone[$k]['show_remark_ud'] = '/img/index/up.png';
                //时间
                $bidDone[$k]['time'] = date('m-d',strtotime($v['bid_start_time']));
                $bidDone[$k]['time1'] = (strtotime($v['bid_start_time']) - time())*1000;//距离开标时间
                //发布者
                $bidDone[$k]['bid_user_company'] =companyNameFilter($bidDone[$k]['bid_user_company']);
                //中标者
                $bidDone[$k]['pick_user_company'] =companyNameFilter($bidDone[$k]['pick_user_company']);
                //项目类型
                $bidDone[$k]['bid_type'] =bidTypeDataMap($bidDone[$k]['bid_type']);
                //城市
                $bidDone[$k]['city_id'] =cityDataMap($bidDone[$k]['city_id']);
            }
            foreach($ids as $k1=>$v1){
                if($v['bid_id'] == $v1){
                    $bidDone[$k]['is_enlist'] = 1; //是否报名

                }
            }
        }
        $re['page']= $page;
        $re['data'] = $bidDone;
        return $re;
    }
    //已报名
    public function getBidColl($request=[],$ids){
        $page = !empty($request['page'])?$request['page']:1;
        $pageSize = $this->pageSize;

        $bidColl  = DB::table('biding');
        if(isset($request['bidtype']) && !empty($request['bidtype'])){
            $bidColl = $bidColl->where('bid_type',$request['bidtype']);
        }
        $bidColl  = $bidColl->whereIn('bid_id',$ids)
            ->offset(0)
            ->limit($page*$pageSize)
            ->where('is_del',0)
            ->orderBy('bid_start_time','asc')
            ->get()->toArray();
        foreach ($bidColl as $k =>$v){
            if(!empty($v)){
                //是否报名
                $bidColl[$k]['is_enlist'] = 0;
                //项目备注显示隐藏
                $bidColl[$k]['show_remark'] =false;
                $bidColl[$k]['show_remark_ud'] = '/img/index/up.png';
                //时间
                $bidColl[$k]['time'] = date('m-d',strtotime($v['bid_start_time']));
                $bidColl[$k]['time1'] = (strtotime($v['bid_start_time']) - time())*1000;//距离开标时间
                //发布者
                $bidColl[$k]['bid_user_company'] =companyNameFilter($bidColl[$k]['bid_user_company']);
                //项目类型
                $bidColl[$k]['bid_type'] =bidTypeDataMap($bidColl[$k]['bid_type']);
                //城市
                $bidColl[$k]['city_id'] =cityDataMap($bidColl[$k]['city_id']);
            }
            foreach($ids as $k1=>$v1){
                if($v['bid_id'] == $v1){
                    $bidColl[$k]['is_enlist'] = 1;

                }
            }
        }

        $re['page']= $page;
        $re['data'] = $bidColl;
        return $re;

    }
    //获取bid 当前状态 //动态状态1 未开始，10第一轮，20第二轮，30第三轮，40竞拍结束 50流拍,60未报名
    public function goWhere($request){

        $result = [];
        $data = DB::table('biding')->where('bid_id',$request['bid_id'])->first();
        //订单状态检查
        $re = $this->bidStatusCheck($data);
        //是否报名
        $enroll = DB::table('biding_enlist')
            ->where(['bid_id'=>$request['bid_id'],'user_id'=>$request['user_id'],'status'=>0])
            ->first();
        $result['isEnroll'] = !empty($enroll)? 1 :0; //1报名了，0没报名

        $bid_change_status = 0;// 订单动态状态 1 未开始，10第一轮，20第二轮，30第三轮，40竞拍结束 50流拍

        $bidStartTime = $data['bid_start_time'];//开标时间

        $bidOneEndTime = date( 'Y-m-d H:i:s',strtotime($data['bid_start_time'])+$this->oneTimeLen);
        $bidTwoEndTime = date( 'Y-m-d H:i:s',strtotime($data['bid_start_time'])+$this->oneTimeLen*2);
        $bidEndTime = date( 'Y-m-d H:i:s',strtotime($data['bid_start_time'])+$this->oneTimeLen*3);


        //流拍
        if($re == 30 || $data['bid_status'] == 30){
            $result['bidChangeStatus']=50;
            $result['toWhere']='enddetail/enddetail';
            return $result;

        }
        //中标
        if($re == 28 || $data['bid_status'] == 28){
            $result['bidChangeStatus']=40;
            $result['toWhere']='enddetail/enddetail';
            return $result;

        }
        //未开始
        if(now() < $bidStartTime && $data['bid_status'] == 20){
            $result['bidChangeStatus']=1;
            $result['toWhere']='showdetail/showdetail';
            return $result;
        }
        //已开始 未结束 未报名
        if(now() >= $bidStartTime && empty($enroll) && now() <=$bidEndTime && $data['bid_status'] != 30){
            $data['bidChangeStatus']=60;
            $data['toWhere']='notdetail/notdetail';

            return $data;

        }
        //第一轮中
        if(now() >= $bidStartTime && now() < $bidOneEndTime && !empty($enroll) && $data['bid_status'] == 20){
            $result['bidChangeStatus']=10;
            $result['toWhere']='onedetail/onedetail';
            return $result;
        }

        //第二轮中
        if(now() >= $bidOneEndTime && now() < $bidTwoEndTime && !empty($enroll) && $data['bid_status'] == 20){
            $result['bidChangeStatus']=20;
            $result['toWhere']='twodetail/twodetail';
            return $result;
        }

        //第三轮中
        if(now() >= $bidTwoEndTime && now() < $bidEndTime && !empty($enroll) && $data['bid_status'] == 20){
            $result['bidChangeStatus']=30;
            $result['toWhere']='threedetail/threedetail';
            return $result;
        }
        //竞拍结束
        if(now() > $bidEndTime || empty($enroll) || in_array($data['bid_status'],[28,29]) || $data['bid_status'] != 20){
            $result['bidChangeStatus']=40;
            $result['toWhere']='enddetail/enddetail';
            return $result;
        }


    }
    //通过bid_id获取 标书详情 //动态状态1 未开始，10第一轮，20第二轮，30第三轮，40竞拍结束 50流拍,60未报名
    public function getBidById($request){
        $bidId = $request['bid_id'];
        $userId = $request['user_id'];

        $data = DB::table('biding')->where('bid_id',$bidId)->first();
        unset($data['bid_user_company'],$data['bid_contact_man'],$data['bid_contact_phone'],$data['bid_user_id']);
        //订单状态检查
        $re = $this->bidStatusCheck($data);
        //产品
        $data['goods'] = DB::table('biding_goods')->where('bid_id',$bidId)->get()->toArray();
        //投标记录
        $data['record'] = DB::table('biding_record')->where('bid_id',$bidId)->get()->toArray();
        //可见最低价
        $data['showMiniPrice'] = $this->getShowMiniPrice($data['record']);
        //是否报名
        $enroll = DB::table('biding_enlist')
            ->where(['bid_id'=>$bidId,'user_id'=>$userId,'status'=>0])
            ->first();
        $data['isEnroll'] = !empty($enroll)? 1 :0; //1报名了，0没报名

        //数据格式
        $data['area'] = provinceDataMap($data['province_id']).'-'.cityDataMap($data['city_id']).'-'.areaDataMap($data['area_id']);
        $data['bid_arrival_time'] =bidArrivalTimeDataMap($data['bid_arrival_time']);//交货时间
        $data['bid_pay_type'] =bidPayTypeDataMap($data['bid_pay_type']);//付款方式


        $bid_change_status = 0;// 订单动态状态 1 未开始，10第一轮，20第二轮，30第三轮，40竞拍结束 50流拍 ,60未报名

        $bidStartTime = $data['bid_start_time'];//开标时间

        $bidOneEndTime = date( 'Y-m-d H:i:s',strtotime($bidStartTime)+$this->oneTimeLen);
        $bidTwoEndTime = date( 'Y-m-d H:i:s',strtotime($bidStartTime)+$this->oneTimeLen*2);
        $bidEndTime = date( 'Y-m-d H:i:s',strtotime($bidStartTime)+$this->oneTimeLen*3);


        //流拍
        if($re == 30){
            $data['bid_status'] = 30;
            $data['bidChangeStatus']=50;
            $data['toWhere']='enddetail/enddetail';
            return $data;

        }
        //中标
        if($re == 28){
            $data['bidChangeStatus']=40;
            $data['toWhere']='enddetail/enddetail';
            return $data;

        }
        //未开始
        if(now() < $bidStartTime && $data['bid_status'] == 20){
            $data['bidChangeStatus']=1;
            $data['toWhere']='showdetail/showdetail';
            $data['time1'] = (strtotime($bidStartTime) - time())*1000;//距离开标时间

            return $data;
        }
        //已开始 未报名
        if(now() >= $bidStartTime && empty($enroll) && now() <=$bidEndTime && $data['bid_status'] == 20){
            $data['bidChangeStatus']=60;
            $data['toWhere']='notdetail/notdetail';
            //倒计时所用时间
            $data['time1'] = (strtotime($bidEndTime) - time())*1000;//
            //如果是医院未报名
            if($request['user_type'] == 10){
                $data['title'] = '等待报价，距离报价结束还有';
                $data['txt'] = '';
            }
            //如果是供应商 未报名
            if($request['user_type'] == 11 || $request['user_type'] == 0){
                $data['title'] = '您未参与，距离报价结束还有';
                $data['txt'] = '';

            }

            unset($data['record']);
            return $data;

        }
        //第一轮中
        if(now() >= $bidStartTime && now() < $bidOneEndTime && !empty($enroll) && $data['bid_status'] == 20){
            $data['bidChangeStatus']=10;
            $data['toWhere']='onedetail/onedetail';
            $data['time1'] = (strtotime($bidOneEndTime) - time())*1000;//倒计时所用时间
            //本轮是否已报价
            $isPushPrice= $this->isPushPrice(['bid_id'=>$bidId,'user_id'=>$userId,'rotation'=>1]);//本路您是否报价
            $data['is_pushPrice'] = $isPushPrice['is_pushPrice'];
            if(isset($isPushPrice['current_pushPrice'])){
                $data['current_pushPrice'] = $isPushPrice['current_pushPrice'];//报价金额
            }
            //当前最低报价
            $minPrice = DB::table('biding_record')->where(['bid_id'=>$bidId])->min('price');
            if(!empty($minPrice)){
                $data['now_minPrice'] = $minPrice; //当前最低报价

                $mixPrice = $minPrice-$minPrice*0.004;
                $data['now_mixPush'] = round($mixPrice,2);//最高可报价金额
            }else{
                $data['now_minPrice'] = '无'; //当前最低报价
                $data['now_mixPush'] = $data['bid_budget'];//最高可报价金额

            }
            //报价 供应商名称不可见
            foreach ($data['record'] as $k=>$v){
                if(!empty($v)){
                    $data['record'][$k]['user_company'] = companyNameFilter($v['user_company']);
                    $data['record'][$k]['add_time'] = date('m/d h:i:s',strtotime($v['add_time']));
                }
            }
            return $data;
        }

        //第二轮中
        if(now() >= $bidOneEndTime && now() < $bidTwoEndTime && !empty($enroll) && $data['bid_status'] == 20){
            $data['bidChangeStatus']=20;
            $data['toWhere']='twodetail/twodetail';
            $data['time1'] = (strtotime($bidTwoEndTime) - time())*1000;//倒计时所用时间
            //本轮是否已报价
            $isPushPrice= $this->isPushPrice(['bid_id'=>$bidId,'user_id'=>$userId,'rotation'=>2]);//本路您是否报价
            $data['is_pushPrice'] = $isPushPrice['is_pushPrice'];
            if(isset($isPushPrice['current_pushPrice'])){
                $data['current_pushPrice'] = $isPushPrice['current_pushPrice'];//报价金额
            }
            //当前最低报价
            $minPrice = DB::table('biding_record')->where(['bid_id'=>$bidId])->min('price');
            $data['now_minPrice'] = !empty($minPrice)?$minPrice:$data['bid_budget']; //当前最低报价
            if(!empty($minPrice)){
                $mixPrice = $minPrice-$minPrice*0.004;
                $data['now_mixPush'] = round($mixPrice,2);//最高可报价金额
            }
            //报价 供应商名称不可见
            foreach ($data['record'] as $k=>$v){
                if(!empty($v)){
                    $data['record'][$k]['user_company'] = companyNameFilter($v['user_company']);
                    $data['record'][$k]['add_time'] = date('m/d h:i:s',strtotime($v['add_time']));

                }
            }
            return $data;
        }

        //第三轮中
        if(now() >= $bidTwoEndTime && now() < $bidEndTime && !empty($enroll) && $data['bid_status'] == 20){
            $data['bidChangeStatus']=30;
            $data['toWhere']='threedetail/threedetail';
            $data['time1'] = (strtotime($bidEndTime) - time())*1000;//倒计时所用时间
            //本轮是否已报价
            $isPushPrice= $this->isPushPrice(['bid_id'=>$bidId,'user_id'=>$userId,'rotation'=>3]);//本路您是否报价
            $data['is_pushPrice'] = $isPushPrice['is_pushPrice'];
            if(isset($isPushPrice['current_pushPrice'])){
                $data['current_pushPrice'] = $isPushPrice['current_pushPrice'];
            }
            //当前最低报价
            $minPrice = DB::table('biding_record')->where(['bid_id'=>$bidId])->min('price');
            $data['now_minPrice'] = !empty($minPrice)?$minPrice:$data['bid_budget']; //当前最低报价
            if(!empty($minPrice)){
                $mixPrice = $minPrice-$minPrice*0.004;
                $data['now_mixPush'] = round($mixPrice,2);//最高可报价金额
            }
            //报价 供应商名称 金额 不可见
            foreach ($data['record'] as $k=>$v){
                if(!empty($v)){
                    $data['record'][$k]['user_company'] = companyNameFilter($v['user_company']);
                    $data['record'][$k]['add_time'] = date('m/d h:i:s',strtotime($v['add_time']));

                }
            }
            return $data;
        }
        //竞拍结束
        if(now() > $bidEndTime || empty($enroll) || in_array($data['bid_status'],[28,29]) || $data['bid_status'] != 20){
            $data['bidChangeStatus']=40;
            $data['toWhere']='enddetail/enddetail';
            //中标人电话
            $data['prick_phone'] = userPhoneDataMap($data['pick_user_id']);
            //监督电话
            $data['supervise_phone'] = '18515116868';
            //提示语
            if($data['bid_status'] == 30){
                $data['is_show_tips'] = 1;
                $data['tips_title'] = '本次报价结果为';
                $data['tips_content'] = '报价人数不足！';
            }
            //提示语
            if($data['pick_user_id'] == $userId){
                $data['is_show_tips'] = 1;
                $data['tips_title'] = '本次报价结果为';
                $data['tips_content'] = '您已中标！请联系客服';
            }

            //投标几率，数据格式化
            foreach ($data['record'] as $k=>$v){
                if(!empty($v)){
                    $data['record'][$k]['add_time'] = date('m/d h:i:s',strtotime($v['add_time']));

                }
            }
            return $data;
        }




        return $data;

    }

    //订单状态检测 , 如果竞标，时间已过，三轮中有一轮无人投标， 则流派，
    // 正常 则待确认。 更新中标人中标信息 ，价格相同，先报价者，中标
    public function bidStatusCheck($data){

        //如果 投标已结束 并且 状态为20
        $bidStartTime = $data['bid_start_time'];//开标时间
        $bidOneEndTime = date( 'Y-m-d H:i:s',strtotime($bidStartTime)+$this->oneTimeLen);
        $bidTwoEndTime = date( 'Y-m-d H:i:s',strtotime($bidStartTime)+$this->oneTimeLen*2);
        $bidEndTime = date( 'Y-m-d H:i:s',strtotime($data['bid_start_time'])+$this->oneTimeLen*3);

        if ($data['bid_status'] == 20) {//已发布
            $record = DB::table('biding_record')
                ->where('bid_id', $data['bid_id'])
                ->get()
                ->toArray();

            //报名人数
            $enlistNum = $data['bid_enlist_num'];
            //三轮每轮报价人数
            $one = 0;
            $two = 0;
            $three = 0;
            $threePrice = []; //三轮，所有报价金额
            foreach ($record as $k => $v) {
                if ($v['rotation'] == 1) {
                    $one = $one + 1;
                }
                if ($v['rotation'] == 2) {
                    $two = $two + 1;
                }
                if ($v['rotation'] == 3) {
                    $three = $three + 1;
                }
                $threePrice[] = $v['price'];//收集第三轮所有价格
            }

            //10暂存，20已发布(竞价中)，25竞价进行中,28(中标，待医院确认)，29(供应商确认),30流拍，40效益作废，50拒收，60待确认(等待供应商确认)，70成交
            //第一轮无供应商报价，竞价结束，项目为流拍
            if ($one == 0 && now() >= $bidOneEndTime) {
                DB::table('biding')->where('bid_id', $data['bid_id'])->update(['bid_status' => 30]);
                return 30;
            }
            //第一轮只有一个家报价，竞价结束，项目为中标
            if ($one == 1 && now() >= $bidOneEndTime) {
                $this->winBid($data,$record,$threePrice);
                return 28;
            }
            //第二轮没有人投标，第一轮报价最低者中标
            if ($two == 0 && now() >= $bidTwoEndTime) {
                $this->winBid($data,$record,$threePrice);
                return 28;
            }
            //第二轮只有一个人投标，竞价结束，报价者中标
            if ($two == 1 && now() >= $bidTwoEndTime) {
                $this->winBid($data,$record,$threePrice);
                return 28;

            }
            //第三轮没有人投标，竞价结束，第二轮最低者中标
            if ($three == 0 && now() >= $bidEndTime) {
                $this->winBid($data,$record,$threePrice);
                return 28;
            }

            //如果投标时间已过，报价最低者中标
            if (now() > $bidEndTime) {
                $this->winBid($data,$record,$threePrice);

            }
        }
    }

    //中标
    public function winBid($data,$record,$threePrice){
        //中标状态
        $pickUser['bid_status'] = 28;//待医院确认
        //第三轮，最大金额是不是有两个
        $minPrice = min($threePrice);//最小金额
        $ciArr = array_count_values($threePrice); //所有金额次数
        $ci = $ciArr[$minPrice];  //有几个最小金额

        //一个最小金额
        if ($ci == 1) {
            foreach ($record as $k => $v) {
                if ($v['price'] == $minPrice) {
                    $pickUser['pick_user_id'] = $v['user_id'];
                    $pickUser['pick_user_company'] = $v['user_company'];
                    $pickUser['pick_price'] = $v['price'];

                }
            }

        }
        //多个最小金额
        if ($ci > 1) {
            $minIds=[]; //最小record_id中标，先报价中标
            foreach ($record as $k => $v) {
                if ($v['price'] == $minPrice) {
                    $minIds[]=$v['record_id'];
                }
            }
            $recordId = min($minIds);
            foreach ($record as $k => $v) {
                if ($v['record_id'] == $recordId) {
                    $minIds[]=$v['record_id'];
                    $pickUser['pick_user_id'] = $v['user_id'];
                    $pickUser['pick_user_company'] = $v['user_company'];
                    $pickUser['pick_price'] = $v['price'];
                }
            }

        }
        $res = DB::table('biding')->where('bid_id', $data['bid_id'])->update($pickUser);
        $noticeModel = new Notice();
        $notice = [
            'type'=>1,//中标通知
            'title'=>'',
            'content'=>'恭喜！供应商'.$pickUser['pick_user_company'].','.$data['bid_name'].'项目中标！时间'.now(),
        ];
        $noticeModel->pushNotice($notice);
        $this->pushPickBidSms($pickUser['pick_user_id'],$data);
        return 28;


    }

    //本轮是否已报价
    public function isPushPrice($data){
        $record = DB::table('biding_record')
            ->where(['bid_id'=>$data['bid_id'],'user_id'=>$data['user_id'],'rotation'=>$data['rotation']])
            ->first();
        $re=[];
        if(!empty($record)){
            $re['is_pushPrice'] = 1;
            $re['current_pushPrice'] = $record['price'];

        }else{
            $re['is_pushPrice'] = 0;
        }
        return  $re;

    }

    //当前可见最低价
    public function getShowMiniPrice($data){
        $oneTwoPrice =[];
        if(!empty($data)){
            foreach ($data as $k=>$v){
                if($v['rotation'] == 1 || $v['rotation'] == 2){
                    $oneTwoPrice[]= $v['price'];

                }
            }
            return min($oneTwoPrice);

        }else{
            return '暂无';
        }
    }

    //报价
    public function pushPrice($data){
//


    }

    //发送中标短信
    public function pushPickBidSms($pickUser){

        $content = '【医竞采】恭喜，'.$pickUser['company_name'].'，您参与的'.$pickUser['bid_name'].'项目，已中标，如有疑问，请联系客服！';

        f_bidSendMsg($pickUser['user_phone'],$content);

    }
    //发送开标提示
    public function pushStartBidSms($bid){
        //给发表人发开标提醒
        $createUser = DB::table('user')->where('user_id',$bid['bid_user_id'])->first();
        if(!empty($createUser)){
            $c = '【医竞采】您发布的，'.$bid['bid_name'].'项目，还有30分钟开标，请及时关注！';
            f_bidSendMsg($createUser['user_phone'],$c);
        }

        //给参与人发短信
        $ids = DB::table('biding_enlist')->where('bid_id',$bid['bid_id'])->pluck('user_id');
        if(!empty($ids)){
            $msg = '【医竞采】您报名的，'.$bid['bid_name'].'项目，还有30分钟开标，请及时关注！';
            foreach ($ids as $v){
                f_bidSendMsg(userPhoneDataMap($v),$msg);

            }
        }


    }
}
