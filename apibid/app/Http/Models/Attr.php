<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Attr extends Model
{
    protected $table = 'product_attribute';

    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $guarded = [];

    public function getAttrIdsById($id){
        //unserialize
        $data = DB::table('product_attribute_group')->where('id', $id)->pluck('attr_ids');
        $ids =unserialize($data[0]);
        $idsArr = [];
        if(!empty($ids)){
            foreach ($ids as $v){
                $idsArr[] = $v['attr_id'];
            }
        }

        return $idsArr;
    }

    //获取全部属性组
    public function getAttrGroupList(){

        $data['status'] = 1;
        $data = DB::table('product_attribute_group')->get()->toArray();

        return $data;
    }


    //分类下的属性
    public function getAttrListById($id){

        $attrList = $this->where('group_id', $id)->get()->toArray();
        foreach ($attrList as $k=>$v){
            $attrList[$k]['display_data'] = unserialize($v['display_data']);
        }

        return $attrList;
    }










}
