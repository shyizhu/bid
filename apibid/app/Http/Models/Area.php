<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Area extends Model
{
    protected $table = 'area';
    protected $primaryKey = 'area_id';
    protected $fillable = [];
    protected $perPage = 20;
    protected $guarded		= [];

    //关闭框架更新时间
    public $timestamps = false;


    public function getListByParentId($parent_id){
        if(!isset($parent_id) ){
            return [];
        }
        $field = [
            'area_id',
            'region as area_region',
            'parent_id as area_parentid',
            'area_name as area_name',
            'level as area_level',
            'short_name',
            'pinyin',
            'lng',
            'lat',
            'position',
            'sort',
            'is_del as area_isdel',
        ];
        if( is_array($parent_id) ){
            $res = $this->select($field)->whereIn('parent_id',$parent_id)->get()->toArray();
            //按company_id为数组的key重新组合
            $data = collect($res)->keyBy('area_id')->all();
            return $data;
        }

        $res = $this->select($field)->where('parent_id','=',$parent_id)->get()->toArray();
        return isset($res) && !empty($res) ? $res : [];
    }



    public function getListByAreaId($areaId){
        if( empty($areaId) ){
            return [];
        }
        $field = [
            'area_id',
            'region as area_region',
            'parent_id as area_parentid',
            'area_name as area_name',
            'level as area_level',
            'short_name',
            'pinyin',
            'lng',
            'lat',
            'position',
            'sort',
            'is_del as area_isdel',
        ];
        if( is_array($areaId) ){
            $res = $this->select($field)->whereIn('area_id',$areaId)->get()->toArray();
            //按company_id为数组的key重新组合
            $data = collect($res)->keyBy('area_id')->all();
            return $data;
        }

        $res = $this->select($field)->where('area_id','=',$areaId)->get()->toArray();
        return isset($res[0]) && !empty($res[0]) ? $res[0] : [];
    }



    //获得需要验证的列表
    public function getList(){
        $data = $this->select()->where('level', 2)->get()->toArray();
        return $data;
    }


    // 获取省级数据
    public function getProvinceData(){
        $data = $this->select()->where('level', 1)->get()->toArray();
        return $data;
    }


    //修改
    public function updateByAreaId($area_id,array $data){
        if(!empty($area_id)  && $area_id>0 && !empty($data) ){
            return $this->where('area_id','=',$area_id)->update($data);
        }
        return false;
    }

    //获取省级信息
    public function getAllProvince(){
        $province = DB::table('area')->select('area_id as id','area_name as name')->where('level', 1)->get();
        $data = array();
        foreach($province as $v){
            $data[$v['id']] = $v['name'];
        }
        return $data;
    }



    //获取所有市信息
    public function getAllCity()
    {
        $province = DB::table('area')->select('area_id as id','area_name as name')->where('level', 2)->get();
        $data = array();
        foreach($province as $v){
            $data[$v['id']] = $v['name'];
        }
        return $data;
    }


    //获取区级信息
    public function getAllArea(){
        $province = DB::table('area')->select('area_id as id','area_name as name')->where('level', 3)->get();
        $data = array();
        foreach($province as $v){
            $data[$v['id']] = $v['name'];
        }
        return $data;
    }



}
