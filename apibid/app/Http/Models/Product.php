<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
    protected $table = 'product_flat';

    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $guarded = [];


    //获取全部商品
    public function getList($request){
        //页数
        $pageNum = !empty($request['pageNum'])?$request['pageNum']:1;
        //每页条数
        $pageSize = !empty($request['pageSize'])?$request['pageSize']:10;


        $data= [];


        $data['total'] =$this->count();
        $data['productList'] = $this->orderBy('up_time', 'desc')->skip(($pageNum - 1) * $pageSize)->take($pageSize)->get()->toArray();

        return $data;
    }










}
