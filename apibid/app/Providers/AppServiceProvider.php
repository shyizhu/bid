<?php

namespace App\Providers;

use DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        DB::listen(function($query){
            $sql = $query->sql;
            $bingings = $query->bindings;
            $time = $query->time;

            if($time>1000){
                writeLog(compact('sql','bingings','time'),'slow_query','slow_query');
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
