import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Test from './views/Test.vue'

//register 登录/注册
import Login from './views/register/Login.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    //主页
    {
      path: '/',
      name: 'home',
      component: Home,
      meta:{
        title:'「医竞采」医疗行业智慧招标交易平台-件件低价'
      }
    },
    //test
    {
      path: '/test',
      name: 'test',
      component: Test
    },
    /*
      register 登录/注册
      import Status from './views/register/Status.vue'
      import Cgregister from './views/register/Cgregister.vue'
      import Cgupload from './views/register/Cgupload.vue'
      import Cgagree from './views/register/Cgagree.vue'
      import Gysregister from './views/register/Gysregister.vue'
      import Gysupload from './views/register/Gysupload.vue'
      import Gysagree from './views/register/Gysagree.vue'
    */
    {
      path: '/register/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register/status',
      name: 'status',
      component: () => import('./views/register/Status.vue')
    },
    {
      path: '/register/cgregister',
      name: 'cgregister',
      component: () => import('./views/register/Cgregister.vue')
    },
    {
      path: '/register/cgupload',
      name: 'cgupload',
      component: () => import('./views/register/Cgupload.vue')
    },
    {
      path: '/register/cgagree',
      name: 'cgagree',
      component: () => import('./views/register/Cgagree.vue')
    },
    {
      path: '/register/gysregister',
      name: 'gysregister',
      component: () => import('./views/register/Gysregister.vue')
    },
    {
      path: '/register/gysupload',
      name: 'gysupload',
      component: () => import('./views/register/Gysupload.vue')
    },
    {
      path: '/register/gysagree',
      name: 'gysagree',
      component: () => import('./views/register/Gysagree.vue')
    },

    /*
       my 我的
      import My from './views/my/My.vue'
      import Notice from './views/my/Notice.vue'
      import Bidlist from './views/my/Bidlist.vue'
      import Usertype from './views/my/Usertype.vue'
      import Editpass from './views/my/Editpass.vue'
      import Inviteman from './views/my/Inviteman.vue'

    */
  
    {
      path: '/my/my',
      name: 'my',
      component: () => import('./views/my/My.vue')
    },
    {
      path: '/my/notice',//消息
      name: 'notice',
      component: () => import('./views/my/Notice.vue')
    },
    {
      path: '/my/bidlist',//订单
      name: 'bidlist',
      meta:{
            requireAuth:true,
      },
      component: () => import('./views/my/Bidlist.vue')
    },
    {
      path: '/my/usertype', //认证
      name: 'usertype',
      component: () => import('./views/my/Usertype.vue')
    },
    {
      path: '/my/editpass',//修改密码
      name: 'editpass',
      component: () => import('./views/my/Editpass.vue')
    },
    {
      path: '/my/inviteman',//成为合伙人
      name: 'inviteman',
      component: () => import('./views/my/Inviteman.vue')
    },
    
    /*
      bid 招投标
      import Pushbid from './views/bid/Pushbid.vue'
      import Showdetail from './views/bid/Showdetail.vue'
      import Onedetail from './views/bid/Onedetail.vue'
      import Twodetail from './views/bid/Twodetail.vue'
      import Threedetail from './views/bid/Threedetail.vue'
      import Notdetail from './views/bid/Notdetail.vue'
      import Enddetail from './views/bid/Enddetail.vue'
      import Typebidlist from './views/bid/Typebidlist.vue'

    */
    {
      path: '/bid/pushbid',
      name: 'pushbid',
      meta:{
            requireAuth:true,
      },
      component: () => import('./views/bid/Pushbid.vue')
    },
    {
      path: '/bid/showdetail/showdetail',
      name: 'showdetail',
      component: () => import('./views/bid/Showdetail.vue')
    },
    {
      path: '/bid/onedetail/onedetail',
      name: 'onedetail',
      component: () => import('./views/bid/Onedetail.vue')
    },
    {
      path: '/bid/twodetail/twodetail',
      name: 'twodetail',
      component: () => import('./views/bid/Twodetail.vue')
    },
    {
      path: '/bid/threedetail/threedetail',
      name: 'threedetail',
      component: () => import('./views/bid/Threedetail.vue')
    },
    {
      path: '/bid/notdetail/notdetail',
      name: 'notdetail',
      component: () => import('./views/bid/Notdetail.vue')
    },
    {
      path: '/bid/enddetail/enddetail',
      name: 'enddetail',
      component: () => import('./views/bid/Enddetail.vue')
    },
    {
      path: '/bid/typebidlist',
      name: 'typebidlist',
      component: () => import('./views/bid/Typebidlist.vue')
    },
    
    /*
      文档
    */
    {
      path: '/file/filelist',
      name: 'Rulelist',
      component: () => import('./views/file/Filelist.vue')
    },
    {
      path: '/file/rule',
      name: 'rule',
      component: () => import('./views/file/Rule.vue')
    },
    {
      path: '/file/banfa',
      name: 'banfa',
      component: () => import('./views/file/Banfa.vue')
    },
    {
      path: '/file/guiding',
      name: 'guiding',
      component: () => import('./views/file/Guiding.vue')
    },
    {
      path: '/file/about',
      name: 'about',
      component: () => import('./views/file/About.vue')
    },


    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    // }
  ]
})
