//index.js
//获取应用实例
const app = getApp()
const util = require('/../../utils/util.js');


Page({

  data: {
    banner: [],//轮播图片路径
    indicatorDots: true, // 是否显示轮播图指示点
    autoplay: true,  //是否自动播放
    interval: 5000,  //轮播间隔时间
    duration: 500,   //轮播滚动所用时间
    //以上 轮播数据
    pos: 'left', //滑动导航指示点判断数据
    ad:'',//广告语
    notice: [],//公告轮播数据
    total:[],//统计
    tabImg1: '../../img/index/tabs_bg1.png',  //选项卡标题背景图片
    tabImg2: '../../img/index/tabs_bg2.png',  //选项卡标题背景图片
    tabImg3: '../../img/index/tabs_bg2.png',  //选项卡标题背景图片
    up_img: '../../img/index/up.png',//上三角
    down_img: '../../img/index/down.png',//下三角

    tab:20,//tab 20 招标中 70已完成，100 已收藏
    tabIndex: 1,
    bidBeing: {},//已发布
    bidDone: {},//已完成
    bidColl: {},//收藏的
    bidBeingPage:1,
    bidDonePage:1,
    bidCollPage:1,
    userInfo:'',
    listData:[],
    downTime:''
  },
  left: function (e) {//滑动导航滑动至左侧 第一个指示点添加active
    this.setData({
      pos: e.detail.direction
    })
  },
  right: function (e) {//滑动导航滑动至左侧 第二个指示点添加active
    this.setData({
      pos: e.detail.direction
    })
  },
  baoming:function(e){   //报名
    var that = this;
    var bidId = e.currentTarget.dataset.bidid;
    var id = e.currentTarget.id;
    var arr = that.data.bidBeing;
    var userInfo = wx.getStorageSync('userInfo');
    //tab 20 招标中 70已完成，100 已收藏
    if(userInfo == ''){
      wx.navigateTo({
         url: '/pages/register/auth/auth',
      })

    }else if(userInfo != '' && userInfo.phoneNumber == undefined){
        wx.navigateTo({
            url: '/pages/register/cj_login/cj_login',
        })

    }else{

      wx.request({
      url: app.globalData.apiUrl + 'bid/enrollBid',
      method: 'post',
      header: that.data.header,
      data: {
        user_id:userInfo.user_id,
        user_token:userInfo.user_token,
        bid_id:bidId

      },
      success: function (re) {
        if(re.data.code == 200){
            wx.showToast({
            title: '报名成功',
            icon: 'success',
            duration: 2000,
            mask: true
          })
          //改变报名按钮
          arr[id].is_enlist = 1;
          that.setData({
             bidBeing:arr
          })
        //未认证
        }else if(re.data.code == 201){
          wx.navigateTo({
            url: '/pages/register/gys_register/gys_register',
          })
        }else{
          wx.showToast({
            title: re.data.msg,
            icon: 'none',
            duration: 2000,
            mask: true
            })
        }
      },
    }) 
   }
  },
  tabChange: function(e) {  //选项卡切换
    let that = this;
    let tab = e.currentTarget.dataset.tab;
    let userInfo = wx.getStorageSync('userInfo');
    

    //tab 20 招标中 70已完成，100 已收藏
    if(tab == 100 && userInfo == ''){
      wx.navigateTo({
         url: '/pages/register/auth/auth',
      })

    }else if(tab == 100 && userInfo != '' && userInfo.phoneNumber == undefined){
        wx.navigateTo({
            url: '/pages/register/cj_login/cj_login',
        })

    }else{
    //数据改变
          wx.request({
              url: app.globalData.apiUrl + 'index/tabBidList',
              method: 'post',
              header: that.data.header,
              data: {
                user_id:userInfo.user_id?userInfo.user_id:0,
                user_token:userInfo.user_token?userInfo.user_token:0,
                tab:tab
              },
              success: function (re) {

                  if(re.data.code == 200){
                      if(tab == 20){
                            that.setData({
                             bidBeing: re.data.data.data,
                             bidBeingPage:re.data.data.page,
                             tab:20
                           });
                      }else if(tab ==70){
                            that.setData({
                             bidDone: re.data.data.data,
                             bidDonePage:re.data.data.page,
                             tab:70
                           });

                      }else if(tab ==100){
                              that.setData({
                               bidColl: re.data.data.data,
                               bidCollPage:re.data.data.page,
                               tab:100
                            });

                      }
                    
                           // that.setCountDown();

                  }
                }

              })

    }


    //图片切换
    if(tab == 20){
      that.setData({
        tabImg1: '../../img/index/tabs_bg1.png',
        tabImg2: '../../img/index/tabs_bg2.png',
        tabImg3: '../../img/index/tabs_bg2.png',
        tabIndex: 1
      })
    } else if (tab == 70) {
      that.setData({
        tabImg1: '../../img/index/tabs_bg2.png',
        tabImg2: '../../img/index/tabs_bg1.png',
        tabImg3: '../../img/index/tabs_bg2.png',
        tabIndex: 2
      })
    } else if (tab == 100) {
      that.setData({
        tabImg1: '../../img/index/tabs_bg2.png',
        tabImg2: '../../img/index/tabs_bg2.png',
        tabImg3: '../../img/index/tabs_bg1.png',
        tabIndex: 3
      })
    }

    
  },

  showText:function(e){
    var id = e.currentTarget.id;
    var that = this;
    var arr = that.data.bidBeing;
    if (arr[id].show_remark == false) {
      arr[id].show_remark = true;
      arr[id].show_remark_ud = that.data.down_img;

      that.setData({
        bidBeing: arr
      })
    } else {
      arr[id].show_remark = false;
      arr[id].show_remark_ud = that.data.up_img;
      that.setData({
        bidBeing: arr
      })
    }
    
  },
  showText2:function(e){
    var id = e.currentTarget.id;
    var that = this;
    var arr = that.data.bidDone;
    if (arr[id].show_remark == false) {
      arr[id].show_remark = true;
      arr[id].show_remark_ud = that.data.down_img;

      that.setData({
        bidDone: arr
      })
    } else {
      arr[id].show_remark = false;
      arr[id].show_remark_ud = that.data.up_img;
      that.setData({
        bidDone: arr
      })
    }
   
  },
  showText3: function (e) {
    var id = e.currentTarget.id;
    var that = this;
    var arr = that.data.bidColl;
    if (arr[id].show_remark == false) {
      arr[id].show_remark = true;
      arr[id].show_remark_ud = that.data.down_img;

      that.setData({
        bidColl: arr
      })
    } else {
      arr[id].show_remark = false;
      arr[id].show_remark_ud = that.data.up_img;
      that.setData({
        bidColl: arr
      })
    }
  },
  //事件处理函数
  bindViewTap: function() {

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    let userInfo = wx.getStorageSync('userInfo');
            wx.request({
              url: app.globalData.apiUrl + 'index/indexdata',
              method: 'post',
              header: that.data.header,
              data: {
                user_id:userInfo.user_id?userInfo.user_id:0,
                user_token:userInfo.user_token?userInfo.user_token:0,

              },
              success: function (re) {

                  if(re.data.code == 200){ 
                    that.setData({
                       banner:re.data.data.banner,
                       ad:re.data.data.ad,
                       notice:re.data.data.notice,
                       total:re.data.data.total,
                       bidBeing: re.data.data.bidBeing.data,
                       bidBeingPage:re.data.data.bidBeing.page
                    });
                      that.setCountDown();

                  }
                }

              })

        

      },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
   onReady: function () {

   },

  /**
   * 生命周期函数--监听页面显示
   */
   onShow: function () {

   },

  /**
   * 生命周期函数--监听页面隐藏
   */
   onHide: function () {

   },

  /**
   * 生命周期函数--监听页面卸载
   */
   onUnload: function () {
    clearInterval(this.data.downTime);

   },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
   onPullDownRefresh: function () {
    wx.showNavigationBarLoading() //在标题栏中显示加载
    let that = this;
    clearInterval(this.data.downTime);
    let userInfo = wx.getStorageSync('userInfo');
            wx.request({
              url: app.globalData.apiUrl + 'index/indexdata',
              method: 'post',
              header: that.data.header,
              data: {
                user_id:userInfo.user_id?userInfo.user_id:0,
                user_token:userInfo.user_token?userInfo.user_token:0,

              },
              success: function (re) {
                  if(re.data.code == 200){ 
                    that.setData({
                       banner:re.data.data.banner,
                       ad:re.data.data.ad,
                       notice:re.data.data.notice,
                       bidBeing: re.data.data.bidBeing.data,
                       bidBeingPage:re.data.data.bidBeing.page
                    });

                      that.setCountDown();
                  }
                }

      })
    setTimeout(function () {

      wx.hideNavigationBarLoading() //完成停止加载

      wx.stopPullDownRefresh() //停止下拉刷新

    }, 1500);

   },

  /**
   * 页面上拉触底事件的处理函数
   */
   onReachBottom: function () {
    let that = this;
    let tab = that.data.tab;
    let page = 1;
    let bidBeingPage = that.data.bidBeingPage
    let bidDonePage = that.data.bidDonePage
    let bidCollPage = that.data.bidCollPage
    let userInfo = wx.getStorageSync('userInfo');
    clearInterval(this.data.downTime);
    wx.showLoading({
      title: '加载中',
    })

    if(tab ==20){
      page = bidBeingPage+1;
    }else if(tab == 70){
      page = bidDonePage+1;
    }else if(tab == 100){
      page = bidCollPage+1;
    }
    //数据改变
          wx.request({
              url: app.globalData.apiUrl + 'index/tabBidList',
              method: 'post',
              header: that.data.header,
              data: {
                user_id:userInfo.user_id?userInfo.user_id:0,
                user_token:userInfo.user_token?userInfo.user_token:0,
                tab:tab,
                page:page
              },
              success: function (re) {
              wx.hideLoading();

                  if(re.data.code == 200){
                      if(tab == 20){
                            that.setData({
                             bidBeing: re.data.data.data,
                             bidBeingPage:re.data.data.page,
                             tab:20
                           });
                      }else if(tab ==70){
                            that.setData({
                             bidDone: re.data.data.data,
                             bidDonePage:re.data.data.page,
                             tab:70
                           });

                      }else if(tab ==100){
                              that.setData({
                               bidColl: re.data.data.data,
                               bidCollPage:re.data.data.page,
                               tab:100
                            });

                      }
                    
                      that.setCountDown();

                  }
                }

              })
  


   },

  /**
   * 用户点击右上角分享
   */
   onShareAppMessage: function () {

   },
   //单类型 标书列表
   toTypeBidList:function(e){
      var bidtype = e.currentTarget.dataset.bidtype;


      wx.navigateTo({
        url: '/pages/bid/typebidlist/typebidlist?bidtype='+bidtype,
      })

   },
  //标书详情
  toDetail:function(e){
    var that =this;
    var userInfo = wx.getStorageSync('userInfo');
    var bidid = e.currentTarget.dataset.bidid;
    let  tabnum  =  e.currentTarget.dataset.tabnum;
  if(tabnum != 1 && userInfo == ''){
       wx.navigateTo({
        url: '/pages/register/auth/auth',
      })
     }else if(tabnum != 1 && userInfo != '' && userInfo.phoneNumber == undefined){
      wx.navigateTo({
        url: '/pages/register/cj_login/cj_login',
      })

     }else{
       wx.request({
              url: app.globalData.apiUrl + 'bid/getGoWhere',
              method: 'post',
              header: that.data.header,
              data: {
                user_token:userInfo.user_token?userInfo.user_token:0,
                bid_id:bidid
              },
              success: function (re) {

                  if(re.data.code == 200){
                wx.navigateTo({
                  url: '/pages/bid/'+re.data.data.toWhere+'?bid_id='+bidid,
                })
                  }
                }
        })
     }
    
   },
   //到公告列表
   toNotice:function(){
        wx.reLaunch({
          url: '/pages/my/notice/notice',
        })
   },
   
   //倒计时
   setCountDown: function(){
    let that = this;
    let time = 1000;
    let  lts = that.data.bidBeing;
    that.setData({
         downTime: setInterval(function(){
          let list = lts.map((v, i) =>{
            if (v.time1 <= 0) {
              v.time1 = 0;
          }
           let formatTime = that.getFormat(v.time1);
            v.time1 -= time;
            v.countDown = `${formatTime.hh}时${formatTime.mm}分${formatTime.ss}秒`;
            return v;

          })
          that.setData({
            bidBeing: list
          });

       }, time),

    })
  
  },
    //时间格式化
    getFormat: function (msec){
      let ss = parseInt(msec / 1000);
      let ms = parseInt(msec % 1000);
      let mm = 0;
      let hh = 0;
      if (ss > 60) {
        mm = parseInt(ss / 60);
        ss = parseInt(ss % 60);
        if (mm > 60) {
          hh = parseInt(mm / 60);
          mm = parseInt(mm % 60);
        }
      }
      ss = ss > 9 ? ss : `0${ss}`;
      mm = mm > 9 ? mm : `0${mm}`;
      hh = hh > 9 ? hh : `0${hh}`;
      return { ms, ss, mm, hh };
    }



  })
