//index.js
//获取应用实例
const app = getApp()
Page({
  bid_data:{},

  data: {
    title: '医疗器械采购',  //页面标题
    title_price: '400000',  //中标金额
    isTime: false,//为true时 显示倒计时   为 false 显示金额
    h: '00',
    m: '25',
    s: '35',
    fb_name: '宋**',//发标用户
    cy_man: 5,//参与人数
    fb_time: '2019.07.22',//发布时间
    zb_user: '付**',//中标用户
    bz: '招标项目需求简介，需要大量采购，希望服务者给的价格低一点，服务人员 耐心 专业水平高 办事利索 请尽快联系我',//备注
    mx: [{ //投标明细循环数据
      name: '曲**',//姓名
      date: '2019/07/29',//日期
      price: '*****'//价格
    },{ //投标明细循环数据
      name: '曲**',//姓名
      date: '2019/07/29',//日期
      price: '*****'//价格
    }, { //投标明细循环数据
      name: '曲**',//姓名
      date: '2019/07/29',//日期
      price: '*****'//价格
    }, { //投标明细循环数据
      name: '曲**',//姓名
      date: '2019/07/29',//日期
      price: '*****'//价格
    }, { //投标明细循环数据
      name: '曲**',//姓名
      date: '2019/07/29',//日期
      price: '*****'//价格
    }]
  },
  //事件处理函数
  bindViewTap: function () {

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that =this;
    var userInfo = wx.getStorageSync('userInfo');
    var bidid = options.bid_id;
    wx.request({
              url: app.globalData.apiUrl + 'bid/getBidDetail',
              method: 'post',
              header: that.data.header,
              data: {
                user_token:userInfo.user_token?userInfo.user_token:0,
                bid_id:bidid
              },
              success: function (re) {
                  if(re.data.code == 200){
              
                        that.setData({
                          bid_data:re.data.data,
                        
                        })
                        
                  }
                }

        })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
