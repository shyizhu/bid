// pages/my/my.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    headImg: '../../../img/my/headImg.png',
    userInfo:'',
    biding_num_text:'招标中',
    enlist_num_text:'已投',
    biding_num:0,
    enlist_num:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that =this;
    var userInfo = wx.getStorageSync('userInfo');
    if(userInfo){
      that.setData({
          userInfo:userInfo
      })
    }
    if(userInfo.user_token){
            wx.request({
                url: app.globalData.apiUrl + 'user/myInit',
                method: 'post',
                header: that.data.header,
                data: {
                  user_token:userInfo.user_token,    
                },
                success: function (re) {
                  if(re.data.code == 200){
                    that.setData({
                      biding_num:re.data.data.biding_num,
                      enlist_num:re.data.data.enlist_num,
                      biding_num_text:re.data.data.biding_num_text,
                      enlist_num_text:re.data.data.enlist_num_text,

                    })

                  }
                },
              })

    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //去标书列表
  toDidlist:function(e){
     let statusId =e.currentTarget.dataset.statusid
      var userInfo = wx.getStorageSync('userInfo');
        if(userInfo == ''){
              wx.navigateTo({
                  url: '/pages/register/auth/auth',
              })
          }else if(userInfo != '' && userInfo.phoneNumber == undefined){
                wx.navigateTo({
                    url: '/pages/register/cj_login/cj_login',
                })

          }else{
              wx.reLaunch({
                    url: '/pages/my/bidlist/bidlist?statusId='+statusId,
                })

          }
     

  },
  //登录注册
  toLogin:function(){
      var userInfo = wx.getStorageSync('userInfo');
          if(userInfo == ''){
              wx.navigateTo({
                  url: '/pages/register/auth/auth',
              })
          }else if(userInfo != '' && userInfo.phoneNumber == undefined){
                wx.navigateTo({
                    url: '/pages/register/cj_login/cj_login',
                })

          }

  },
  //去认证
  toAuth:function(){
      let that =this;
      let userInfo = wx.getStorageSync('userInfo');
        if(userInfo == ''){
              wx.navigateTo({
                  url: '/pages/register/auth/auth',
              })
          }else if(userInfo != '' && userInfo.phoneNumber == undefined){
                wx.navigateTo({
                    url: '/pages/register/cj_login/cj_login',
                })

          }else{
            wx.request({
                    url: app.globalData.apiUrl + 'user/getUserByToken',
                    method: 'post',
                    header: that.data.header,
                    data: {
                      user_token:userInfo.user_token,    
                    },
                    success: function (re) {
                      if(re.data.code == 200){
                          if(re.data.data.user_type ==0){
                              wx.navigateTo({
                                url: '/pages/register/status/status',
                            })
                          }else{
                            wx.navigateTo({
                                url: '/pages/my/usertype/usertype',
                            })

                          }
                      
                       }
                    },
                  })
            
          }

  },//去关于我们
  toAbout:function(){
            wx.navigateTo({
                    url: '/pages/my/about/about',
            }) 
  },
  //修改密码
  toEditpass:function(){
     var userInfo = wx.getStorageSync('userInfo');
        if(userInfo == ''){
              wx.navigateTo({
                  url: '/pages/register/auth/auth',
              })
          }else if(userInfo != '' && userInfo.phoneNumber == undefined){
                wx.navigateTo({
                    url: '/pages/register/cj_login/cj_login',
                })

          }else if(userInfo.user_type != 10 && userInfo.user_type != 11 && userInfo.user_type != 9){
              
                wx.navigateTo({
                    url: '/pages/register/status/status',
                })

          }else{
                wx.navigateTo({
                    url: '/pages/my/editpass/editpass',
                })
              
          }

  },
  //成为合伙人
  toInviteman:function(){
    let that = this
    var userInfo = wx.getStorageSync('userInfo');

        if(userInfo == ''){
              wx.navigateTo({
                  url: '/pages/register/auth/auth',
              })
          }else if(userInfo != '' && userInfo.phoneNumber == undefined){
                wx.navigateTo({
                    url: '/pages/register/cj_login/cj_login',
                })

          }else{
               wx.request({
                    url: app.globalData.apiUrl + 'user/getUserByToken',
                    method: 'post',
                    header: that.data.header,
                    data: {
                      user_token:userInfo.user_token,    
                    },
                    success: function (re) {
                      if(re.data.code == 200){
                          if(re.data.data.is_invite ==0){
                              wx.showModal({
                              title: '提示',
                              content: '成为合伙人,生成邀请码,可推荐同行入驻,赢取返利',
                              success (res) {
                                //如果点击确认，生成邀请码
                                if (res.confirm) {
                                  wx.request({
                                    url: app.globalData.apiUrl + 'user/goInvite',
                                    method: 'post',
                                    header: that.data.header,
                                    data: {
                                      user_token:userInfo.user_token,    
                                    },
                                    success: function (r) {
                                      if(r.data.code == 200){
                                        wx.navigateTo({
                                         url: '/pages/my/inviteman/inviteman',
                                        })
                                      }
                                      
                                    },
                                  })

                                }
                              }
                            })
                          }else{
                            wx.navigateTo({
                                url: '/pages/my/inviteman/inviteman',
                            })

                          }
                      
                       }
                    },
                  })
            

          }
  },
  //退出账号
  outLogin:function(){
    var that =this;
    var userInfo = wx.getStorageSync('userInfo');

      if(userInfo != ''){
          wx.showModal({
          title: '提示',
          content: '退出登录，无法看到订单信息，确定退出？',
          success (res) {
            if (res.confirm) {
                wx.setStorageSync('userInfo','');
                that.setData({
                    userInfo:''
                },function(){
                    wx.reLaunch({
                      url: '/pages/my/home/home'
                    })
                })          
              }
          }
        })

      }else{
            wx.showToast({
              title: '您还没有登录',
              icon: 'none',
              duration: 2000,
              mask: true
            })

      }
      

      
      
  },
  //规则
  toRule:function(){
          wx.navigateTo({
            url: '/pages/my/rule/rule'
          })

  }


    
})