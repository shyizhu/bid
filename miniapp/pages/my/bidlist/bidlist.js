//index.js
//获取应用实例
const app = getApp()
Page({
  data: {
    is_show:true,
    navData: [{text: '招标中'},{text: '已中标'},{text: '未中标'},{text: '报价人数不足'},
    {text: '效益作废'},{text: '拒收'},{text: '等待确认结果'},{text: '成交'}],

    bidList:{},
    tab:0,
    currentTab: 0, //导航菜单使用数据
    navScrollLeft: 0, //导航菜单使用数据
    lts2: {},
    isNull: true,


    up_img: '../../../img/index/up.png',//上三角
    down_img: '../../../img/index/down.png',//下三角
  },
  showText3: function (e) {//选项卡第三项 点击向下三角 简介全部展示
     var id = e.currentTarget.id;

     console.log(id);

    var that = this;
    var arr = that.data.bidList;
    if (arr[id].show_remark == false) {
      arr[id].show_remark = true;
      arr[id].show_remark_ud = that.data.down_img;

      that.setData({
        bidList: arr
      })
    } else {
      arr[id].show_remark = false;
      arr[id].show_remark_ud = that.data.up_img;
      that.setData({
        bidList: arr
      })
    }
  },
  //事件处理函数
  bindViewTap: function () {
    
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.getSystemInfo({
      success: (res) => {
        this.setData({
          pixelRatio: res.pixelRatio,
          windowHeight: res.windowHeight,
          windowWidth: res.windowWidth
        })
      }
    })
    let that = this;
    let tab = options.statusId?options.statusId:0;
    let userInfo = wx.getStorageSync('userInfo');
    //判断是否登录
    if(userInfo == ''){
            that.setData({
              is_show:false
            })
      }else if(userInfo != '' && userInfo.phoneNumber == undefined){
            that.setData({
              is_show:false
            })
     }else{
            //每个tab选项宽度占1/3
          var singleNavWidth = this.data.windowWidth / 3;
          //tab选项居中                            
          that.setData({
            navScrollLeft: (tab - 1) * singleNavWidth,
            currentTab: tab,
          })

           wx.request({
            url: app.globalData.apiUrl + 'bid/statusBidList',
            method: 'post',
            header: that.data.header,
            data: {
              user_id:userInfo.user_id,
              user_token:userInfo.user_token,
              tab:tab

            },
            success: function (re) {
              if(re.data.code == 200){
                  if(Object.keys(re.data.data).length == 0){
                      that.setData({
                        isNull: true
                      })
                  }else{
                      that.setData({
                            isNull: false
                      })
                  }
                  that.setData({
                      bidList: re.data.data
                  })
              }
            },
          }) 


     }


       
    
    
  },
  switchNav(event) {  //导航栏点击事件
    let that = this;
    let userInfo = wx.getStorageSync('userInfo');
    var tab = event.currentTarget.dataset.current;

    //每个tab选项宽度占1/3
    var singleNavWidth = this.data.windowWidth / 3;
    //tab选项居中                            
    this.setData({
      navScrollLeft: (tab - 1) * singleNavWidth,
      currentTab: tab,
    })
    
      wx.request({
      url: app.globalData.apiUrl + 'bid/statusBidList',
      method: 'post',
      header: that.data.header,
      data: {
        user_id:userInfo.user_id,
        user_token:userInfo.user_token,
        tab:tab

      },
      success: function (re) {
        if(re.data.code == 200){
            if(Object.keys(re.data.data).length == 0){
                that.setData({
                  isNull: true
                })
            }else{
                that.setData({
                      isNull: false
                })
            }
            that.setData({
                bidList: re.data.data
            })
            }
          },
        }) 

    },
  switchTab(tab) {
    let that =this
    //每个tab选项宽度占1/3
    var singleNavWidth = this.data.windowWidth / 3;
    //tab选项居中                            
    that.setData({
      navScrollLeft: (tab - 1) * singleNavWidth,
      currentTab: tab,

    })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading() //在标题栏中显示加载

    setTimeout(function () {

      wx.hideNavigationBarLoading() //完成停止加载

      wx.stopPullDownRefresh() //停止下拉刷新

    }, 1500);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    wx.showLoading({
      title: '加载中',
    })
    setTimeout(function(){
      wx.hideLoading();
    },1500)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },
  //医院供应商确认
  istrue:function(e){
    var that =this;
    var userInfo = wx.getStorageSync('userInfo');
    var bidid = e.currentTarget.dataset.bidid;
     var id = e.currentTarget.id;
    let arr = that.data.bidList;
     wx.request({
              url: app.globalData.apiUrl + 'bid/isTrue',
              method: 'post',
              header: that.data.header,
              data: {
                user_token:userInfo.user_token?userInfo.user_token:0,
                bid_id:bidid
              },
              success: function (re) {

                  if(re.data.code == 200){
                    arr[id].is_true = 1;
                    that.setData({
                      bidList: arr
                    })
                  }
                }
        })

  },
  //标书详情
  toDetail:function(e){
    var that =this;
    var userInfo = wx.getStorageSync('userInfo');
    var bidid = e.currentTarget.dataset.bidid;
    
    if(userInfo == ''){
     wx.navigateTo({
      url: '/pages/register/auth/auth',
    })
   }else if(userInfo != '' && userInfo.phoneNumber == undefined){
    wx.navigateTo({
      url: '/pages/register/cj_login/cj_login',
    })

   }else{
       wx.request({
              url: app.globalData.apiUrl + 'bid/getGoWhere',
              method: 'post',
              header: that.data.header,
              data: {
                user_token:userInfo.user_token?userInfo.user_token:0,
                bid_id:bidid
              },
              success: function (re) {

                  if(re.data.code == 200){
                wx.navigateTo({
                  url: '/pages/bid/'+re.data.data.toWhere+'?bid_id='+bidid,
                })
                  }
                }
        })
      }

   },
   //去登录
   toLogin:function(){
    var that =this;
    var userInfo = wx.getStorageSync('userInfo');
        if(userInfo == ''){
          wx.navigateTo({
           url: '/pages/register/auth/auth',
         })
        }else if(userInfo != '' && userInfo.phoneNumber == undefined){
          wx.navigateTo({
            url: '/pages/register/cj_login/cj_login',
          })

        }
    }
})
