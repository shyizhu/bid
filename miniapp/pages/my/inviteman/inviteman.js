// pages/my/friend/friend.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    headImg: '../../../img/my/headImg.png',  //头像路径
    hospitalNum:0, //邀请医院数
    dealNum:0, //邀请医院 成交数
    hospital:[],//邀请的医院
    inviteCode: '',   //邀请码
  },
  copy:function(e){   //点击复制邀请码
    console.log(e)
    wx.setClipboardData({
      data: e.currentTarget.dataset.text,
      success: function (res) {
        wx.getClipboardData({
          success: function (res) {
            wx.showToast({
              title: '复制成功',
              icon: 'none',
              duration: 1000,
              mask: true
            })
          }
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that =this;
    var userInfo = wx.getStorageSync('userInfo');
    if(userInfo.avatarUrl){
      that.setData({
        headImg:userInfo.avatarUrl,
      })
    }
    if(userInfo.user_token){
            wx.request({
                url: app.globalData.apiUrl + 'user/getInviteInfo',
                method: 'post',
                header: that.data.header,
                data: {
                  user_token:userInfo.user_token,    
                },
                success: function (re) {
                  if(re.data.code == 200){
                      console.log(re)

                    that.setData({
                      hospitalNum:re.data.data.hospital_num,
                      dealNum:re.data.data.deal_num,
                      hospital:re.data.data.hospital,

                      inviteCode:re.data.data.invite_code,
                     
                    })

                  }
                },
              })

    }


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    var that =this;
    var userInfo = wx.getStorageSync('userInfo');
    if(userInfo){
      that.setData({
          userInfo:userInfo
      })
    }
    if(userInfo.user_token){
            wx.request({
                url: app.globalData.apiUrl + 'user/myInit',
                method: 'post',
                header: that.data.header,
                data: {
                  user_token:userInfo.user_token,    
                },
                success: function (re) {
                  if(re.data.code == 200){
                    that.setData({
                      biding_num:re.data.data.biding_num,
                      enlist_num:re.data.data.enlist_num

                    })

                  }
                },
              })

    }

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})