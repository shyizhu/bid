const app = getApp()
// pages/login/login.wxml.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    number: '133********',  //当前手机号
    is_psd: true,  //密码框状态
    img: false, //眼睛状态
    ma: "", //验证码的值
    psd: "", //新密码的值
    yzm: false,  // false 显示获取验证码  true显示倒计时
    huoqu: '获取验证码',  //倒计时结束该值变为 重新获取验证码
    num: 60,   //倒计时
    disabled: true  //重置按钮是否禁用
  },
  showPsd: function (event) {   //点击眼睛 显示/隐藏密码
    var that = this;
    var is_psd = that.data.is_psd;
    var img = that.data.img;
    that.setData({
      is_psd: !is_psd,
      img: !img
    })
  },
  psdInput: function (e) {  //绑定用户输入的密码值
    this.setData({
      psd: e.detail.value
    })
  },
  yzmInput: function (e) {  //绑定用户输入的密码值
    var ma = this.data.ma;
    this.setData({
      ma: e.detail.value
    })
    if (ma != "") {
      this.setData({
        disabled: false
      })
    }
  },
  login: function () {  //重置按钮点击
    var that = this;
    var userInfo = wx.getStorageSync('userInfo'); 
    var ma = that.data.ma;
    var psd = that.data.psd;
    var reg1 = /^\w+$/;
    if (psd == "") {
      wx.showToast({
        title: '请输入密码',
        icon: 'none',
        duration: 2000,
        mask: true
      })
    } else if (!reg1.test(psd)) {
      wx.showToast({
        title: '密码格式不正确',
        icon: 'none',
        duration: 2000,
        mask: true
      })
    } else if (ma == "") {
      wx.showToast({
        title: '请输入验证码',
        icon: 'none',
        duration: 2000,
        mask: true
      })
    }else{
            wx.request({
              url: app.globalData.apiUrl + 'login/editPass',
              method: 'post',
              header: that.data.header,
              data: {
                user_pass:psd,
                code:ma,
                user_token:userInfo.user_token?userInfo.user_token:0,
                user_id:userInfo.user_id?userInfo.user_id:0,
              },
              success: function (re) {
                  if(re.data.code == 200){

                    wx.showToast({
                      title: '修改成功',
                      icon: 'success',
                      duration: 2000,
                      success:function(){
                        wx.switchTab({
                            url: '/pages/index/index',
                        })
                      }

                    })



                  }
                }

              })

        

    }
  },
  countDown: function () {
    let that = this;
    let num = that.data.num;//获取倒计时初始值
    //如果将定时器设置在外面，那么用户就看不到countDownNum的数值动态变化，所以要把定时器存进data里面
    that.setData({
      timer: setInterval(function () {//这里把setInterval赋值给变量名为timer的变量
        //每隔一秒countDownNum就减一，实现同步
        num--;
        //然后把countDownNum存进data，好让用户知道时间在倒计着
        that.setData({
          num: num
        })
        //在倒计时还未到0时，这中间可以做其他的事情，按项目需求来
        if (num == 0) {
          //这里特别要注意，计时器是始终一直在走的，如果你的时间为0，那么就要关掉定时器！不然相当耗性能
          //因为timer是存在data里面的，所以在关掉时，也要在data里取出后再关闭
          clearInterval(that.data.timer);
          //关闭定时器之后，可作其他处理codes go here
          that.setData({
            yzm: false,
            huoqu: '重新获取验证码',
            num: 60
          })
        }
      }, 1000)
    })
  },
  getYzm: function () {  //点击获取验证码时 调用倒计时方法  并将yzm数据的值改变为true 倒计时显示
    var that = this;
    var psd = that.data.psd;
    var userInfo = wx.getStorageSync('userInfo'); 
    if (psd == ""){
      wx.showToast({
        title: '请先输入密码',
        icon: 'none',
        duration: 2000,
        mask: true
      })
    }else {
      this.countDown();
      that.setData({
        yzm: true
      })

         wx.request({
              url: app.globalData.apiUrl + 'login/sendSmsCode',
              method: 'post',
              header: that.data.header,
              data: {
                phone:userInfo.phoneNumber?userInfo.phoneNumber:0,
                user_token:userInfo.user_token?userInfo.user_token:0,

              },
              success: function (re) {
                  if(re.data.code == 200){
                  }
                }

              })

    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that =this;
    var userInfo = wx.getStorageSync('userInfo'); 
        that.setData({
          number:userInfo.phoneNumber
        }) 

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})