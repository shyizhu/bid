const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info:'',
    rePush:false,

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that =this;
      let userInfo = wx.getStorageSync('userInfo');
      let info = '';
        if(userInfo == ''){
              wx.navigateTo({
                  url: '/pages/register/auth/auth',
              })
          }else if(userInfo != '' && userInfo.phoneNumber == undefined){
                wx.navigateTo({
                    url: '/pages/register/cj_login/cj_login',
                })

          }else{
            wx.request({
                    url: app.globalData.apiUrl + 'user/getUserByToken',
                    method: 'post',
                    header: that.data.header,
                    data: {
                      user_token:userInfo.user_token,    
                    },
                    success: function (re) {
                      if(re.data.code == 200){
                          if(re.data.data.user_status ==0){
                              wx.navigateTo({
                                url: '/pages/register/status/status',
                            })

                          }else if(re.data.data.user_status ==1){
                            info = "等待审核，小麦正在紧急审核中...";

                          }else if(re.data.data.user_status ==2){
                            info = "恭喜,审核通过了!";

                          }else if(re.data.data.user_status ==3){
                            info = "您的审核被驳回了,请联系客服";
                            that.setData({
                              rePush:true
                            })

                          }
                          that.setData({
                            info:info
                          })
                      
                       }
                    },
                  })
            
          }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  toIndex:function(){
    wx.reLaunch({
       url: '/pages/index/index',
    })
  },

  //重新认证
  toRegister:function(){
    let that =this;
      let userInfo = wx.getStorageSync('userInfo');
      let info = '';
        if(userInfo == ''){
              wx.navigateTo({
                  url: '/pages/register/auth/auth',
              })
          }else if(userInfo != '' && userInfo.phoneNumber == undefined){
                wx.navigateTo({
                    url: '/pages/register/cj_login/cj_login',
                })

          }else{
            wx.request({
                    url: app.globalData.apiUrl + 'user/getUserByToken',
                    method: 'post',
                    header: that.data.header,
                    data: {
                      user_token:userInfo.user_token,    
                    },
                    success: function (re) {
                      console.log(re)
                      if(re.data.code == 200){
                          if(re.data.data.user_type ==10){
                              wx.reLaunch({
                                url: '/pages/register/cg_register/cg_register',
                              })

                          }else if(re.data.data.user_type ==11){
                              wx.reLaunch({
                                  url: '/pages/register/gys_register/gys_register',
                              })
                          }
                      
                       }
                    },
                  })
            
          }


  }
})