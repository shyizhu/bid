// pages/bid/detail/detail.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    headBg: false,  //左上角 “< 投标” 使用数据  当页面向下滚动  该值变为 true
    headImg: '../../../img/iobd/detail_head5.jpg',



    bid_data:{},
  
    guize: '本次报价共3轮，每轮允许报价1次，每轮报价10分钟',
    bz_msg: '招标项目需求简介，需要大量采购，希望服务者给的价格低一点，服务人员 耐心 专业水平高 办事利索 请尽快联系我',  //备注信息数据
    goods_detail:{
      goods_name:'',
      goods_brand_model:'',//品牌型号
      goods_unit:'',
      goods_price:'',
      goods_num:'',
      goods_remark:''
    },
    isProMaskShow: false // 是否显示弹框

  },
  onPageScroll: function (e) {  //判断 滚动条位置  给头部添加背景
    if (e.scrollTop >= 100) {
      this.setData({
        headBg: true
      })
    } else {
      this.setData({
        headBg: false
      })
    }
    // console.log(e.scrollTop)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that =this;
    var aurl = that.route
    var userInfo = wx.getStorageSync('userInfo');
    var bidid = options.bid_id;
    
       wx.request({
              url: app.globalData.apiUrl + 'bid/getBidDetail',
              method: 'post',
              header: that.data.header,
              data: {
                user_token:userInfo.user_token?userInfo.user_token:0,
                bid_id:bidid
              },
              success: function (re) {
                  if(re.data.code == 200){
                    let toWhere = 'pages/bid/'+re.data.data.toWhere;

                    if(toWhere != that.route){//如果不是当前页面跳转
                        wx.navigateTo({
                          url: '/'+toWhere+'?bid_id='+bidid,
                        })
                    }else{
                      
                        that.setData({
                          bid_data:re.data.data,
                        
                        })
                        

                    }    


                  }
                }

        })

  let pages = getCurrentPages();
      console.log(pages);

    console.log(pages[0].route);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  showPro:function(e){ //点击产品信息显示弹窗
    let that = this;
    let goodsidx = e.currentTarget.dataset.goodsidx
    let bid_data = that.data.bid_data;
    that.setData({
        isProMaskShow: true,
        goods_detail:bid_data.goods[goodsidx]
    })
  },
  closeMask:function(){ //点击关闭按钮隐藏弹窗
    var that = this;


    that.setData({
    isProMaskShow: false,
    goods_detail:{}
    })
  },
  toIndex:function(){
    var that =this;
   let pages = getCurrentPages();
    if(pages[0].route !=  that.route){
        wx.navigateBack({
          delta: 1
        })
    }else{
       wx.switchTab({
       url: '/pages/index/index'
      })
    }
  }
})