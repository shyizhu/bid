// pages/iobd/iobd.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    is_show:false,
    show_login:false,
    show_auto:false,//去认证
    show_success:false,
    array: ['请选择', '医用耗材', '医疗设备', '外科器械', '康复护理', '宠物用品', '医院其他'],
    lei_index: 0,
    region: ['', '', '请选择'],
    jiaohuo_index:0,
    jiaohuo_array:['请选择','7天', '15天','30天'],
    fukuan_index: 0,
    fukuan_array: ['请选择','首付30%', '货到七日内付款'],
    addPro:[{
        goods_name:'',
        goods_brand_model:'',
        goods_unit:'',
        goods_num:'',
        goods_price:'',
        goods_remark:''

    }],
    bid_name:'',//
    bid_type:'',//项目分类
    bid_post_addr:'',//收货地址 
    bid_contact_name:'', //联系人
    bid_contact_phone:'', //联系电话 
    bid_arrival_time:'',//交货时间 
    bid_budget:'',//预算 
    bid_pay_type:'', //付款方式
    bid_remark:'',//项目备注

    province_id: '',
    city_id: '',
    area_id: '',
    res_bid_id:0

  },
  //项目分类改变
  bindPickerChange: function (e) {
    this.setData({
      lei_index: e.detail.value,
      bid_type:e.detail.value*10
    })
  },
  bindRegionChange: function (e) {  //选择城市
    // console.log('picker发送选择改变，携带值为', e.detail.code)
    this.setData({
      region: e.detail.value,
      province_id: e.detail.code[0],
      city_id: e.detail.code[1],
      area_id: e.detail.code[2],
    })
  },

  bindJiaoChange: function (e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      jiaohuo_index: e.detail.value,
      bid_arrival_time:e.detail.value
    })
  },
  //付款方式
  bindPayTypeChange:function(e){
      console.log('picker发送选择改变，携带值为', e.detail.value)

    this.setData({
      fukuan_index: e.detail.value,
      bid_pay_type:e.detail.value
    })

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that =this;
    var userInfo = wx.getStorageSync('userInfo'); 
        //判断是否登录
        if(userInfo == ''){
                that.setData({
                  show_login:true
                })
        }else if(userInfo != '' && userInfo.phoneNumber == undefined){
                that.setData({
                  show_login:true
                })
         }else{
              wx.request({
              url: app.globalData.apiUrl + 'bid/pushBidInit',
              method: 'post',
              header: that.data.header,
              data: {
                user_id:userInfo.user_id?userInfo.user_id:0,
                user_token:userInfo.user_token?userInfo.user_token:0,

              },
              success: function (re) {
                    //用户未认证
                    if(re.data.code == 211){
                      wx.navigateTo({
                        url: '/pages/register/cg_register/cg_register',
                        success:function(){
                          that.setData({
                            show_auto:true
                          })
                        }
                      })
                    //如果是医院 ，且审核通过
                    }else if(re.data.code == 200){
                      let userInfo =re.data.data;
                      that.setData({
                        is_show:true,
                        bid_post_addr:userInfo.post_addr,
                        province_id: userInfo.province_id,
                        city_id: userInfo.city_id,
                        area_id: userInfo.area_id,
                        region:userInfo.region,

                        bid_contact_name:userInfo.agent,
                        bid_contact_phone:userInfo.user_phone
                     })
                    }else{
                      wx.showModal({
                        title: '提示',
                        content: re.data.msg,
                        showCancel:false,
                        confirmText:'知道了',
                        success (res) {
                            wx.reLaunch({
                              url: '/pages/index/index',
                            })
                        }
                      })

                    }
                }

              })

          }


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //添加商品
  addNewPro: function() {
    let newArray = {
        goods_name:'',
        goods_brand_model:'',
        goods_unit:'',
        goods_num:0,
        goods_price:0,
        goods_remark:''

    }
    this.setData({
      addPro: this.data.addPro.concat(newArray)
    })
  },
  //删除
  delPro: function(e) {
    let that = this
    let index = e.currentTarget.dataset.index //数组下标
    let arrayLength = that.data.addPro.length //数组长度
    let newArray = []
    if (arrayLength > 1) {
      //数组长度>1 才能删除
      for (let i = 0; i < arrayLength; i++) {
        if (i !== index) {
          newArray.push(that.data.addPro[i])
        }
      }
      that.setData({
        addPro: newArray
      })
    } else {
      wx.showToast({
        icon: 'none',
        title: '必须设置一个产品',
      })
    }
  },
   //产品 获取输入框信息
  setInputValue: function(e) {
    var that =this
    let index = e.currentTarget.dataset.index //数组下标
    let tag = e.target.dataset.tag  //字段名称
    let array = that.data.addPro;
    let total = 0;
   

     array[index][tag] = e.detail.value  //赋值
    this.setData({
      addPro: array
    })
    //计算预算
    for (var i = 0; i < array.length; i++) {
        total =  total+Math.floor(parseFloat(array[i]['goods_price']*100 * array[i]['goods_num']))/100;
        // total = (total+array[i]['goods_price'] * array[i]['goods_num'])*100/100
    }
    this.setData({
      bid_budget: total
    })

  },
  getVal: function (e) {
    let that = this;
    let tag = e.target.dataset.tag  //字段名称
    let val = e.detail.value

    this.setData({
      [tag]: val
    })
  },
  formSubmit:function(){
    var that = this;
    var formData ={}
    var userInfo = wx.getStorageSync('userInfo');
    var arr = that.data.addPro;

    for (var i = 0; i < arr.length; i++) {
              if (new RegExp('^[\\S]{2,20}$').test(arr[i].goods_name) != true) {
                  wx.showToast({
                    title: `产品名称不能为空或格式不正确`,
                    icon: 'none',
                    duration: 2000,
                    mask: true
                  })
                  return false;
                }
                if (new RegExp('^[\\S]{2,20}$').test(arr[i].goods_brand_model) != true) {
                  wx.showToast({
                    title: `品牌/型号不能为空或格式不正确`,
                    icon: 'none',
                    duration: 2000,
                    mask: true
                  })
                  return false;
                }
                if (new RegExp('^[\u4E00-\u9FA5A-Za-z]{1,10}$').test(arr[i].goods_unit) != true) {
                  wx.showToast({
                    title: `单位不能为空或格式不正确`,
                    icon: 'none',
                    duration: 2000,
                    mask: true
                  })
                  return false;
                }
                if (new RegExp('^[0-9]{1,10}$').test(arr[i].goods_num) != true) {
                  wx.showToast({
                    title: `数量不能为空或格式不正确`,
                    icon: 'none',
                    duration: 2000,
                    mask: true
                  })
                  return false;
                }
                if (new RegExp('(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)').test(arr[i].goods_price) != true) {
                  wx.showToast({
                    title: `单价不能为空或格式不正确`,
                    icon: 'none',
                    duration: 2000,
                    mask: true
                  })
                  return false;
                }
                if (new RegExp('^[\\S]{2,20}$').test(arr[i].goods_remark) != true) {
                  wx.showToast({
                    title: `产品说明不能为空或格式不正确`,
                    icon: 'none',
                    duration: 2000,
                    mask: true
                  })
                  return false;
                }
  
    }
   if(that.data.bid_type == "") {
            wx.showToast({
            title: `请选择项目分类`,
            icon: 'none',
            duration: 2000,
            mask: true
           })
           return false;
   }
   
   if(that.data.province_id == "" || that.data.city_id == "" || that.data.area_id == "") {
            wx.showToast({
            title: `请选择收货地址`,
            icon: 'none',
            duration: 2000,
            mask: true
           })
           return false;
   }
  if(new RegExp('^[\\S]{2,40}$').test(that.data.bid_post_addr) != true) {
            wx.showToast({
            title: `详细地址不能为空或格式不正确`,
            icon: 'none',
            duration: 2000,
            mask: true
           })
           return false;
   }if (new RegExp('^[\u4E00-\u9FA5A-Za-z0-9]{1,20}$').test(that.data.bid_contact_name) != true) {
            wx.showToast({
            title: `联系人不能为空或格式不正确`,
            icon: 'none',
            duration: 2000,
            mask: true
           })
           return false;
   }
   if(new RegExp('^[0-9\-]{8,20}$').test(that.data.bid_contact_phone) != true) {
            wx.showToast({
            title: `联系电话不能为空或格式不正确`,
            icon: 'none',
            duration: 2000,
            mask: true
           })
           return false;
   }
 if (that.data.bid_arrival_time == "" || that.data.bid_arrival_time == 0) {
            wx.showToast({
            title: `请选择交货时间`,
            icon: 'none',
            duration: 2000,
            mask: true
           })
           return false;
   }
    if (RegExp('(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)').test(that.data.bid_budget) != true) {
            wx.showToast({
            title: `预算金额不能为空或格式不正确`,
            icon: 'none',
            duration: 2000,
            mask: true
           })
           return false;
   }if (that.data.bid_pay_type == "" || that.data.bid_pay_type == 0) {
            wx.showToast({
            title: `请选择付款方式`,
            icon: 'none',
            duration: 2000,
            mask: true
           })
           return false;
   }
   if(that.data.bid_remark == "") {
            wx.showToast({
            title: `请填写项目备注`,
            icon: 'none',
            duration: 2000,
            mask: true
           })
           return false;
   }


    formData['addPro'] = that.data.addPro; //产品
    formData['bid_type'] = that.data.bid_type;//项目分类
    formData['bid_post_addr'] = that.data.bid_post_addr;//收货地址
    formData['bid_contact_name'] = that.data.bid_contact_name;//联系人
    formData['bid_contact_phone'] = that.data.bid_contact_phone;//联系电话
    formData['bid_arrival_time'] = that.data.bid_arrival_time;//交货时间
    formData['bid_budget'] = that.data.bid_budget;//预算
    formData['bid_pay_type'] = that.data.bid_pay_type;//付款方式
    formData['bid_remark'] = that.data.bid_remark;//项目备注
    formData['province_id'] = that.data.province_id;
    formData['city_id'] = that.data.city_id;
    formData['area_id'] = that.data.area_id;

    formData['user_token'] = userInfo.user_token;
    formData['user_id'] = userInfo.user_id;


       wx.showModal({
          title: '提示',
          content: '您发布竞价具有法律效益，请认真对待，确认发布竞价？',
          success (res) {
            if (res.confirm) {
                  wx.request({
                    url: app.globalData.apiUrl + 'bid/pushBid',
                    method: 'post',
                    header: that.data.header,
                    data: {
                      formData:formData,
                    },
                    success: function (re) {
                      if(re.data.code == 200){
                        that.setData({
                            is_show:false,
                            show_success:true,
                            res_bid_id:re.data.data.bid_id
                        })
                      }
                    },
                  })           
             } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })


     
  },
  //去登录
   toLogin:function(){
    var that =this;
    var userInfo = wx.getStorageSync('userInfo');
        if(userInfo == ''){
          wx.navigateTo({
           url: '/pages/register/auth/auth',
         })
        }else if(userInfo != '' && userInfo.phoneNumber == undefined){
          wx.navigateTo({
            url: '/pages/register/cj_login/cj_login',
          })

        }
    },
    //去认证
   toAuto:function(){
          wx.navigateTo({
            url: '/pages/register/cg_register/cg_register',
          })

    },
    //去商城
  toShop:function(){
    wx.reLaunch({
       url: '/pages/em/em',
    })

  },
  toBidShow:function(){
    let that = this;
    wx.reLaunch({
       url: '/pages/bid/showdetail/showdetail?bid_id='+that.data.res_bid_id,
    })
  }




})