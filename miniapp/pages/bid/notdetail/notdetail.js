// pages/bid/detail/detail.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    headBg: false,  //左上角 “< 投标” 使用数据  当页面向下滚动  该值变为 true
    headImg: '../../../img/iobd/detail_head6.jpg',
    bid_data:{},
      goods_detail:{
      goods_name:'',
      goods_brand_model:'',//品牌型号
      goods_unit:'',
      goods_price:'',
      goods_num:'',
      goods_remark:''
    },
    isProMaskShow: false, // 是否显示弹框
    downTime:''

  },
  onPageScroll: function (e) {  //判断 滚动条位置  给头部添加背景
    if (e.scrollTop >= 100) {
      this.setData({
        headBg: true
      })
    } else {
      this.setData({
        headBg: false
      })
    }
    // console.log(e.scrollTop)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
     var that =this;
    var aurl = that.route
    var userInfo = wx.getStorageSync('userInfo');
    var bidid = options.bid_id;
    if(userInfo == ''){
     wx.navigateTo({
      url: '/pages/register/auth/auth',
    })
   }else if(userInfo != '' && userInfo.phoneNumber == undefined){
    wx.navigateTo({
      url: '/pages/register/cj_login/cj_login',
    })

   }else{
       wx.request({
              url: app.globalData.apiUrl + 'bid/getBidDetail',
              method: 'post',
              header: that.data.header,
              data: {
                user_token:userInfo.user_token?userInfo.user_token:0,
                bid_id:bidid
              },
              success: function (re) {
                  if(re.data.code == 200){
                    let toWhere = 'pages/bid/'+re.data.data.toWhere;

                    if(toWhere != that.route){//如果不是当前页面跳转
                        wx.reLaunch({
                          url: '/'+toWhere+'?bid_id='+bidid,
                        })
                    }else{
                      
                        that.setData({
                          bid_data:re.data.data,
                        
                        })
                        
                        that.setCountDown();

                    }    


                  }
                }

        })

   }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    clearInterval(this.data.downTime);

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //倒计时
  setCountDown: function(){
    let that = this;
    let time = 1000;
    let bid_data = that.data.bid_data
    that.setData({
         downTime: setInterval(function(){
          if (bid_data.time1 < 0) {
          bid_data.time1 = 0;
          wx.reLaunch({
              url: '/pages/bid/enddetail/enddetail?bid_id='+bid_data.bid_id,
          })
          }else{
                let formatTime = that.getFormat(bid_data.time1);
                bid_data.time1 -= time;
              that.setData({
                bid_data: bid_data,
                h : formatTime.hh,
                m :formatTime.mm,
                s :formatTime.ss
              });
          }

       }, time),

    })
      

  },
    //时间格式化
    getFormat: function (msec){
      let ss = parseInt(msec / 1000);
      let ms = parseInt(msec % 1000);
      let mm = 0;
      let hh = 0;
      if (ss > 60) {
        mm = parseInt(ss / 60);
        ss = parseInt(ss % 60);
        if (mm > 60) {
          hh = parseInt(mm / 60);
          mm = parseInt(mm % 60);
        }
      }
      ss = ss > 9 ? ss : `0${ss}`;
      mm = mm > 9 ? mm : `0${mm}`;
      hh = hh > 9 ? hh : `0${hh}`;
      return { ms, ss, mm, hh };
    },
  showPro:function(e){ //点击产品信息显示弹窗
    let that = this;
    let goodsidx = e.currentTarget.dataset.goodsidx
    let bid_data = that.data.bid_data;
    that.setData({
        isProMaskShow: true,
        goods_detail:bid_data.goods[goodsidx]
    })
  },
  closeMask:function(){ //点击关闭按钮隐藏弹窗
    var that = this;


    that.setData({
    isProMaskShow: false,
    goods_detail:{}
    })
  },
  toIndex:function(){
    wx.switchTab({
       url: '/pages/index/index'
    })
  }
})