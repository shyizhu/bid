const app = getApp()
const util = require('/../../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    headBg: false,  //左上角 “< 投标” 使用数据  当页面向下滚动  该值变为 true
    headImg:'../../../img/iobd/detail_head3.jpg',

    bid_data:{},
    bjVal:0.00,
    formatMoney:0.00,
    h: '00', //时
    m: '00', //分
    s: '00', //秒
    guize: '本次报价共3轮，每轮允许报价1次，每轮报价10分钟',
    bz_msg: '招标项目需求简介，需要大量采购，希望服务者给的价格低一点，服务人员 耐心 专业水平高 办事利索 请尽快联系我',  //备注信息数据
    goods_detail:{
      goods_name:'',
      goods_brand_model:'',//品牌型号
      goods_unit:'',
      goods_price:'',
      goods_num:'',
      goods_remark:''
    },
    isProMaskShow: false, // 是否显示弹框
    downTime:''

  },
  onPageScroll: function (e) {  //判断 滚动条位置  给头部添加背景
    if(e.scrollTop >= 100){
      this.setData({
        headBg: true
      })
    }else {
      this.setData({
        headBg: false
      })
    }
    // console.log(e.scrollTop)
  },
  getVal:function(e){ //绑定报价输入框的值
    let val=e.detail.value;
    let formatMoney = util.toMoney(val);
    this.setData({
      bjVal: val,
      formatMoney:formatMoney
    })
  },
  pushPrice:function(){  //判断报价是否为空  如果为空 弹出提示  否则 弹出模态框
    var that =this;
    var val = this.data.bjVal;
    let formatMoney = that.data.formatMoney;//格式化金额
    let bid_data = that.data.bid_data
    var userInfo = wx.getStorageSync('userInfo');
    if(val == ""){
      wx.showToast({
        title: '请填写您的报价',
        icon: 'none',
        duration: 2000,
        mask: true
      })
    }else if(parseFloat(val) > parseFloat(that.data.bid_data.two_mixpush)){
        wx.showToast({
          title: '您的报价至少要小于等于：'+that.data.bid_data.two_mixpush,
          icon: 'none',
          duration: 2000,
          mask: true
        })
    }else{
      wx.showModal({
        title: '友情提示',
        content: '您的报价金额为：'+formatMoney+'元,确认报价？',
        success(res) {
          if (res.confirm) {
              wx.request({
                url: app.globalData.apiUrl + 'bid/pushPrice',
                method: 'post',
                header: that.data.header,
                data: {
                  user_token:userInfo.user_token?userInfo.user_token:0,
                  bid_id:that.data.bid_data.bid_id,
                  price:val,
                  rotation:2
                },
                success: function (re) {
                    if(re.data.code == 200){
                        wx.showToast({
                        title: '报价成功',
                        icon: 'none',
                        duration: 2000,
                        mask: true
                      })
                        bid_data.is_pushPrice = 1;
                        that.setData({
                          bid_data:bid_data,
                        })
                    }else{
                      wx.showToast({
                        title: re.data.msg,
                        icon: 'none',
                        duration: 2000,
                        mask: true
                      })

                    }
                  }

                })

            //console.log('用户点击确定')
          } 
        }
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
     var that =this;
    var aurl = that.route
    var userInfo = wx.getStorageSync('userInfo');
    var bidid = options.bid_id;
    
    if(userInfo == ''){
     wx.navigateTo({
      url: '/pages/register/auth/auth',
    })
   }else if(userInfo != '' && userInfo.phoneNumber == undefined){
    wx.navigateTo({
      url: '/pages/register/cj_login/cj_login',
    })

   }else{
       wx.request({
              url: app.globalData.apiUrl + 'bid/getBidDetail',
              method: 'post',
              header: that.data.header,
              data: {
                user_token:userInfo.user_token?userInfo.user_token:0,
                bid_id:bidid
              },
              success: function (re) {

                  if(re.data.code == 200){
                    let toWhere = 'pages/bid/'+re.data.data.toWhere;

                    if(toWhere != that.route){//如果不是当前页面跳转
                        wx.reLaunch({
                          url: '/'+toWhere+'?bid_id='+bidid,
                        })
                    }else{
                        let bid_data = re.data.data;
                        if(bid_data.is_pushPrice ==1){
                          that.setData({
                            formatMoney:util.toMoney(bid_data.current_pushPrice),
                          })
                        }
                        that.setData({
                          bid_data:re.data.data,
                        })
                        
                          that.setCountDown();

                    }    


                  }
                }

        })

   }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    clearInterval(this.data.downTime);

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
   onPullDownRefresh: function () {
    wx.showNavigationBarLoading() //在标题栏中显示加载
    setTimeout(function () {

      wx.hideNavigationBarLoading() //完成停止加载

      wx.stopPullDownRefresh() //停止下拉刷新

    }, 1500);

   },

  /**
   * 页面上拉触底事件的处理函数
   */
   onReachBottom: function () {
     wx.showLoading({
      title: '加载中',
    })
    let that = this;
    var userInfo = wx.getStorageSync('userInfo');
    var bidid = that.data.bid_data.bid_id;
       wx.request({
              url: app.globalData.apiUrl + 'bid/getBidDetail',
              method: 'post',
              header: that.data.header,
              data: {
                user_token:userInfo.user_token?userInfo.user_token:0,
                bid_id:bidid
              },
              success: function (re) {
                  if(re.data.code == 200){
                      
                        that.setData({
                          bid_data:re.data.data,
                          
                        })

                  }
                }
        })

    setTimeout(function(){
      wx.hideLoading();
    },1500)

   },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  showPro:function(e){ //点击产品信息显示弹窗
    let that = this;
    let goodsidx = e.currentTarget.dataset.goodsidx
    let bid_data = that.data.bid_data;
    that.setData({
        isProMaskShow: true,
        goods_detail:bid_data.goods[goodsidx]
    })
  },
  closeMask:function(){ //点击关闭按钮隐藏弹窗
    var that = this;


    that.setData({
    isProMaskShow: false,
    goods_detail:{}
    })
  },
  toIndex:function(){
    var that =this;
   let pages = getCurrentPages();
    if(pages[0].route !=  that.route){
        wx.navigateBack({
          delta: 1
        })
    }else{
       wx.switchTab({
       url: '/pages/index/index'
      })
    }
  },
   //倒计时
   setCountDown: function(){
    let that = this;
    let time = 1000;
    let bid_data = that.data.bid_data
    that.setData({
         downTime: setInterval(function(){
          if (bid_data.time1 < 0) {
          bid_data.time1 = 0;
          wx.reLaunch({
            url: '/pages/bid/threedetail/threedetail?bid_id='+bid_data.bid_id,
          })
          }else{
                let formatTime = that.getFormat(bid_data.time1);
                bid_data.time1 -= time;
              that.setData({
                h : formatTime.hh,
                m :formatTime.mm,
                s :formatTime.ss
              });
          }

       }, time),

    })
  
      
  },
    //时间格式化
    getFormat: function (msec){
      let ss = parseInt(msec / 1000);
      let ms = parseInt(msec % 1000);
      let mm = 0;
      let hh = 0;
      if (ss > 60) {
        mm = parseInt(ss / 60);
        ss = parseInt(ss % 60);
        if (mm > 60) {
          hh = parseInt(mm / 60);
          mm = parseInt(mm % 60);
        }
      }
      ss = ss > 9 ? ss : `0${ss}`;
      mm = mm > 9 ? mm : `0${mm}`;
      hh = hh > 9 ? hh : `0${hh}`;
      return { ms, ss, mm, hh };
    }
})