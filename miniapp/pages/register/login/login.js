const app = getApp()
// pages/login/login.wxml.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    is_psd: true,
    img: false,
    tit: "",
    psd: "",
    userInfo:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '账号密码登录'
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //微信快捷登录
  //获取微信绑定的手机号
  getPhoneNumber: function(e) {
    var that = this
    var encryptedData = e.detail.encryptedData
    var iv = e.detail.iv
    var userInfo = wx.getStorageSync('userInfo')

    if (e.detail.errMsg == 'getPhoneNumber:fail user deny') {
      wx.showToast({
            title: '需要通过授权才能继续，请重新点击并授权',
            icon: 'none',
            duration: 2000
          })
    } else {
      wx.login({
        success: function(res) {
          if (res.code) {
            wx.request({
              url: app.globalData.apiUrl + 'login/getUserPhone',
              method: 'post',
              header: that.data.header,
              data: {
                encryptedData: encryptedData,
                iv: iv,
                code: res.code,
              },
              success: function(data) {
                // console.log(data)
                if(data.data.code == 200){

                      //更新缓存
                      userInfo.phoneNumber = data.data.data.phoneNumber
                      userInfo.user_id = data.data.data.user_id
                      userInfo.user_token = data.data.data.user_token
                      userInfo.user_type = data.data.data.user_type
                      wx.setStorageSync('userInfo',userInfo);
                      that.setData({
                        userInfo: userInfo
                      })
                     //如果是未认证用户，引导注册，否则调回两页
                      wx.reLaunch({
                        url: '/pages/my/home/home',
                      })
                }
              },
           })
          }
        }
      })
    }
  },
  showPsd:function (event) {
    var that = this;
    var is_psd = that.data.is_psd;
    var img = that.data.img;
    that.setData({
      is_psd:!is_psd,
      img:!img
    })
  },
  titInput:function(e){
    this.setData({
      tit: e.detail.value
    })
  },
  psdInput: function (e) {
    this.setData({
      psd: e.detail.value
    })
  },
  formSubmit:function(){
    var that = this;
    var userInfo = wx.getStorageSync('userInfo')
    var tit = that.data.tit;
    var psd = that.data.psd;
    var reg = /^[A-Za-z0-9]+$/;
    var reg1 = /^\w+$/;
    if(tit == ""){
      wx.showToast({
        title: '请输入用户名',
        icon: 'none',
        duration: 2000,
        mask: true
      })
    }else if(psd == ""){
      wx.showToast({
        title: '请输入密码',
        icon: 'none',
        duration: 2000,
        mask: true
      })
    } else if (!reg.test(tit)){
      wx.showToast({
        title: '账号格式不正确',
        icon: 'none',
        duration: 2000,
        mask: true
      })
    }else if (!reg1.test(psd)){
      wx.showToast({
        title: '密码格式不正确',
        icon: 'none',
        duration: 2000,
        mask: true
      })
    }else{
      wx.request({
              url: app.globalData.apiUrl + 'login/userLogin',
              method: 'post',
              header: that.data.header,
              data: {
                user_name: tit,
                user_pass: psd,
              },
              success: function(data) {
                console.log(data)
                if(data.data.code == 200){
                  //更新缓存
                  userInfo.phoneNumber = data.data.data.phoneNumber
                  userInfo.user_id = data.data.data.user_id
                  userInfo.user_token = data.data.data.user_token
                  userInfo.user_type = data.data.data.user_type

                  wx.setStorageSync('userInfo',userInfo);
                  that.setData({
                    userInfo: userInfo
                  })

              //如果是未认证用户，引导注册，否则调回两页
                    if(userInfo.user_type == 0){
                      wx.navigateTo({
                        url: '../status/status',
                      })
                    }else{
                     wx.reLaunch({
                        url: '/pages/my/home/home',
                     }) 
                    }

                }else{
                      wx.showToast({
                        title: '账号或密码不正确',
                        icon: 'none',
                        duration: 2000,
                        mask: true
                      })
                }
                
               
               
              },
           })

    }

  }
})