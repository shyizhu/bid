const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    is_show:true,
    show_success:false,
    title: '请提交注册商信息',
    msg: [{  //  用户名至负责人循环数据
      label: '登录名',
      id: 'user_name',
      type: 'text',
      placehold: '大小写字母数字5-15位',
      bindinput: 'getVal',
      val: '',
      reg: '^[A-Za-z0-9]{5,15}$'
    }, {
      label: '密码',
      id: 'user_pass',
      type: 'password',
      placehold: '6~18 大小写字母数字',
      bindinput: 'getVal',
      val: '',
      reg: '^[a-zA-Z0-9]{6,18}$'
    }, {
      label: '确认密码',
      id: 'aginPwd',
      type: 'password',
      placehold: '两次密码需一致',
      bindinput: 'getVal',
      val: '',
      reg: '^[a-zA-Z0-9]{6,18}$'
    }, {
      label: '医院名称',
      id: 'company_name',
      type: 'text',
      placehold: '请填写',
      bindinput: 'getVal',
      val: '',
      reg: '^[\u4E00-\u9FA5A-Za-z0-9_]{2,40}$'
    },{
      label: '负责人',
      id: 'agent',
      type: 'text',
      placehold: '请填写',
      bindinput: 'getVal',
      val: '',
      reg: '^[\u4E00-\u9FA5A-Za-z0-9]{2,40}$'
    }],


    msg2: {
      label: '通讯地址',
      id: 'post_addr',
      type: 'text',
      placehold: '请填写',
      bindinput: 'getVal2',
      val: '',
      reg: '^.{2,40}$'
    },
    msg3: {
      label: '税务登记号',
      id: 'tax_register_num',
      type: 'text',
      placehold: '请填写',
      bindinput: 'getVal3',
      val: '',
      reg: '^[A-Za-z0-9]{2,40}$'
    },
    

    city: '所在省市',  //选择城市数据
    region: ['', '', '请选择'],
    btnType: 'primary',  //按钮样式
    province_id: '',
    city_id: '',
    area_id: '',
    //医院类型
    hospital_type1: 0,
    hospital_type2: 0,
    //邀请码
    invite_code:'',

    file: '企业扫描件',
    checked: true, //复选框是否选中a
    userInfo:{

    },

  multiArray: [["请选择","诊所","门诊部","专科医院","综合医院","卫生院","社会服务中心站","医疗防疫血站","临床检验中心","民族医院","村卫生室","互联网医院","其他医疗机构"], ["请选择","中医诊所","中西医结合诊所","民族医院诊所","口腔诊所","美容整形外科诊所","医疗美容诊所","精神卫生诊所","中小学卫生保健所","综合门诊部","中医门诊部","中西医结合门诊部","民族医门诊部","专科门诊部","口腔门诊部","整形外科门诊部","医疗美容门诊部","心血管医院","血液病医院","疗养院","妇幼保健院","康复医院","精神病医院","皮肤病医院","美容整容医院","肿瘤医院","口腔医院","眼科医院","部级院","部队医院","省级医院","市级医院","县级以下医院","县级医院","一般卫生院","二级卫生院"]],
  multiIndex: [0, 0],
  },
  bindRegionChange: function (e) {  //选择城市
    // console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      region: e.detail.value,
      province_id: e.detail.code[0],
      city_id: e.detail.code[1],
      area_id: e.detail.code[2],
    })
  },
  getVal: function (e) {
    var arr = this.data.msg;
    var obj = arr[e.currentTarget.dataset.index]
    obj.val = e.detail.value
    this.setData({
      msg: arr
    })
  },
  getVal2: function (e) {
    // console.log(e)
    var arr = this.data.msg2;
    arr.val = e.detail.value
    this.setData({
      msg2: arr
    })
  },
  getVal3: function (e) {
    // console.log(e)
    var arr = this.data.msg3;
    arr.val = e.detail.value
    this.setData({
      msg3: arr
    })
  },
  getVal4: function (e) {
    // console.log(e)
    var invite = e.detail.value
    this.setData({
      invite_code: invite
    })
  },
  submit: function () {
    var that = this;
    var formData ={}
    var yhtmpImg = wx.getStorageSync('yhtmpImg')?wx.getStorageSync('yhtmpImg'):{};
    var userInfo = wx.getStorageSync('userInfo');
    var arr = that.data.msg;
    for (var i = 0; i < arr.length; i++) {
            formData[arr[i].id] =arr[i].val;

      if (arr[i].val === "") {
        wx.showToast({
          title: `请输入${arr[i].label}`,
          icon: 'none',
          duration: 2000,
          mask: true
        })
        return false;
      } else if (new RegExp(arr[i].reg).test(arr[i].val) != true) {
        wx.showToast({
          title: `${arr[i].label}格式不正确`,
          icon: 'none',
          duration: 2000,
          mask: true
        })
        return false;
      }
    }
    if(that.data.msg2.val == ""){
      wx.showToast({
        title: `请输入通讯地址`,
        icon: 'none',
        duration: 2000,
        mask: true
      })
      return false;
    } else if (new RegExp(that.data.msg2.reg).test(that.data.msg2.val) != true){
      wx.showToast({
        title: `通讯地址格式不正确`,
        icon: 'none',
        duration: 2000,
        mask: true
      })
      return false;
    }else if (that.data.msg3.val == "") {
      wx.showToast({
        title: `请输入税务登记号`,
        icon: 'none',
        duration: 2000,
        mask: true
      })
      return false;
    } else if (new RegExp(that.data.msg3.reg).test(that.data.msg3.val) != true) {
      wx.showToast({
        title: `税务登记号格式不正确`,
        icon: 'none',
        duration: 2000,
        mask: true
      })
      return false;
    }else if (arr[1].val != arr[2].val) {
      wx.showToast({
        title: '两次密码不一致',
        icon: 'none',
        duration: 2000,
        mask: true
      })
      return false;
    } 
    if(that.data.province_id == "" || that.data.city_id == "" ||that.data.area_id == ""){
      wx.showToast({
        title: '请选择省市',
        icon: 'none',
        duration: 2000,
        mask: true
      })
      return false;
     }
     if(that.data.hospital_type1 == 0){
              wx.showToast({
              title: '请选择确认医院类型',
              icon: 'none',
              duration: 2000,
              mask: true
            })
            return false;
        }

    if(!that.data.checked){
       wx.showToast({
        title: '需同意注册供应商协议',
        icon: 'none',
        duration: 2000,
        mask: true
      })
      return false;
    }

    if(yhtmpImg.yingyeImg == undefined || yhtmpImg.zhiyeImg == undefined ){
         wx.showToast({
          title: `请上传所需企业扫描件`,
          icon: 'none',
          duration: 2000,
          mask: true
        })
         return false;
    } 

     formData['post_addr'] = that.data.msg2.val //通信地址
     formData['tax_register_num'] = that.data.msg3.val // 税务登记号

    formData['hospital_type1'] = that.data.hospital_type1;
    formData['hospital_type2'] = that.data.hospital_type2;

    formData['province_id'] = that.data.province_id;
    formData['city_id'] = that.data.city_id;
    formData['area_id'] = that.data.area_id;
    formData['yingyeImg'] = yhtmpImg.yingyeImg;
    formData['zhiyeImg'] = yhtmpImg.zhiyeImg;
    formData['user_token'] = userInfo.user_token;
    formData['user_id'] = userInfo.user_id;
    //邀请码
    formData['invite_code'] = that.data.invite_code;
console.log(formData)
        wx.showModal({
          title: '提示',
          content: '可发布竞价信息，确认认证为采购方？',
          success (res) {
            if (res.confirm) {
              wx.request({
                url: app.globalData.apiUrl + 'login/buyerRegister',
                method: 'post',
                header: that.data.header,
                data: {
                  formData:formData,
                 
                  
                },
                success: function (re) {
                  if(re.data.code == 200){
                      //缓存加入openid
                      console.log(userInfo)
                      userInfo.user_type = 10;//供应商
                      wx.setStorageSync('userInfo',userInfo);
                      that.setData({
                        is_show:false,
                        show_success:true,
                      })

                  }else{
                      wx.showToast({
                        title: re.data.msg,
                        icon: 'none',
                        duration: 2000,
                        mask: true
                      })
                  }
                },
              })     
             } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
     

              


  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '医院认证'
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //跳转到上传那图片
  goUpfile:function(){
    wx.navigateTo({
       url: '../cg_saomiao/cg_saomiao',
    })
  },
  checkboxChange: function(e) {
        var that =this;
        that.setData({
          checked:!that.data.checked
        })
  },
  toAgree:function(){
    wx.navigateTo({
       url: '../cg_agree/cg_agree',
    })
  },
  //只发生改变时触发
  bindMultiPickerChange: function (e) {
    console.log(e.detail.value)
    this.setData({
      multiIndex: e.detail.value,
      hospital_type1:e.detail.value[0],
      hospital_type2:e.detail.value[1]

    })
  },
  //一级目录修改，触发
  bindMultiPickerColumnChange: function (e) {
    console.log('修改的列为', e.detail.column, '，值为', e.detail.value);
    var data = {
      multiArray: this.data.multiArray,
      multiIndex: this.data.multiIndex
    };
    data.multiIndex[e.detail.column] = e.detail.value;
    switch (e.detail.column) {
      case 0:
        switch (data.multiIndex[0]) {
          case 0:
            data.multiArray[1] = ["请选择"];
            break;
          case 1:
            data.multiArray[1] = ["中医诊所","中西医结合诊所","民族医院诊所","口腔诊所","美容整形外科诊所","医疗美容诊所","精神卫生诊所","中小学卫生保健所"];
            break;
          case 2:
            data.multiArray[1] = ["综合门诊部","中医门诊部","中西医结合门诊部","民族医门诊部","专科门诊部","口腔门诊部","整形外科门诊部","医疗美容门诊部"];
            break;
          case 3:
            data.multiArray[1] = ["心血管医院","血液病医院","疗养院","妇幼保健院","康复医院","精神病医院","皮肤病医院","美容整容医院","肿瘤医院","口腔医院","眼科医院"];
            break;
          case 4:
            data.multiArray[1] = ["部级院","部队医院","省级医院","市级医院","县级以下医院","县级医院"];
            break;
          case 5:
            data.multiArray[1] = ["一般卫生院","二级卫生院"];
            break;
            case 6:
            data.multiArray[1] = [''];
            break;
            case 7:
            data.multiArray[1] = [''];
            break;
        }
        data.multiIndex[1] = 0;
        data.multiIndex[2] = 0;
        break;

        }


    this.setData(data);
  },
  bindRegionChange: function (e) {  //选择城市
    // console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      region: e.detail.value,
      province_id: e.detail.code[0],
      city_id: e.detail.code[1],
      area_id: e.detail.code[2],

    })
  },
  //去商城
  toShop:function(){
    wx.reLaunch({
       url: '/pages/em/em',
    })

  },
  toIndex:function(){
    wx.reLaunch({
       url: '/pages/index/index',
    })
  }

})