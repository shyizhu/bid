// pages/gys_register/gys_saomiao.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    moren:'../../../img/gys_register/up.png',
    yingyeImg: '../../../img/gys_register/up.png',
    zhengImg: '../../../img/gys_register/zheng.png',
    fanImg: '../../../img/gys_register/fan.png',
    xvkeImg: '',
    xvkeImg1:'',
    types: 'primary',
    btnText: '完成',
    tmpImg:{},
    morenShow:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    wx.setNavigationBarTitle({
      title: '企业件扫描',
    })
    var tmpImg = wx.getStorageSync('tmpImg')?wx.getStorageSync('tmpImg'):{};
      if(tmpImg.yingyeImg){
         that.setData({
            yingyeImg:app.globalData.apiUrl+'uploads/'+tmpImg.yingyeImg,
        })
      }
      if(tmpImg.zhengImg){
         that.setData({
            zhengImg:app.globalData.apiUrl+'uploads/'+tmpImg.zhengImg,
        })
      }
      if(tmpImg.fanImg){
         that.setData({
            fanImg:app.globalData.apiUrl+'uploads/'+tmpImg.fanImg,
        })
      }
      if(tmpImg.xvkeImg){
         that.setData({
            xvkeImg:app.globalData.apiUrl+'uploads/'+tmpImg.xvkeImg,
            morenShow:false
        })
      }
      if(tmpImg.xvkeImg1){
         that.setData({
            xvkeImg1:app.globalData.apiUrl+'uploads/'+tmpImg.xvkeImg1,
            morenShow: false

        })
      }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //营业执照上传
  yingyeImgUpfile:function (e) {
            var that = this;
            var tmpImg = wx.getStorageSync('tmpImg')?wx.getStorageSync('tmpImg'):{};
            wx.chooseImage({
                count:1,
                sizeType: ['compressed'],
                sourceType: ['album', 'camera'],
                success (res) {
                  const tempFilePaths = res.tempFilePaths
                  let tempFilesSize = res.tempFiles[0].size;  //获取图片的大小，单位B
                  if(tempFilesSize <= 3000000){   //图片小于或者等于2M时 可以执行获取图片
                        wx.showLoading({
                          title: '正在上传...',
                        })    
                        wx.uploadFile({
                          url: app.globalData.apiUrl + 'index/upfile',
                          filePath: tempFilePaths[0],
                          name: 'fileData',
                          header: that.data.header,
                          formData: {
                            'user': 'test'
                          },
                          success (re){
                            wx.hideLoading();
                                var imgUrl  =JSON.parse(re.data);
                              //更新图片
                              that.setData({
                                yingyeImg: app.globalData.apiUrl+'uploads/'+imgUrl.data
                              })
                              //存入缓存
                              tmpImg.yingyeImg = imgUrl.data;
                              wx.setStorageSync('tmpImg',tmpImg);

                            }
                          })
                      }else{
                        wx.showToast({
                            title:'上传图片不能大于3M!', 
                            icon:'none'       
                        })

                      }
                      
                }
              });
            
  },
  //法人身份证正面
  zhengImgUpfile:function (e) {
            var that = this;
            var tmpImg = wx.getStorageSync('tmpImg')?wx.getStorageSync('tmpImg'):{};
            wx.chooseImage({
                count:1,
                sizeType: ['compressed'],
                sourceType: ['album', 'camera'],
                success (res) {
                  const tempFilePaths = res.tempFilePaths
                  let tempFilesSize = res.tempFiles[0].size;  //获取图片的大小，单位B
                  if(tempFilesSize <= 3000000){   //图片小于或者等于2M时 可以执行获取图片
                        wx.showLoading({
                          title: '正在上传...',
                        })
                      wx.uploadFile({
                        url: app.globalData.apiUrl + 'index/upfile',
                        filePath: tempFilePaths[0],
                        name: 'fileData',
                        header: that.data.header,
                        formData: {
                          'user': 'test'
                        },
                        success (re){
                          wx.hideLoading();
                              var imgUrl  =JSON.parse(re.data);
                            //更新图片
                            that.setData({
                              zhengImg: app.globalData.apiUrl+'uploads/'+imgUrl.data
                            })
                            //存入缓存
                            tmpImg.zhengImg = imgUrl.data;
                            wx.setStorageSync('tmpImg',tmpImg);
                        }
                      })
                    }else{
                        wx.showToast({
                            title:'上传图片不能大于3M!', 
                            icon:'none'       
                        })

                    }                 
                }
              });
            
  },
  //法人身份证反面
  fanImgUpfile:function (e) {
            var that = this;
            var tmpImg = wx.getStorageSync('tmpImg')?wx.getStorageSync('tmpImg'):{};
            wx.chooseImage({
                count:1,
                sizeType: ['compressed'],
                sourceType: ['album', 'camera'],
                success (res) {
                  const tempFilePaths = res.tempFilePaths
                  let tempFilesSize = res.tempFiles[0].size;  //获取图片的大小，单位B
                  if(tempFilesSize <= 3000000){   
                        wx.showLoading({
                          title: '正在上传...',
                        })
                        wx.uploadFile({
                          url: app.globalData.apiUrl + 'index/upfile',
                          filePath: tempFilePaths[0],
                          name: 'fileData',
                          header: that.data.header,
                          formData: {
                            'user': 'test'
                          },
                          success (re){
                            wx.hideLoading();
                                var imgUrl  =JSON.parse(re.data);
                              //更新图片
                              that.setData({
                                fanImg: app.globalData.apiUrl+'uploads/'+imgUrl.data
                              })
                              //存入缓存
                              tmpImg.fanImg = imgUrl.data;
                              wx.setStorageSync('tmpImg',tmpImg);

                          }
                        })
                      }else{
                          wx.showToast({
                            title:'上传图片不能大于3M!', 
                            icon:'none'       
                         })
                      }
                }
              });
            
  },
  //医疗许可证
  xvkeImgUpfile:function (e) {
            var that = this;
            var tmpImg = wx.getStorageSync('tmpImg')?wx.getStorageSync('tmpImg'):{};
            wx.chooseImage({
                count:2,
                sizeType: ['compressed'],
                sourceType: ['album', 'camera'],
                success (res) {

                  const tempFilePaths = res.tempFilePaths
                  const tempFiles = res.tempFiles
                  let tempFilesSize = res.tempFiles[0].size;  //获取图片的大小，单位B
                  let imgLength = res.tempFilePaths.length; //图片长度
                   if(tempFilesSize <= 3000000){   
                      wx.showLoading({
                          title: '正在上传...',
                       })
                        wx.uploadFile({
                          url: app.globalData.apiUrl + 'index/upfile',
                          filePath: tempFilePaths[0],
                          name: 'fileData',
                          header: that.data.header,
                          formData: {
                            'user': 'test'
                          },
                          success (re){
                            wx.hideLoading();
                            that.setData({
                              morenShow:false
                            })
                                var imgUrl  =JSON.parse(re.data);
                              //更新图片
                              that.setData({
                                xvkeImg: app.globalData.apiUrl+'uploads/'+imgUrl.data
                              })
                              //存入缓存
                              tmpImg.xvkeImg = imgUrl.data;
                              wx.setStorageSync('tmpImg',tmpImg);

                              //如果是2张图片
                              if(imgLength == 2){
                                if(tempFiles[1].size <= 3000000){

                                   wx.showLoading({
                                      title: '正在上传...',
                                   })
                                    wx.uploadFile({
                                      url: app.globalData.apiUrl + 'index/upfile',
                                      filePath: tempFilePaths[1],
                                      name: 'fileData',
                                      header: that.data.header,
                                      formData: {
                                        'user': 'test'
                                      },
                                      success (r){
                                        wx.hideLoading();
                                            var imgUrl2  =JSON.parse(r.data);
                                          //更新图片
                                          that.setData({
                                            xvkeImg1: app.globalData.apiUrl+'uploads/'+imgUrl2.data
                                          })
                                          //存入缓存
                                          tmpImg.xvkeImg1 = imgUrl2.data;
                                          wx.setStorageSync('tmpImg',tmpImg);

                                        }
                                        })
                                     }else{
                                          wx.showToast({
                                            title:'上传图片不能大于3M!', 
                                            icon:'none'       
                                         })
                                    }  


                              }


                          }
                      })

                      }else{
                          wx.showToast({
                            title:'上传图片不能大于3M!', 
                            icon:'none'       
                         })
                    }  

                }
              });
            
  },
  back:function(){
      wx.navigateBack({
        delta: 1
      })

  }







})