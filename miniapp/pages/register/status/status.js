// pages/status/status.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    btn1: '我 是 供 应 商',
    btn2: '我 是 采 购 方',
    type1: 'primary',
    type2: 'primary',
    img1: '../../../img/status/img1.jpg',
    img2: '../../../img/status/img2.jpg'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '身份选择',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //供应商注册
  sRegister:function(){
    wx.navigateTo({
       url: '../gys_register/gys_register',
    })

  },
  //医院注册
  bRegister:function(){
    wx.navigateTo({
       url: '../cg_register/cg_register',
    })

  }










})