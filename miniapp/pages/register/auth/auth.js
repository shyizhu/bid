const app = getApp()
// pages/login/login.wxml.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    is_psd: true,
    img: false,
    tit: "",
    psd: "",
    userInfo:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '微信授权'
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindGetUserInfo: function(res) {
    wx.showLoading({
      title: '加载中',
    })
    var that = this 
   if(res.detail.userInfo){
        //用户信息传入存入缓存
        wx.setStorageSync('userInfo',res.detail.userInfo);
        that.setData({
                  userInfo: res.detail.userInfo
        })

       wx.login({
        success: function (r) {
          var code = r.code;
          if (code) {
            wx.getUserInfo({
                success: function(re) {
                  

                      wx.request({
                      url: app.globalData.apiUrl + 'login/getSessionKey',
                      method: 'post',
                      header: that.data.header,
                      data: {
                        encryptedData: res.detail.encryptedData,
                        iv: res.detail.iv,
                        code: code,
                        udata:re.encryptedData,
                        uiv:re.iv
                        
                      },
                      success: function (data) {
                        wx.hideLoading()
                        //缓存加入openid
                        that.data.userInfo.openid = data.data.data.openid;
                        wx.setStorageSync('userInfo',that.data.userInfo);
                        wx.navigateTo({
                              url: '../cj_login/cj_login',
                            })
                        },
                    
                    })

                }
              })

            
          }
      },//login
    })

   }else{
    wx.showToast({
            title: '需要通过授权才能继续，请重新点击并授权',
            icon: 'none',
            duration: 2000
          })
    
   }
  },
  goIndex:function (event) {
      wx.switchTab({
          url: '/pages/index/index',
      }) 
  }
 
})