// pages/gys_register/gys_register.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    is_show:true,
    show_success:false,
    title: '请提交注册商信息',
    msg: [{  //  用户名至企业名称循环数据
      label: '用户名',
      id: 'user_name',
      type: 'text',
      placehold: '大小写字母数字5-15位',
      bindinput: 'getVal',
      val: '',
      reg: '^[A-Za-z0-9]{5,15}$'
    },{
      label: '密码',
      id: 'user_pass',
      type: 'password',
      placehold: ' 6~18 大小写字母数字',
      bindinput: 'getVal',
      val: '',
      reg: '^[a-zA-Z0-9]{6,18}$'
    }, {
      label: '确认密码',
      id: 'aginPwd',
      type: 'password',
      placehold: '两次密码需一致',
      bindinput: 'getVal',
      val: '',
      reg: '^[a-zA-Z0-9]{6,18}$'
    }, {
      label: '企业名称',
      id: 'company_name',
      type: 'text',
      placehold: '请填写',
      bindinput: 'getVal',
      val: '',
      reg: '^[\u4E00-\u9FA5A-Za-z0-9_]{2,40}$'
    }],

    city: '所在省市',  //选择城市数据
    region: ['', '','请选择'],
    province_id: '',
    city_id: '',
    area_id: '',

    msg2: [{  // 通讯地址至邮箱循环数据
      label: '通讯地址',
      id: 'post_addr',
      type:'text',
      placehold: '请填写',
      bindinput: 'getVal2',
      val: '',
      reg: '^[\\S]{3,50}$'
    },{
      label: '营业执照号',
      id: 'business_license_num',
      type: 'text',
      placehold: '请填写',
      bindinput: 'getVal2',
      val: '',
      reg: '^\\w+$'
    },{
      label: '法人姓名',
      id: 'legal_person',
      type: 'text',
      placehold: '请填写',
      bindinput: 'getVal2',
      val: '',
      reg: '^[\u4E00-\u9FA5A-Za-z0-9]{2,40}$'
    },{
      label: '法人身份证号',
      id: 'legal_person_no',
      type: 'text',
      placehold: '请填写',
      bindinput: 'getVal2',
      val: '',
      reg: '(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)'
    },{
      label: '邮箱',
      id: 'user_email',
      type: 'text',
      placehold: '请填写',
      bindinput: 'getVal2',
      val: '',
      reg: '^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$'
    }],
    
    file: '企业扫描件',
    checked: true, //复选框是否选中a
    btnType: 'primary'  //按钮样式
  },
  bindRegionChange: function (e) {  //选择城市
    // console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      region: e.detail.value,
      province_id: e.detail.code[0],
      city_id: e.detail.code[1],
      area_id: e.detail.code[2],

    })
  },
  getVal:function(e){
    var arr = this.data.msg;
    var obj = arr[e.currentTarget.dataset.index]
    obj.val = e.detail.value
    this.setData({
      msg: arr
    })
  },
  getVal2: function (e) {
    var arr = this.data.msg2;
    var obj = arr[e.currentTarget.dataset.index]
    obj.val = e.detail.value
    this.setData({
      msg2: arr
    })
  },
  submit:function(){
    var that = this;
    var arr = that.data.msg;
    var arr1 = that.data.msg2;
    var tmpImg = wx.getStorageSync('tmpImg')?wx.getStorageSync('tmpImg'):{};
    var userInfo = wx.getStorageSync('userInfo')

    var formData ={}
    for(var i=0;i<arr.length;i++){
      formData[arr[i].id] =arr[i].val;

      if(arr[i].val === ""){
        wx.showToast({
          title: `请输入${arr[i].label}`,
          icon: 'none',
          duration: 2000,
          mask: true
        })
        return false;
      }else if(new RegExp(arr[i].reg).test(arr[i].val) != true){
        wx.showToast({
          title: `${arr[i].label}格式不正确`,
          icon: 'none',
          duration: 2000,
          mask: true
        })
        return false;
      }
    }

    for (var i = 0; i < arr1.length; i++) {
      formData[arr1[i].id] =arr1[i].val;

      if (arr1[i].val === "") {
        wx.showToast({
          title: `请输入${arr1[i].label}`,
          icon: 'none',
          duration: 2000,
          mask: true
        })
        return false;
      }else if(new RegExp(arr1[i].reg).test(arr1[i].val) !=true){
        console.log(new RegExp(arr1[i].reg))
        wx.showToast({
          title: `${arr1[i].label}格式不正确`,
          icon: 'none',
          duration: 2000,
          mask: true
        })
        return false;
      }
    }
    if(arr[1].val != arr[2].val){
      wx.showToast({
        title: '两次密码不一致',
        icon: 'none',
        duration: 2000,
        mask: true
      })
      return false;
    }
    if(that.data.province_id == "" || that.data.city_id == "" ||that.data.area_id == ""){
      wx.showToast({
        title: '请选择省市',
        icon: 'none',
        duration: 2000,
        mask: true
      })
      return false;
     }
    if(!that.data.checked){
       wx.showToast({
        title: '需同意注册供应商协议',
        icon: 'none',
        duration: 2000,
        mask: true
      })
      return false;
    }

    if(tmpImg.yingyeImg == undefined || tmpImg.zhengImg == undefined || tmpImg.fanImg == undefined || tmpImg.xvkeImg == undefined){
         wx.showToast({
          title: `请上传所需企业扫描件`,
          icon: 'none',
          duration: 2000,
          mask: true
        })
         return false;
    } 
    //拼接  省市区，企业扫描件 用户信息
    formData['province_id'] = that.data.province_id;
    formData['city_id'] = that.data.city_id;
    formData['area_id'] = that.data.area_id;
    formData['yingyeImg'] = tmpImg.yingyeImg;
    formData['zhengImg'] = tmpImg.zhengImg;
    formData['fanImg'] = tmpImg.fanImg;
    formData['xvkeImg'] = tmpImg.xvkeImg;
    if(tmpImg.xvkeImg1){
          formData['xvkeImg1'] = tmpImg.xvkeImg1;
    }
    formData['user_token'] = userInfo.user_token;
    formData['user_id'] = userInfo.user_id;

    

       wx.showModal({
          title: '提示',
          content: '可参与竞价，确定认证为供应商？',
          success (res) {
            if (res.confirm) {
                 wx.request({
                      url: app.globalData.apiUrl + 'login/sellerRegister',
                      method: 'post',
                      header: that.data.header,
                      data: {
                        formData:formData,
                       
                        
                      },
                      success: function (re) {
                        if(re.data.code == 200){
                          //缓存加入openid
                        userInfo.user_type = 11;//供应商
                        wx.setStorageSync('userInfo',userInfo);
                            that.setData({
                              is_show:false,
                              show_success:true,
                            })

                        }else{
                            wx.showToast({
                              title: re.data.msg,
                              icon: 'none',
                              duration: 2000,
                              mask: true
                            })
                        }
                        
                      },
                    
                    })        
             } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
          


  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '供应商注册'
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //跳转到上传那图片
  goUpfile:function(){
    wx.navigateTo({
       url: '../gys_saomiao/gys_saomiao',
    })
  },
  checkboxChange: function(e) {
        var that =this;
        that.setData({
          checked:!that.data.checked
        })
  },
  toAgree:function(){
    wx.navigateTo({
       url: '../gys_agree/gys_agree',
    })
  },
  toIndex:function(){
    wx.reLaunch({
       url: '/pages/index/index',
    })
  },
   //去商城
  toShop:function(){
    wx.reLaunch({
       url: '/pages/em/em',
    })

  },




})