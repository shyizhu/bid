// pages/gys_register/gys_saomiao.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    yingyeImg: '../../../img/gys_register/up.png',
    zhiyeImg: '../../../img/gys_register/up.png',
    types: 'primary',
    btnText: '完成',
    yhtmpImg:{} //医院上传图片缓存

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '证件扫描件',
    })

    var that = this;
    var tmpImg = wx.getStorageSync('yhtmpImg')?wx.getStorageSync('yhtmpImg'):{};
      if(tmpImg.yingyeImg){
         that.setData({
            yingyeImg:app.globalData.apiUrl+'uploads/'+tmpImg.yingyeImg,
        })
      }
      if(tmpImg.zhiyeImg){
         that.setData({
            zhiyeImg:app.globalData.apiUrl+'uploads/'+tmpImg.zhiyeImg,
        })
      }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
   back:function(){
      wx.navigateBack({
        delta: 1
      })

  },
   //营业执照上传
  yingyeImgUpfile:function (e) {
            var that = this;
            var tmpImg = wx.getStorageSync('yhtmpImg')?wx.getStorageSync('yhtmpImg'):{};
            wx.chooseImage({
                count:1,
                sizeType: ['compressed'],
                sourceType: ['album', 'camera'],
                success (res) {
                  const tempFilePaths = res.tempFilePaths
                  let tempFilesSize = res.tempFiles[0].size;  //获取图片的大小，单位B
                  if(tempFilesSize <= 3000000){   
                        wx.showLoading({
                          title: '正在上传...',
                        })    
                        wx.uploadFile({
                          url: app.globalData.apiUrl + 'index/upfile',
                          filePath: tempFilePaths[0],
                          name: 'fileData',
                          header: that.data.header,
                          formData: {
                            'user': 'test'
                          },
                          success (re){
                            wx.hideLoading();
                                var imgUrl  =JSON.parse(re.data);
                              //更新图片
                              that.setData({
                                yingyeImg: app.globalData.apiUrl+'uploads/'+imgUrl.data
                              })
                              //存入缓存
                              tmpImg.yingyeImg = imgUrl.data;
                              wx.setStorageSync('yhtmpImg',tmpImg);

                          }
                        })
                    }else{
                        wx.showToast({
                            title:'上传图片不能大于3M!', 
                            icon:'none'       
                        })

                    }

                }
              });
            
  },
   //医院执业许可证
  zhiyeImgUpfile:function (e) {
            var that = this;
            var tmpImg = wx.getStorageSync('yhtmpImg')?wx.getStorageSync('yhtmpImg'):{};
            wx.chooseImage({
                count:1,
                sizeType: ['compressed'],
                sourceType: ['album', 'camera'],
                success (res) {
                  const tempFilePaths = res.tempFilePaths
                  let tempFilesSize = res.tempFiles[0].size;  //获取图片的大小，单位B
                  if(tempFilesSize <= 3000000){   //图片小于或者等于2M时 可以执行获取图片
                        wx.showLoading({
                          title: '正在上传...',
                        })    
                        wx.uploadFile({
                          url: app.globalData.apiUrl + 'index/upfile',
                          filePath: tempFilePaths[0],
                          name: 'fileData',
                          header: that.data.header,
                          formData: {
                            'user': 'test'
                          },
                          success (re){
                            wx.hideLoading();
                                var imgUrl  =JSON.parse(re.data);
                              //更新图片
                              that.setData({
                                zhiyeImg: app.globalData.apiUrl+'uploads/'+imgUrl.data
                              })
                              //存入缓存
                              tmpImg.zhiyeImg = imgUrl.data;
                              wx.setStorageSync('yhtmpImg',tmpImg);

                          }
                         })
                      }else{
                        wx.showToast({
                            title:'上传图片不能大于3M!', 
                            icon:'none'       
                        })

                      }
                }
              });
            
  },
})