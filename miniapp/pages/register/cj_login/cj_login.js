const app = getApp()
// pages/login/login.wxml.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    is_psd: true,
    img: false,
    tit: "",
    psd: "",
    userInfo:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '微信快捷登录'
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //微信快捷登录
  //获取微信绑定的手机号
  getPhoneNumber: function(e) {
    var that = this
    var encryptedData = e.detail.encryptedData
    var iv = e.detail.iv
    var userInfo = wx.getStorageSync('userInfo')

    if (e.detail.errMsg == 'getPhoneNumber:fail user deny') {
      wx.showToast({
            title: '需要通过授权才能继续，请重新点击并授权',
            icon: 'none',
            duration: 2000
          })
    } else {
      wx.login({
        success: function(res) {
          if (res.code) {
            wx.request({
              url: app.globalData.apiUrl + 'login/getUserPhone',
              method: 'post',
              header: that.data.header,
              data: {
                encryptedData: encryptedData,
                iv: iv,
                code: res.code,
                user_nick:userInfo.nickName
              },
              success: function(data) {
                // console.log(data)
                //更新缓存
                if(data.data.code == 200){
                      userInfo.phoneNumber = data.data.data.phoneNumber
                      userInfo.user_id = data.data.data.user_id
                      userInfo.user_token = data.data.data.user_token
                      userInfo.user_type = data.data.data.user_type
                      

                      wx.setStorageSync('userInfo',userInfo);
                      that.setData({
                        userInfo: userInfo
                      })
                            //返回我的
                            wx.reLaunch({
                            url: '/pages/my/home/home',

                          })
                      
                }
                
              },
           })
          }
        }
      })
    }
  },
  gologin:function (event) {
      wx.navigateTo({
          url: '../login/login',
      }) 
  },
  goIndex:function (event) {
      wx.switchTab({
          url: '/pages/index/index',
      }) 
  }
})