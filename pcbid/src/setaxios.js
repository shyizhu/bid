import axios from 'axios'
import router from './router'

//http全局拦截
//token要放在我们请求的header上面带回去给后端


export default function setAxios(){

    //请求拦截
    axios.interceptors.request.use(
        config=>{
            let userInfo = localStorage.getItem('userInfo');
            if(userInfo){
                config.headers.token = JSON.parse(userInfo).user_token
            }

            return config
        }
    )
        //每次的请求有返回的，都是先经过这个拦截器先的
    axios.interceptors.response.use(
        response=>{
            if(response.status==200){
                const data=response.data
                if(data.code== 401){
                    localStorage.removeItem('userInfo')
                    //跳转到login页面
                    router.replace({path:'/register/login'})
                }
                return data
            }
            return response
        }
    )
}