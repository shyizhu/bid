import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Test from './views/Test.vue'

//register 登录/注册
import Login from './views/register/Login.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    //主页
    {
      path: '/',
      name: 'home',
      component: Home
    },
    //about
    {
      path: '/about',
      name: 'about',
      component: () => import('./views/About.vue')
    },
    //404
    {
      path: '/error',
      name: 'error',
      component: () => import('./views/Error.vue')
    },
    /*
      register 登录/注册
      
    */
    {
      path: '/register/login',
      name: 'login',
      component: Login
    },
    {
      path: '/register/status',
      name: 'status',
      component: () => import('./views/register/Status.vue')
    },
    {
      path: '/register/cgregister',
      name: 'cgregister',
      component: () => import('./views/register/Cgregister.vue')
    },
  
    // {
    //   path: '/register/cgagree',
    //   name: 'cgagree',
    //   component: () => import('./views/register/Cgagree.vue')
    // },
    {
      path: '/register/gysregister',
      name: 'gysregister',
      component: () => import('./views/register/Gysregister.vue')
    },
   
    // {
    //   path: '/register/gysagree',
    //   name: 'gysagree',
    //   component: () => import('./views/register/Gysagree.vue')
    // },

    /*
       my 我的
      

    */
    // {
    //   path: '/my/my',
    //   name: 'my',
    //   component: () => import('./views/my/My.vue')
    // },
    {
      path: '/my/agree',//消息
      name: 'agree',
      component: () => import('./views/my/Agree.vue')
    },
    {
      path: '/my/notice',//消息
      name: 'notice',
      component: () => import('./views/my/Notice.vue')
    },
    {
      path: '/my/bidlist',//订单
      name: 'bidlist',
      meta:{
            requireAuth:true,
      },
      component: () => import('./views/my/Bidlist.vue')
    },
    
    {
      path: '/my/usertype', //认证
      name: 'usertype',
      component: () => import('./views/my/Usertype.vue')
    },
    {
      path: '/my/editpass',//修改密码
      name: 'editpass',
      component: () => import('./views/my/Editpass.vue')
    },
    {
      path: '/my/inviteman',//成为合伙人
      name: 'inviteman',
      component: () => import('./views/my/Inviteman.vue')
    },
    
    /*
      bid 招投标
      

    */
    {
      path: '/bid/pushbid',
      name: 'pushbid',
      meta:{
            requireAuth:true,
      },
      component: () => import('./views/bid/Pushbid.vue')
    },
    {
      path: '/bid/showdetail/showdetail',
      name: 'showdetail',
      component: () => import('./views/bid/Showdetail.vue')
    },
    {
      path: '/bid/onedetail/onedetail',
      name: 'onedetail',
      component: () => import('./views/bid/Onedetail.vue')
    },
    {
      path: '/bid/twodetail/twodetail',
      name: 'twodetail',
      component: () => import('./views/bid/Twodetail.vue')
    },
    {
      path: '/bid/threedetail/threedetail',
      name: 'threedetail',
      component: () => import('./views/bid/Threedetail.vue')
    },
    {
      path: '/bid/notdetail/notdetail',
      name: 'notdetail',
      component: () => import('./views/bid/Notdetail.vue')
    },
    {
      path: '/bid/enddetail/enddetail',
      name: 'enddetail',
      component: () => import('./views/bid/Enddetail.vue')
    },
    {
      path: '/bid/typebidlist',
      name: 'typebidlist',
      component: () => import('./views/bid/Typebidlist.vue')
    },
    {
      path: '/bid/bidcenter',
      name: 'bidcenter',
      component: () => import('./views/bid/Bidcenter.vue')
    },

  ]
})
